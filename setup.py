import setuptools

with open('README.md', 'r') as readme_file:
    long_description = readme_file.read()

setuptools.setup(
    name='gym_dragon_search',
    version='0.1.0',
    author="Yu Quan Chong",
    author_email="yuquanc@andrew.cmu.edu",
    description='CBS-TA-PTC search to solve gym_dragon',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/cmu_aart/asist/gym-dragon-search',
    packages=setuptools.find_packages(include=['gym_dragon_search']),
    python_requires='>=3.9',
    install_requires=[
        'gym_dragon',
        'scipy>=1.10.1',
        'scikit-learn>=1.2.2',
        'seaborn'
    ],
)
