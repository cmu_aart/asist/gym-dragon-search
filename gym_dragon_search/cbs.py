import copy
import heapq
import itertools
import math
import numpy as np
import os
import random
import shutil
import signal
import time

from .dragon_search import DragonBaseEnvSearch
from gym_dragon.core import Color, Region
from gym_dragon_search.utils import (
    bomb_dependency_order_sort,
    compute_distance,
    detect_collisions,
    generate_allocations,
    get_absolute_temporal_range_conflicts,
    get_bomb_defusal_info,
    get_bomb_dependency_order_list,
    get_gif,
    get_inter_goal_temporal_conflicts,
    get_new_node_constraints,
    get_path_dict,
    get_precedence_conflicts,
    get_return_conflicts, 
    get_sub_bomb_dependency_order_list,
    get_sub_bomb_fuse_dict,
    resolve_absolute_temporal_range_conflict,
    resolve_collision,
    resolve_inter_goal_temporal_conflict,
    resolve_precedence_conflict,
    resolve_return_conflict
)
from sklearn.cluster import KMeans
from typing import Callable, Union, Optional

class CBSSolver(object):
    """
    Conflict Based Search (CBS) solver for gym_dragon

    Algorithms
    ----------
    'cbs_ta' : CBS with task assignment. Naively adds vertex conflicts when environment terminates with score less than
               epsilon * max score
    'cbs_ta_ptc' : CBS with task assignment, precedence and temporal constraints. 
    """
    def __init__(self,
                 algo: str = 'cbs_ta_ptc',
                 num_bombs_sub_task: int = 3,
                 mission_length: float = 10*60,
                 recon_phase_length: float = 2*60, 
                 seconds_per_timestep: float = 2.0,
                 valid_regions: set[Region] = set(Region),
                 csv_path: Optional[str] = None,
                 num_bombs_per_region: int = 15,
                 budget_per_region: int = None,
                 start_location: Optional[tuple[int, int]] = (24, 149),
                 start_regions: set[Region] = set(Region),
                 epsilon: float = 1.0,
                 same_start_loc: bool = True,
                 include_collisions: bool = False,
                 include_chained_bombs: bool = True,
                 include_fire_bombs: bool = False,
                 include_fuse_bombs: bool = True,
                 bomb_explode_terminate: bool = True,
                 task_allocation_heuristic: str = 'k_means',
                 pruning_heuristic: str = 'even_bomb_allocation',
                 low_level_search: str = 'multi_label_a_star',
                 save_render: bool = False,
                 verbose: bool = True,
                 **kwargs
        ):
        """
        Parameters
        ----------
        algo : str, default='cbs_ta_ptc'
            CBS algorithm ('cbs_ta', "cbs_ta_ptc")
        num_bombs_sub_task : int, default=3
            Number of bombs to resolve per sub-solution search
        mission_length : float, default=10*60
            Mission length, in seconds
        recon_phase_length : float, default=2*60
            Length of reconnaissance phase, in seconds
        seconds_per_timestep : float, default=2.0
            Number of seconds per timestep in the environment
        valid_regions : set[Region], optional
            Set of regions to include in the environment
        csv_path : str, optional
            Path to CSV file containing bomb distribution
        num_bombs_per_region : int, default=15
            Number of bombs to be placed randomly in each region
        budget_per_region : int, default=None
            Team budget per region, to be spent randomly on purchasing tools
        start_location : tuple[int, int], optional, default=(24, 149)
            Initial location for all agents
        start_regions : set[Region], optional
            Set of valid regions for initial mission locations
        epsilon : float, default=1.0
            Suboptimality factor for score. Default is optimal
        same_start_loc : bool, default=True
            Whether for all agents to start at the same location
            NOTE: Default DragonBaseEnv starts at same location/node
        include_collisions : bool, default=False
            Whether to consider collisions for environment
            NOTE: Default DragonBaseEnv has no collisions
        include_chained_bombs : bool, default=True
            Whether to include chained bombs in the environment
        include_fire_bombs : bool, default=False
            Whether to include fire bombs in the environment
        include_fuse_bombs : bool, default=True
            Whether to include fuse bombs in the environment
        bomb_explode_terminate : bool, default=True
            Whether to terminate environment when bomb explodes
        task_allocation_heuristic : str, default='k_means'
            Heuristic to order tasks
        pruning_heuristic : str, default='even_bomb_allocation'
            Heuristic to prune subtask allocations
        low_level_search : str
            Algorithm for low level search. Either 'multi_step_a_star' or 'multi_label_a_star'
        save_render : bool, default=False
            Whether to save rendered images
        verbose : bool, default=True
            Whether to print metrics/results

        Keyword Arguments
        -----------------
        save_path : str
            Path to save rendered images
        num_of_clusters : int
            Number of clusters for K-Means
        csv_subtract_fuse : int
            Recon phase length to subtract from fuse if reading bomb distribution from CSV
        overlay_graph : bool
            Whether to overlay the graph representation over the map for rendering
        """
        # Generate random seed
        self.random_seed = random.randint(0, 1e10)

        # Parameters
        self.algo = algo
        self.num_bombs_sub_task = num_bombs_sub_task
        self.csv_path = csv_path
        self.num_bombs_per_region = num_bombs_per_region
        self.mission_length = mission_length
        self.recon_phase_length = recon_phase_length
        self.budget_per_region = budget_per_region
        self.start_location = start_location
        self.start_regions = start_regions
        self.epsilon = epsilon
        self.same_start_loc = same_start_loc
        self.include_collisions = include_collisions
        self.include_chained_bombs = include_chained_bombs
        self.include_fire_bombs = include_fire_bombs
        self.include_fuse_bombs = include_fuse_bombs
        self.bomb_explode_terminate = bomb_explode_terminate
        self.task_allocation_heuristic = task_allocation_heuristic
        self.pruning_heuristic = pruning_heuristic
        self.low_level_search = low_level_search
        self.save_render = save_render
        self.verbose = verbose
        self.save_path = kwargs.get('save_path', './experiments/gif/test') 
        self.num_of_clusters = kwargs.get('num_of_clusters', self.num_bombs_per_region // self.num_bombs_sub_task)
        self.csv_subtract_fuse = kwargs.get('csv_subtract_fuse', 0)
        self.overlay_graph = kwargs.get('overlay_graph', True)
        self.num_of_generated = 0
        self.num_of_expanded = 0
        self.open_list = []
        self.open_root_list = []
        self.team_bomb_tup_list = []
        self.bomb_dependency_order_list = None
        self.team_bomb_dependency_order_list = []
        self.bomb_fuse_dict = None
        self.team_bomb_fuse_dict_list = [] 

        assert algo == 'cbs_ta_ptc' or algo == 'cbs_ta', f'{algo} is not implemented.'
        # If start location for all agens is given, cannot start at different start location / cannot include collisions
        assert not (start_location != None and (same_start_loc == False or include_collisions == True)), \
            'Start location is given. Cannot start at different location or include collisions.'
        # Ensure that agents start at the same location if collisions are allowed
        assert not (same_start_loc == True and include_collisions == True), \
            'Cannot start at same location with collisions included'   

        # Directory for saving rendered images
        if self.save_render:
            if not os.path.exists(self.save_path):
                # Create directory if it doesn't exist
                os.makedirs(self.save_path)
            else:
                # Remove and make new directory if it exist
                shutil.rmtree(self.save_path)
                os.makedirs(self.save_path)

        # Starting timestep
        self.start_timestep = math.ceil(recon_phase_length / seconds_per_timestep)

        # Initialise environment
        self.env = DragonBaseEnvSearch(mission_length=mission_length,
                                       recon_phase_length=recon_phase_length,
                                       seconds_per_timestep=seconds_per_timestep,
                                       valid_regions=valid_regions,
                                       same_start_loc=same_start_loc,
                                       include_collisions=include_collisions,
                                       include_chained_bombs=include_chained_bombs,
                                       include_fire_bombs=include_fire_bombs,
                                       include_fuse_bombs=include_fuse_bombs,
                                       bomb_explode_terminate=bomb_explode_terminate)
        self.tool_allocation = self.env.random_tool_allocation(
            budget=10000, random=np.random.default_rng(self.random_seed))
        self.env_reset(pre_compute_heuristics=True)
        
        # Maximum possible score
        self.max_score = self.env.max_score
        
        # Task assignment using k_means heuristic
        if self.task_allocation_heuristic == 'k_means':
            # Obtain the bomb sequences and locations
            bomb_seq_dict = self.env.bomb_sequence
            bomb_locations_dict = self.env.bomb_locations
            bomb_locations_list = list(self.env.bomb_locations.values())
            # K means clustering of bomb locations
            k_means = KMeans(n_clusters=self.num_of_clusters, 
                             n_init='auto', 
                             random_state=(self.random_seed % (2**32))
                            ).fit(bomb_locations_list)
            # Obtain current location tuple that is the average of the centroid of start locations
            curr_loc_x = 0
            curr_loc_y = 0 
            for start_node in self.env.start_nodes.values():
                curr_loc_x += start_node.centroid[0]
                curr_loc_y += start_node.centroid[1]
            curr_loc = (round(curr_loc_x / len(self.env.start_nodes)), round(curr_loc_y / len(self.env.start_nodes)))
            # Obtain clusters centers as a list
            cluster_centers_list = k_means.cluster_centers_.tolist()
            # Dictionary to store sorted bomb sequences
            # Sort based on the following methodology:
            #   1) Agents are assigned to the nearest cluster of bombs (cluster center) from current location 
            #      (average centroid of start_node location / cluster center)
            #   2) Within a cluster, bombs are sorted according the nearest distance from current location
            sorted_bomb_seq_dict = {}
            while len(cluster_centers_list) != 0:
                # Track closest cluster to current location
                min_dist = np.inf
                min_cluster_label = None
                min_cluster_center_loc = None 
                # Iterate over remaining cluster centers
                for cluster_center_loc in cluster_centers_list:
                    # Compute euclidean distance between current location and cluster center
                    dist = compute_distance(curr_loc, cluster_center_loc)
                    # Update tracking variables
                    if dist < min_dist:
                        min_dist = dist
                        min_cluster_label = np.where([np.array_equal(x, cluster_center_loc) \
                                                      for x in k_means.cluster_centers_])[0][0]
                        min_cluster_center_loc = cluster_center_loc.copy()
                # Remove closest cluster
                cluster_centers_list.remove(min_cluster_center_loc)
                # Generate bomb locations at the closest cluster
                cluster_bomb_locations_list = [bomb_locations_list[ind] \
                                               for ind in np.where(k_means.labels_ == min_cluster_label)[0]]
                while len(cluster_bomb_locations_list) != 0:
                    # Track cloest bomb to current location
                    min_dist = np.inf
                    min_bomb_id = None
                    min_bomb_location = None
                    # Iterate over remaining bomb locations
                    for bomb_loc in cluster_bomb_locations_list:
                        # Compute euclidean distance between current location and bomb location
                        dist = compute_distance(curr_loc, bomb_loc)
                        # Update tracking variables
                        if dist < min_dist:
                            min_dist = dist
                            min_bomb_id = next(key for key, value in bomb_locations_dict.items() if value == bomb_loc)
                            min_bomb_location = bomb_loc
                    # Update sorted_bomb_seq_dict
                    sorted_bomb_seq_dict.update({min_bomb_id: bomb_seq_dict[min_bomb_id]})
                    # Remove closest bomb location
                    cluster_bomb_locations_list.remove(min_bomb_location)
                # Update current location
                curr_loc = min_cluster_center_loc.copy()
            # Check for chained bombs
            if self.include_chained_bombs:
                # Obtain bomb dependencies
                bomb_dependency = self.env.bomb_dependency
                # Obtain list of lists containing order to resolve bombs given dependenies to avoid explosions
                self.bomb_dependency_order_list = get_bomb_dependency_order_list(bomb_dependency.copy())
                # Sort sorted_bomb_seq_dict to ensure ordering from bomb_dependency_order_list are followed
                sorted_bomb_seq_dict = bomb_dependency_order_sort(sorted_bomb_seq_dict, self.bomb_dependency_order_list)
            # Check for fuse bombs
            if self.include_fuse_bombs:
                # Obtain bomb fuses
                self.bomb_fuse_dict = self.env.bomb_fuse
            # Generate a sequence of tasks based on tuples (bomb ID, Color) for each subtask and compile to a list
            for i in range(0, len(bomb_seq_dict), self.num_bombs_sub_task):
                # Obtain chunks of sorted_bomb_seq_dict with length num_bombs_sub_task
                sub_bomb_seq_dict = dict(itertools.islice(sorted_bomb_seq_dict.items(), i, i + self.num_bombs_sub_task))
                # Obtain tuples of (bomb ID, Color) for subtask
                sub_team_bomb_tup_list = []
                for bomb_id, sequence in sub_bomb_seq_dict.items():
                    sub_team_bomb_tup_list += [(bomb_id, color) for color in sequence]
                # Append to sub task list to main list
                self.team_bomb_tup_list.append(sub_team_bomb_tup_list[:])
                # Check for chained bombs
                if self.include_chained_bombs:
                    # Get relevant bomb_dependency_order_list for each subtask given chained bombs
                    sub_bomb_dependency_order_list = get_sub_bomb_dependency_order_list(self.bomb_dependency_order_list, 
                                                                                        sub_bomb_seq_dict)
                    # Append to main list
                    self.team_bomb_dependency_order_list.append(sub_bomb_dependency_order_list)
                # Check for fuse bombs
                if self.include_fuse_bombs:
                    # Get relevant bomb fuses for each subtask given fuse bombs
                    # Ensure that fuses are in tuple format (e.g. (0, 60) or (180, 240))
                    sub_bomb_fuse_dict = get_sub_bomb_fuse_dict(self.bomb_fuse_dict, sub_bomb_seq_dict)
                    # Append to main list
                    self.team_bomb_fuse_dict_list.append(sub_bomb_fuse_dict)
        # Task assignment in ascending fuse length
        elif self.task_allocation_heuristic == 'fuse_length':
            # Ensure that include_fuse_bombs is true
            assert self.include_fuse_bombs == True, 'Cannot sort by fuse length if fuse bombs are not included'
            # Obtain the bomb sequences and fuse
            bomb_seq_dict = self.env.bomb_sequence
            self.bomb_fuse_dict = self.env.bomb_fuse
            # Obtain sorted bomb sequences based on fuse length in ascending order
            sorted_bomb_seq_dict = dict(sorted(bomb_seq_dict.items(), key=lambda x: self.bomb_fuse_dict[x[0]]))
            # Check for chained bombs
            if self.include_chained_bombs:
                # Obtain bomb dependencies
                bomb_dependency = self.env.bomb_dependency
                # Obtain list of lists containing order to resolve bombs given dependenies to avoid explosions
                self.bomb_dependency_order_list = get_bomb_dependency_order_list(bomb_dependency.copy())
                # Sort sorted_bomb_seq_dict to ensure ordering from bomb_dependency_order_list are followed
                sorted_bomb_seq_dict = bomb_dependency_order_sort(sorted_bomb_seq_dict, self.bomb_dependency_order_list)
            # Generate a sequence of tasks based on tuples (bomb ID, Color) for each subtask and compile to a list
            for i in range(0, len(bomb_seq_dict), self.num_bombs_sub_task):
                # Obtain chunks of sorted_bomb_seq_dict with length num_bombs_sub_task
                sub_bomb_seq_dict = dict(itertools.islice(sorted_bomb_seq_dict.items(), i, i + self.num_bombs_sub_task))
                # Obtain tuples of (bomb ID, Color) for subtask
                sub_team_bomb_tup_list = []
                for bomb_id, sequence in sub_bomb_seq_dict.items():
                    sub_team_bomb_tup_list += [(bomb_id, color) for color in sequence]
                # Append to sub task list to main list
                self.team_bomb_tup_list.append(sub_team_bomb_tup_list[:])
                # Check for chained bombs
                if self.include_chained_bombs:
                    # Get relevant bomb_dependency_order_list for each subtask given chained bombs
                    sub_bomb_dependency_order_list = get_sub_bomb_dependency_order_list(self.bomb_dependency_order_list, 
                                                                                        sub_bomb_seq_dict)
                    # Append to main list
                    self.team_bomb_dependency_order_list.append(sub_bomb_dependency_order_list)
                # Get relevant bomb fuses for each subtask given fuse bombs
                # Ensure that fuses are in tuple format (e.g. (0, 60) or (180, 240))
                sub_bomb_fuse_dict = get_sub_bomb_fuse_dict(self.bomb_fuse_dict, sub_bomb_seq_dict)
                # Append to main list
                self.team_bomb_fuse_dict_list.append(sub_bomb_fuse_dict)
        
        # Check if agents can solve the problem given the tools allocation it has
        if not self.is_solvable():
            if self.verbose:
                self.print_results(False)
            raise Exception(f'For given bombs and tools from agents, task has no solution')

    def bomb_id_to_node_id(self, bomb_ids_list: list[int]) -> list[int]:
        """
        Function to convert bomb IDs to node IDs

        Parameters
        ----------
        bomb_ids_list : list[int]
            List of bomb IDs of bombs that are the sequence of bombs the agent has to visit

        Returns
        -------
        node_ids_list : list[int]
            List of node IDs with bomb IDs of bombs that are the sequence of bombs the agent has to visit
        """
        # Reset environment
        self.env_reset()

        node_ids_list = []
        for bomb_id in bomb_ids_list:
            node_ids_list.append(self.env.bomb_id_to_node_id(bomb_id))
        return node_ids_list

    def env_reset(self, pre_compute_heuristics: bool = False):
        """
        Function to reset environment

        Parameters
        ----------
        pre_compute_heuristics : bool, default=False
            Whether to pre-compute heuristics
        """ 
        # Reset environment
        self.env.reset(seed=self.random_seed,
                       csv_path=self.csv_path,
                       num_bombs_per_region=self.num_bombs_per_region,
                       budget_per_region=self.budget_per_region,
                       start_location=self.start_location,
                       start_regions=self.start_regions,
                       tool_allocation=self.tool_allocation,
                       csv_subtract_fuse=self.csv_subtract_fuse)
        # Pre-compute heuristics if necessary
        if pre_compute_heuristics:
            self.env.pre_compute_heuristics(seed=self.random_seed,
                                            additional_node_id_list=[])
        # Fast forward past recon phase if necessary
        if self.start_timestep != 0:
            self.env.tick(dt=self.start_timestep * self.env.seconds_per_timestep)

    def evaluate_paths(
            self,
            bomb_id_tup_dict: dict[str, list[tuple[int, Color]]], 
            paths: dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]],
            past_bomb_id_tup_dict: Optional[dict[str, list[tuple[int, Color]]]] = None,  
            past_paths: Optional[dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]] = None,
            sub_bomb_dependency_order_list: Optional[list[list[int]]] = None,
            sub_bomb_fuse_dict: Optional[dict[int, tuple[int, int]]] = None, 
            render: bool = False
        ) -> tuple[list[dict[str, Union[Callable, float, str]]], list[dict[str, Union[Color, float, int, str]]], 
                   list[dict[str, Union[Color, float, int, str]]], bool, bool, int, float]:
        """
        Function to evaluate paths generated by low-level a star search in environment

        Parameters
        ----------
        bomb_id_tup_dict : dict[str, list[tuple[int, Color]]]
            Dictionary mapping of agent ID to the list containing the sequence of tuples (bomb ID, Color) that the agent 
            have to execute for defusal step using corresponding color tool.
        paths : dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]
            Dictionary mapping agent ID to list containing sequence of dictionaries that represents a path. 
            Dictionary has the following structure:
            {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
             'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
             if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
             'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
             None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
             of tool being applied to bomb if action is 'apply_bomb_tool' else None}

            Possible Actions
            ----------------
            'go_to': Travel to next location (node ID) in path
            'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence
                               in task allocation
        past_bomb_id_tup_dict : Optional[dict[str, list[tuple[int, Color]]]], default=None
            Previous bomb_id_tup_dict
        past_paths : Optional[dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]], default=None
            Previous paths
        sub_bomb_dependency_order_list: Optional[list[list[int]]], default=None
            List of lists containing ordered bomb IDs according to their dependencies relevant to subtask specified by
            sub_bomb_seq_dict. Ordering is based on ordering of bomb defusal from smallest to largest index.
        sub_bomb_fuse_dict: Optional[dict[int, tuple[int, int]]], default=None
            Dictionary mapping bomb ID to bomb fuses in tuple format to represent temporal ranges for given subtask.
        render : bool, default=False 
            Whether to render environment.

        Returns
        -------
        absolute_temporal_range_conflict_list : list[dict[str, Union[Callable, float, str]]]
            List of absolute temporal range conflicts/constraints that are dictionaries of the following structure:
            {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
             'tool_color': Color, 'value': float}
        inter_goal_temporal_conflict_list : list[dict[str, Union[Color, float, int, str]]]
            List of inter-goal temporal conflicts which are dictionaries of the following structure:
            {'agent_id_1': agent ID, 'agent_id_1_bomb_id': bomb ID, agent_id_1_time': time, 
             'agent_id_1_timestep': timestep, 'agent_id_1_tool_color': Color, 'agent_id_2': agent ID, 
             'agent_id_2_bomb_id': bomb ID, 'agent_id_2_time': time, 'agent_id_2_timestep': timestep, 
             'agent_id_2_tool_color': Color, 'bomb_countdown_length': int, 'seconds_per_timestep': float}
        precedence_conflict_list : list[dict[str, Union[Color, float, int, str]]]
            List of precedence conflicts which are dictionaries of the following structure:
            {'agent_id_1': agent ID, 'agent_id_1_bomb_id': bomb ID, 'agent_id_1_time': time, 
             'agent_id_1_timestep': timestep, 'agent_id_1_tool_color': Color, 'agent_id_2': agent ID, 
             'agent_id_2_bomb_id': bomb ID, 'agent_id_2_time': time, 'agent_id_2_timestep': timestep, 
             'agent_id_2_tool_color': Color}
        env_done : bool
            Whether environment has terminated
        path_done : bool
            Whether all agents' path has terminated
        last_timestep : int
            Last timestep of environment
        score : float
            Score of the evaluated paths from the environment
        """
        # Evaluate path from start
        if past_paths == None and past_bomb_id_tup_dict == None:
            # Reset environment
            self.env_reset()
            # Boolean to track if episode has terminated
            done = False
            # Last timestep at end of recon phase
            last_timestep = self.start_timestep    
            # Render environement if desired
            if render:
                if self.save_render:
                    # Create save path for image 
                    save_path = self.save_path + f'{int(self.env.timestep - self.start_timestep)}'   
                    self.env.render(sleep=0.5, save_path=save_path, overlay_graph=self.overlay_graph)
                else:
                    self.env.render(sleep=0.5, overlay_graph=self.overlay_graph)
        # Evaluate path from starting from where past paths terminated
        else:
            # Set the environment to the point where the previous path terminated and obtain last timestep
            past_absolute_temporal_range_conflict_list, past_inter_goal_temporal_conflict_list, \
            past_precedence_conflict_list, env_done, path_done, last_timestep, _ = \
                self.evaluate_paths(paths=past_paths,
                                    bomb_id_tup_dict=copy.deepcopy(past_bomb_id_tup_dict),
                                    render=render)
            assert len(past_absolute_temporal_range_conflict_list) == 0, \
                f'Past paths should not have absolute temporal range conflicts: ' + \
                f'{past_absolute_temporal_range_conflict_list}'
            assert len(past_inter_goal_temporal_conflict_list) == 0, \
                f'Past paths should not have inter-goal temporal conflicts: {past_inter_goal_temporal_conflict_list}'
            assert len(past_precedence_conflict_list) == 0, \
                f'Past paths should not have precedence conflicts: {past_precedence_conflict_list}'
            assert env_done == False and path_done == True, \
                f'env_done = {env_done}, path_done = {path_done}. Past paths must terminate but environment should not.'
            # Boolean to track if episode has terminated
            done = False
    
        # Dictionary to track if all agent's path have terminated
        path_done_dict = {agent_id: False for agent_id in self.env.agents}
        # Evaluate path while episode has not terminated
        while not done:
            # Dictionary to store path dictionaries for current and next timestep
            curr_path_dict = {}
            next_path_dict = {}
            for agent_id, agent in self.env.agents.items():
                # No task is allocated the agent
                if len(bomb_id_tup_dict[agent_id]) == 0 or path_done_dict[agent_id]:
                    # Remain at same location
                    curr_path_dict[agent_id] = {'loc': agent.node.id,
                                                'loc_centroid': agent.node.centroid, 
                                                'action': 'go_to',
                                                'bomb_id': None, 
                                                'bomb_countdown_length': None,
                                                'bomb_sequence': None,
                                                'tool_color': None
                                               }
                    next_path_dict[agent_id] = {'loc': agent.node.id,
                                                'loc_centroid': agent.node.centroid, 
                                                'action': 'go_to',
                                                'bomb_id': None, 
                                                'bomb_countdown_length': None,
                                                'bomb_sequence': None,
                                                'tool_color': None
                                               }
                    # Highlight that this agent's path has terminated
                    path_done_dict[agent_id] = True
                # Obtain path dictionary for current and next timestep
                else:
                    if past_paths == None and past_bomb_id_tup_dict == None:
                        curr_path_dict[agent_id], _ = get_path_dict(paths[agent_id], self.env.timestep - last_timestep)
                        # Location and action at last timestep should be apply_bomb_tool action at specified location
                        # Next location and action doesn't matter (only relevant for go_to actions)
                        next_path_dict[agent_id], path_done = get_path_dict(paths[agent_id], 
                                                                            self.env.timestep + 1 - last_timestep)
                    else:
                        curr_path_dict[agent_id], _ = get_path_dict(paths[agent_id], 
                                                                    self.env.timestep - 1 - last_timestep)
                        # Location and action at last timestep should be apply_bomb_tool action at specified location
                        # Next location and action doesn't matter (only relevant for go_to actions)
                        next_path_dict[agent_id], path_done = get_path_dict(paths[agent_id], 
                                                                            self.env.timestep - 1 + 1 - last_timestep)
                    # Update path_done_list with path terminal flag
                    path_done_dict[agent_id] = path_done
            # print(f'curr_path_dict: {curr_path_dict}')
            # print(f'next_path_dict: {next_path_dict}')
            # print(f'last_timestep: {last_timestep}')
            # print(f'timestep: {self.env.timestep}')
            # print(f'time: {self.env.time}')
            # Execute one timestep in environment
            env_done, score = self.env.step(curr_path_dict=curr_path_dict, next_path_dict=next_path_dict)
            # Obtain terminal condition based on following:
            # 1) Whether environment has terminated, 2) All agents path have terminated
            done = env_done or all(path_done_dict.values())
            # print(f'path_done_dict: {path_done_dict}')
            # print(f'env_done: {env_done}')
            # print(f'done: {done}')
            # Render environement if desired
            if render:
                if self.save_render:
                    save_path = self.save_path + f'{int(self.env.timestep - self.start_timestep)}'  
                    self.env.render(sleep=0.5, save_path=save_path, overlay_graph=self.overlay_graph)
                else:
                    self.env.render(sleep=0.5, overlay_graph=self.overlay_graph)

        # CBS-TA-PTC
        if self.algo == 'cbs_ta_ptc':
            # Generate inter-goal temporal conflicts from bomb_id_defusal_info_dict
            bomb_id_defusal_info_dict = get_bomb_defusal_info(paths,
                                                              self.start_timestep, 
                                                              last_timestep, 
                                                              self.env.seconds_per_timestep)
            # print("\n BOMB DEFUSAL INFO DICT \n")
            # print(f'bomb_id_defusal_info_dict: {bomb_id_defusal_info_dict}')
            inter_goal_temporal_conflict_list = get_inter_goal_temporal_conflicts(bomb_id_defusal_info_dict, 
                                                                                  self.env.seconds_per_timestep)
            # print("\n INTER-GOAL TEMPORAL CONFLICTS \n")
            # print(f'inter_goal_temporal_conflict_list: {inter_goal_temporal_conflict_list}')
            precedence_conflict_list= get_precedence_conflicts(bomb_id_defusal_info_dict, 
                                                               self.env.seconds_per_timestep,
                                                               list(self.env.agents.keys()),
                                                               sub_bomb_dependency_order_list)
            # print("\n PRECEDENCE CONFLICTS \n")
            # print(f'sub_bomb_dependency_order_list: {sub_bomb_dependency_order_list}')
            # print(f'precedence_conflict_list: {precedence_conflict_list}')
            absolute_temporal_range_conflict_list = get_absolute_temporal_range_conflicts(bomb_id_defusal_info_dict,
                                                                                          self.env.seconds_per_timestep,
                                                                                          sub_bomb_fuse_dict)
            # print("\n ABSOLUTE TEMPORAL RANGE CONFLICTS \n")
            # print(f'sub_bomb_fuse_dict: {sub_bomb_fuse_dict}')
            # print(f'absolute_temporal_range_conflict_list: {absolute_temporal_range_conflict_list}')
            return absolute_temporal_range_conflict_list, inter_goal_temporal_conflict_list, precedence_conflict_list, \
                   env_done, all(path_done_dict.values()), int(self.env.timestep - 1), score
        # CBS-TA
        elif self.algo == 'cbs_ta':
            return [], [], [], env_done, all(path_done_dict.values()), int(self.env.timestep - 1), score

    def find_solution(self, 
                      solution_timeout: float
        ) -> tuple[dict, bool, list[float], list[float], Optional[Exception]]:
        """
        Function to find solution.
        
        Parameters
        ----------
        solution_timeout : float
            Timeout in seconds to find solution

        Returns
        -------
        solution_node : dict
            Solution node in the form of a dictionary of the following format:
            {'cost': Total path cost from all agents,
             'last_time': Last time (in seconds) from path in environment,    
             'last_timestep': Last timestep from path in environment, 
             'paths': Dictionary mapping agent ID to path of agent,
             'score': Evaluated score of path in environment,
             'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                                agent
            }
        success : bool
            Whether solver is successful finding solution given epsilon
        compute_time_list : list[float]
            List containing cpu time required to generate solution based on sub-solutions sequentially
        optimality_ratio_list : list[float]
            List containing ratio of score w.r.t max score of solution based on sub-solutions sequentially
        exception : Optional[Exception]
            Exception from find_sub_solution() if any
        """
        if self.verbose:
            print(f'Start {self.algo}')
        # List to store compute time and optimality ratio
        compute_time_list = []
        optimality_ratio_list = []
        # Start time
        self.start_time = time.time()
        # Solution node with the relevant variables
        self.solution_node = {'cost': 0,
                              'epsilon': self.epsilon,
                              'individual_cost': None,
                              'max_score': self.max_score,
                              'last_time': 0,
                              'last_timestep': self.start_timestep,
                              'paths': None,
                              'score': 0,
                              'task_allocation': None}

        # Iterate over subtasks
        for i, sub_team_bomb_tup_list in enumerate(self.team_bomb_tup_list):
            # Try to obtain solution for subtask
            try:
                sub_solution_node = \
                    self.find_sub_solution(self.start_time,
                                           solution_timeout,
                                           sub_team_bomb_tup_list,
                                           self.solution_node['last_timestep'],
                                           self.solution_node['score'],
                                           self.solution_node['paths'],
                                           self.solution_node['task_allocation'],
                                           sub_bomb_dependency_order_list=self.team_bomb_dependency_order_list[i] if \
                                                                          self.include_chained_bombs else None,
                                           sub_bomb_fuse_dict=self.team_bomb_fuse_dict_list[i] if \
                                                              self.include_fuse_bombs else None
                                          )
            except Exception as e:
                # Compute time
                compute_time_list.append(time.time() - self.start_time)
                # Optimality ratio
                optimality_ratio_list.append(self.solution_node['score'] / self.max_score)
                # Print metric/results if desired and get compute time
                if self.verbose:
                    print(f'Exception raised during find_sub_solution(): {str(e)}')
                # Solution equal/above desired bound
                if self.solution_node['score'] >= self.epsilon * self.max_score:
                    # Return whatever solution that is available given exception
                    return self.solution_node, True, compute_time_list, optimality_ratio_list, e
                else:
                    # Return whatever solution that is available given exception
                    return self.solution_node, False, compute_time_list, optimality_ratio_list, e
           
            # Add fillers to ensure that paths are of end at the same timestep
            for agent_id in sub_solution_node['paths']:
                # First sub solution node
                if self.solution_node['last_timestep'] == self.start_timestep:
                    while len(sub_solution_node['paths'][agent_id]) - 1 < \
                        sub_solution_node['last_timestep'] - self.solution_node['last_timestep']:
                        sub_solution_node['paths'][agent_id]\
                            .append({'loc': sub_solution_node['paths'][agent_id][-1]['loc'],
                                     'loc_centroid': sub_solution_node['paths'][agent_id][-1]['loc_centroid'], 
                                     'action': 'go_to',
                                     'bomb_id': None, 
                                     'bomb_countdown_length': None,
                                     'bomb_sequence': None,
                                     'tool_color': None
                                    })
                # Remaining sub solution nodes
                else:
                    while len(sub_solution_node['paths'][agent_id]) < \
                        sub_solution_node['last_timestep'] - self.solution_node['last_timestep']:
                        sub_solution_node['paths'][agent_id]\
                            .append({'loc': sub_solution_node['paths'][agent_id][-1]['loc'],
                                     'loc_centroid': sub_solution_node['paths'][agent_id][-1]['loc_centroid'], 
                                     'action': 'go_to',
                                     'bomb_id': None, 
                                     'bomb_countdown_length': None,
                                     'bomb_sequence': None,
                                     'tool_color': None
                                    })
           
            # Update solution node
            self.solution_node['cost'] += sub_solution_node['cost']
            self.solution_node['last_time'] = sub_solution_node['last_timestep'] * self.env.seconds_per_timestep
            self.solution_node['last_timestep'] = sub_solution_node['last_timestep']
            self.solution_node['score'] = sub_solution_node['score']
            if i == 0:
                self.solution_node['individual_cost'] = sub_solution_node['individual_cost'].copy()
                self.solution_node['paths'] = copy.deepcopy(sub_solution_node['paths'])
                self.solution_node['task_allocation'] = copy.deepcopy(sub_solution_node['task_allocation'])
            else:
                for agent_id in self.env.agents:
                    self.solution_node['individual_cost'][agent_id] += \
                        sub_solution_node['individual_cost'][agent_id]
                    self.solution_node['paths'][agent_id] += sub_solution_node['paths'][agent_id]
                    self.solution_node['task_allocation'][agent_id] += \
                        sub_solution_node['task_allocation'][agent_id]
           
            # Print metrics if desired
            if self.verbose:
                print(f'\n Solution for subtask {i} found! \n')
                self.print_results(True)

            # Evaluate current solution node
            _, _, _, env_done, _, _, _ = \
                self.evaluate_paths(bomb_id_tup_dict=copy.deepcopy(self.solution_node['task_allocation']),
                                    paths=self.solution_node['paths'])

            # If environment has terminated already given current paths and not last iteration
            if env_done == True and i < len(self.team_bomb_tup_list) - 1:
                # Solution equal/above desired bound
                if self.solution_node['score'] >= self.epsilon * self.max_score:
                    # Print metric/results if desired and get compute time
                    if self.verbose:
                        print("\n Found a solution above desired score bound! \n")
                        compute_time = self.print_results(True)
                        compute_time_list.append(compute_time)
                    else:
                        # Compute time
                        compute_time_list.append(time.time() - self.start_time)
                    # Optimality ratio
                    optimality_ratio_list.append(self.solution_node['score'] / self.max_score)
                    return self.solution_node, True, compute_time_list, optimality_ratio_list, None
                # Solution below desired bound
                else:
                    # Print metric/results if desired and get compute time
                    if self.verbose:
                        print("\n Solution below desired score bound found \n")
                        compute_time = self.print_results(True)
                        compute_time_list.append(compute_time)
                    else:
                        # Compute time
                        compute_time_list.append(time.time() - self.start_time)
                    # Optimality ratio
                    optimality_ratio_list.append(self.solution_node['score'] / self.max_score)
                    return self.solution_node, False, compute_time_list, optimality_ratio_list, None
            else:
                # Compute time
                compute_time_list.append(time.time() - self.start_time)
                # Optimality ratio
                optimality_ratio_list.append(self.solution_node['score'] / self.max_score)

        # Evaluate final solution node
        absolute_temporal_range_conflicts, inter_goal_temporal_conflicts, precedence_conflicts, env_done, path_done, \
        _, score = self.evaluate_paths(bomb_id_tup_dict=copy.deepcopy(self.solution_node['task_allocation']), 
                                       paths=self.solution_node['paths'])
        assert len(absolute_temporal_range_conflicts) == 0, 'There must be zero absolute temporal range conflicts'
        assert len(inter_goal_temporal_conflicts) == 0, 'There must be zero inter-goal temporal conflicts'
        assert len(precedence_conflicts) == 0, 'There must be zero precedence conflicts'
        assert env_done == True, 'Environment must terminate'
        assert path_done == True, 'All agents paths must terminate'
        assert score == self.solution_node['score'], \
            f"Evaluated score of {score} is not equal to solution node score of {self.solution_node['score']}"
        for agent_id in self.solution_node['paths']:
            assert len(self.solution_node['paths'][agent_id]) - 1 + self.start_timestep \
                == self.solution_node['last_timestep'], \
                f"Path of agent {agent_id} has timestep of {len(self.solution_node['paths'][agent_id]) - 1}, " + \
                f"which is not equal to the last timestep from environment of {self.solution_node['last_timestep']}"
        if self.include_collisions:
            collisions = detect_collisions(self.solution_node['paths'])
            assert len(collisions) == 0, 'There must be zero collisions'

        # Solution equal/above desired bound
        if self.solution_node['score'] >= self.epsilon * self.max_score:
            # Print metric/results if desired and get compute time
            if self.verbose:
                print("\n Found a solution above desired score bound! \n")
                compute_time = self.print_results(True)
                compute_time_list.append(compute_time)
            else:
                # Compute time
                compute_time_list.append(time.time() - self.start_time)
            # Optimality ratio
            optimality_ratio_list.append(self.solution_node['score'] / self.max_score)
            return self.solution_node, True, compute_time_list, optimality_ratio_list, None
        # Solution below desired bound
        else:
            # Print metric/results if desired and get compute time
            if self.verbose:
                print("\n Solution below desired score bound found \n")
                compute_time = self.print_results(True)
                compute_time_list.append(compute_time)
            else:
                # Compute time
                compute_time_list.append(time.time() - self.start_time)
            # Optimality ratio
            optimality_ratio_list.append(self.solution_node['score'] / self.max_score)
            return self.solution_node, False, compute_time_list, optimality_ratio_list, None

    def find_sub_solution(
            self,
            start_time: float,
            solution_timeout: float, 
            sub_team_bomb_tup_list: list[tuple[int, Color]],
            past_last_timestep: int,
            past_score: float,
            past_paths: Optional[dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]] = None,
            past_bomb_id_tup_dict: Optional[dict[str, list[tuple[int, Color]]]] = None,
            sub_bomb_dependency_order_list: Optional[list[list[int]]] = None,
            sub_bomb_fuse_dict: Optional[dict[int, tuple[int, int]]] = None                  
        ) -> dict[str, Union[float, list, dict]]:
        """
        Function to find solution to subtask

        Parameters
        ----------
        start_time : float
            Start time of solver
        solution_timeout : float
            Timeout in seconds to find solution
        sub_team_bomb_tup_list : list[tuple[int, Color]]
            List of task sequences of (bomb ID, Color) to be allocated to agents for the subtask.
        past_last_timestep : int
            Last timestep of previous solution
        past_score : float
            Previous score
        past_paths : Optional[dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]], default=None
            Previous paths. paths is a dictionary mapping agent ID to list containing sequence of dictionaries that 
            represents a path. Dictionary has the following structure:
            {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
             'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
             if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
             'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
             None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
             of tool being applied to bomb if action is 'apply_bomb_tool' else None}

            Possible Actions
            ----------------
            'go_to': Travel to next location (node ID) in path
            'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence
                               in task allocation
        past_bomb_id_tup_dict : Optional[dict[str, list[tuple[int, Color]]]], default=None
            Previous bomb_id_tup_dict. bomb_id_tup_dict is the dictionary mapping of agent ID to the list containing the 
            sequence of tuples (bomb ID, Color) that the agent have to execute for defusal step using corresponding 
            color tool.
        sub_bomb_dependency_order_list: Optional[list[list[int]]], default=None
            List of lists containing ordered bomb IDs according to their dependencies relevant to subtask specified by
            sub_bomb_seq_dict. Ordering is based on ordering of bomb defusal from smallest to largest index.
        sub_bomb_fuse_dict: Optional[dict[int, tuple[int, int]]], default=None
            Dictionary mapping bomb ID to bomb fuses in tuple format to represent temporal ranges for given subtask.

        Returns
        -------
        node : dict[str, Union[float, list, dict]]
            Node to be pushed to the list with the following parameters:
            {'collisions': List of collisions, 
             'conflicts': List of conflicts (a conflict is basically a list of constraints due to condition 3 and 4), 
             'constraints': List of constraints, 
             'cost': Total path cost from all agents, 
             'individual_cost': Dictionary mapping agent ID to respective agent's path cost, 
             'paths': Dictionary mapping agent ID to path of agent,
             'root': If node is root node, 
             'score': Evaluated score of path in environment,
             'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                                agent
            }
        """
        # Get root nodes given sub task allocation push to open_root_list
        self.get_root_nodes(
            start_time=start_time,
            solution_timeout=solution_timeout, 
            sub_team_bomb_tup_list=sub_team_bomb_tup_list,
            past_last_timestep=past_last_timestep,
            past_paths=past_paths,
            past_score=past_score,
            past_bomb_id_tup_dict=past_bomb_id_tup_dict,
            sub_bomb_dependency_order_list=sub_bomb_dependency_order_list,
            sub_bomb_fuse_dict=sub_bomb_fuse_dict  
        )
        
        # Obtain the best target sequence from open_root_list and push to open_list
        if len(self.open_root_list) > 0:
            best_root = self.pop_node(root=True)
            self.push_node(node=best_root, root=False)
        # Scenario has no solution
        else:
            if self.verbose: 
                self.print_results(False)
            raise Exception(f'Subtask with task allocation {sub_team_bomb_id_list} has no solution')

        # Iterate while open list not empty and within solution timeout
        while len(self.open_list) > 0 and time.time() - start_time <= solution_timeout:
            # Pop node from open list
            node = self.pop_node(root=False)    
            # Return node based on score lower bound
            if node['score'] >= self.epsilon * (10 * len(sub_team_bomb_tup_list) + past_score):
                # Reset lists and variables
                self.reset()
                return node
            # Push next best root node to open_list if popped node is a root node
            if node['root'] == True and len(self.open_root_list) > 0:
                best_root = self.pop_node(root=True)
                self.push_node(node=best_root, root=False)
            # Resolve conflicts and get list of constraints
            constraint_list = self.resolve_conflicts(node=node)
            # Iterate over constraints
            for constraint in constraint_list:
                # Copy paths, individual paths costs and task allocation (constant)
                new_node_paths = copy.deepcopy(node['paths'])
                new_node_path_cost_dict = node['individual_cost'].copy()
                new_node_bomb_id_tup_dict = copy.deepcopy(node['task_allocation'])
                # CBS-TA-PTC
                if self.algo == 'cbs_ta_ptc':
                    new_node_absolute_temporal_range_completion_timestep_constraints, \
                    new_node_collision_constraints, new_node_inter_goal_temporal_completion_timestep_constraints, \
                    new_node_precedence_completion_timestep_constraints = \
                        get_new_node_constraints(algo=self.algo, node=node, constraint=constraint)
                    # Generate path and its cost for given agent given current task assignment and new constraints
                    # Accounts for past paths and task assignment
                    new_agent_path, new_agent_path_cost = \
                        self.get_new_path_and_cost(
                            agent_id=constraint['agent_id'], 
                            bomb_id_tup_dict=new_node_bomb_id_tup_dict, 
                            collision_constraint_list=new_node_collision_constraints,
                            past_bomb_id_tup_dict=past_bomb_id_tup_dict,
                            past_last_timestep=past_last_timestep,
                            past_paths=past_paths,
                            absolute_temporal_range_completion_timestep_constraint_list=\
                                new_node_absolute_temporal_range_completion_timestep_constraints,
                            inter_goal_temporal_completion_timestep_constraint_list=\
                                new_node_inter_goal_temporal_completion_timestep_constraints,
                            precedence_completion_timestep_constraint_list=\
                                new_node_precedence_completion_timestep_constraints,
                            return_constraint_list=[]  
                        )
                # CBS-TA
                elif self.algo == 'cbs_ta':
                    new_node_collision_constraints, new_node_return_constraints = \
                        get_new_node_constraints(algo=self.algo, node=node, constraint=constraint)
                    # Generate path and its cost for given agent given current task assignment and new constraints
                    # Accounts for past paths and task assignment
                    new_agent_path, new_agent_path_cost = \
                        self.get_new_path_and_cost(
                            agent_id=constraint['agent_id'], 
                            bomb_id_tup_dict=new_node_bomb_id_tup_dict, 
                            collision_constraint_list=new_node_collision_constraints,
                            past_bomb_id_tup_dict=past_bomb_id_tup_dict,
                            past_last_timestep=past_last_timestep,
                            past_paths=past_paths,
                            absolute_temporal_range_completion_timestep_constraint_list=[],
                            inter_goal_temporal_completion_timestep_constraint_list=[],
                            precedence_completion_timestep_constraint_list=[],
                            return_constraint_list=new_node_return_constraints  
                        )
                # If new agent path exist
                if new_agent_path is not None:
                    # Update paths with new agent path
                    new_node_paths[constraint['agent_id']] = copy.deepcopy(new_agent_path)
                    # Obtain new cost of paths and update cost of new agent path
                    new_node_cost = node['cost'] + new_agent_path_cost - node['individual_cost'][constraint['agent_id']]
                    new_node_path_cost_dict[constraint['agent_id']] = new_agent_path_cost
                    # Evaluate score and inter-goal temporal conflicts for new set of paths
                    new_node_absolute_temporal_range_conflicts, new_node_inter_goal_temporal_conflicts, \
                    new_node_precedence_conflicts, new_node_env_done, new_node_path_done, new_node_last_timestep, \
                    new_node_score = \
                        self.evaluate_paths(bomb_id_tup_dict=new_node_bomb_id_tup_dict,
                                            paths=new_node_paths, 
                                            past_bomb_id_tup_dict=past_bomb_id_tup_dict,
                                            past_paths=past_paths,
                                            sub_bomb_dependency_order_list=sub_bomb_dependency_order_list,
                                            sub_bomb_fuse_dict=sub_bomb_fuse_dict)
                    # Obtain new collisions if desired
                    if self.include_collisions:
                        new_node_collisions = detect_collisions(new_node_paths)
                    else:
                        new_node_collisions = []
                    # CBS-TA-PTC
                    if self.algo == 'cbs_ta_ptc':
                        new_node = {
                            'absolute_temporal_range_conflicts': new_node_absolute_temporal_range_conflicts,
                            'absolute_temporal_range_completion_timestep_constraints': \
                                new_node_absolute_temporal_range_completion_timestep_constraints, 
                            'collisions': new_node_collisions,
                            'collisions_constraints': new_node_collision_constraints,
                            'cost': new_node_cost,
                            'env_done': new_node_env_done,
                            'individual_cost': new_node_path_cost_dict,
                            'inter_goal_temporal_conflicts': new_node_inter_goal_temporal_conflicts,
                            'inter_goal_temporal_completion_timestep_constraints': \
                                new_node_inter_goal_temporal_completion_timestep_constraints,
                            'last_timestep': new_node_last_timestep,
                            'paths': new_node_paths,
                            'path_done': new_node_path_done,
                            'precedence_conflicts': new_node_precedence_conflicts, 
                            'precedence_completion_timestep_constraints': \
                                new_node_precedence_completion_timestep_constraints,
                            'root': False, 
                            'score': new_node_score,
                            'task_allocation': new_node_bomb_id_tup_dict
                        }
                    # CBS-TA
                    elif self.algo == 'cbs_ta':
                        new_node_return_conflicts = []
                        # Generate return conflicts if score is below desired bound
                        if new_node_score < self.epsilon * (10 * len(sub_team_bomb_tup_list) + past_score):
                            new_node_return_conflicts = get_return_conflicts(last_timestep=new_node_last_timestep,
                                                                             paths=new_node_paths, 
                                                                             past_paths=past_paths)
                        new_node = {
                            'collisions': new_node_collisions,
                            'collisions_constraints': new_node_collision_constraints,
                            'cost': new_node_cost,
                            'env_done': new_node_env_done,
                            'individual_cost': new_node_path_cost_dict,
                            'last_timestep': new_node_last_timestep,
                            'paths': new_node_paths,
                            'path_done': new_node_path_done,
                            'return_conflicts': new_node_return_conflicts, 
                            'return_constraints': new_node_return_constraints,
                            'root': False, 
                            'score': new_node_score,
                            'task_allocation': new_node_bomb_id_tup_dict
                        }
                    # Push new node to open list
                    self.push_node(new_node, root=False)
        # Timeout
        if time.time() - start_time > solution_timeout:
            raise Exception(f'Timeout given solution timeout of {solution_timeout} seconds!')
        # Actually can't find solution
        else:
            self.print_results(False)
            raise Exception(f'Subtask with task allocation {sub_team_bomb_tup_list} has no solutions')

    def get_new_path_and_cost(
            self,
            agent_id: str, 
            bomb_id_tup_dict: dict[str, list[tuple[int, Color]]], 
            collision_constraint_list: list[dict[str, Union[int, list[int], float, str]]],
            past_bomb_id_tup_dict: Optional[dict[str, list[tuple[int, Color]]]] = None,
            past_last_timestep: int = 0,
            past_paths: Optional[dict[str, list[dict[str, Union[str, int]]]]] = None,
            absolute_temporal_range_completion_timestep_constraint_list: \
                list[dict[str, Union[Callable, Color, float, str]]] = [],
            inter_goal_temporal_completion_timestep_constraint_list: \
                list[dict[str, Union[Callable, Color, float, str]]] = [],
            precedence_completion_timestep_constraint_list: list[dict[str, Union[Callable, Color, float, str]]] = [],
            return_constraint_list: list[dict[str, Union[int, str]]] = []
        ) -> tuple[Optional[list[dict[str, Union[int, str, None, Color, tuple[Color]]]]], Optional[float]]:
        """
        Function to generate new path and cost from past paths for an agent under constraints

        Parameters
        ----------
        agent_id : str
            Agent ID
        bomb_id_tup_dict : dict[str, list[tuple[int, Color]]]
            Dictionary mapping of agent ID to the list containing the sequence of tuples (bomb ID, Color) that the agent 
            have to execute for defusal step using corresponding color tool.
        collision_constraint_list : list[dict[str, Union[int, list[int], float, str]]]
            List of dictionary containing collision constraints of the following format:
            {'agent': agent ID, 'constraint_type': 'edge_collision'/'vertex_collision', 
             'loc': location (node ID) of vertex (list of len 1)/edge collision (list of len 2), 
             'timestep': float (at the end for edge collision)}
        past_bomb_id_tup_dict : Optional[dict[str, list[tuple[int, Color]]]]
            Previous bomb_id_tup_dict. bomb_id_tup_dict is the dictionary mapping of agent ID to the list containing the 
            sequence of tuples (bomb ID, Color) that the agent have to execute for defusal step using corresponding 
            color tool.
        past_last_timestep : int, default=0
            Last timestep of previous solution     
        past_paths : Optional[dict[str, list[dict[str, Union[str, int]]]]]
            Previous paths. paths is a dictionary mapping of agent ID to the path of the agent, which is a list 
            containing sequence of dictionaries with location (node ID) and the action to be implemented at the 
            ocation (node ID) that represents a path. Dictionary has the following structure:
            {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
             'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
             if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
             'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
             None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
             of tool being applied to bomb if action is 'apply_bomb_tool' else None}

            Possible Actions
            ----------------
            'go_to': Travel to next location (node ID) in path
            'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence 
                               in task allocation
        absolute_temporal_range_completion_timestep_constraint_list : list[dict[str, Union[Callable, Color, float, str]]]
            List of absolute temporal range completion timestep constraints that are dictionaries of the following 
            structure:
            {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
             'tool_color': Color, 'value': float}
        inter_goal_temporal_completion_timestep_constraint_list : list[dict[str, Union[Callable, Color, float, str]]]
            List of inter-goal temporal completion timestep constraints that are dictionaries of the following 
            structure:
            {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
             'tool_color': Color, 'value': float}
        precedence_completion_timestep_constraint_list : list[dict[str, Union[Callable, Color, float, str]]]
            List of precedence completion timestep constraints that are dictionaries of the following structure:
            {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
             'tool_color': Color, 'value': float}
        return_conflict_list : list[dict[str, Union[int, str]]]
            List containing return conflicts (vertex constraints) based on dictionary of following structure:
            {'agent_id': agent ID, 'conflict_type': str, 'loc': node ID, 'timestep': timestep} 
        
        Returns
        -------
        agent_path : Optional[list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]
            List containing sequence of dictionaries with location (node ID) and the action to be implemented at the 
            location (node ID) that represents a path. Global path consists of all the node IDs of nodes to be visited 
            by node_ids_list. Dictionary has the following structure:
            {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
             'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
             if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
             'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
             None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
             of tool being applied to bomb if action is 'apply_bomb_tool' else None}

            Possible Actions
            ----------------
            'go_to': Travel to next location (node ID) in path
            'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence 
                               in task allocation
        agent_path_cost : Optional[float]
            Cost of agent_path
        """
        # Obtain list of bomb IDs
        bomb_ids_list = [tup[0] for tup in bomb_id_tup_dict[agent_id]]
        # Convert list of bomb IDs to list of node IDs where the bombs are located
        node_ids_list = self.bomb_id_to_node_id(bomb_ids_list)

        # Multi-step a star search
        if self.low_level_search == 'multi_step_a_star':
            # Account for the fact that no task is allocated the agent
            if len(node_ids_list) == 0:
                # All agents at start node if no past_paths are given
                # Move to same location (i.e start node)
                if past_paths == None and past_bomb_id_tup_dict == None: 
                    agent_path, agent_path_cost = \
                        self.env.multi_step_a_star(
                            agent_id=agent_id,
                            algo=self.algo,
                            bomb_tup_list=copy.deepcopy(bomb_id_tup_dict[agent_id]),
                            collision_constraint_list=collision_constraint_list,
                            node_ids_list=[self.env.start_nodes[agent_id].id],
                            past_last_timestep=past_last_timestep,
                            start_loc=self.env.start_nodes[agent_id].id, 
                            absolute_temporal_range_completion_timestep_constraint_list=\
                                absolute_temporal_range_completion_timestep_constraint_list, 
                            inter_goal_temporal_completion_timestep_constraint_list=\
                                inter_goal_temporal_completion_timestep_constraint_list,
                            precedence_completion_timestep_constraint_list=\
                                precedence_completion_timestep_constraint_list,
                            return_constraint_list=return_constraint_list      
                        )
                # Moving from last known location from past_paths
                # Move to same location (i.e last known location from past_paths)
                else:
                    agent_path, agent_path_cost = \
                        self.env.multi_step_a_star(
                            agent_id=agent_id,
                            algo=self.algo,
                            bomb_tup_list=copy.deepcopy(bomb_id_tup_dict[agent_id]),
                            collision_constraint_list=collision_constraint_list,
                            node_ids_list=[past_paths[agent_id][-1]['loc']],
                            past_last_timestep=past_last_timestep,
                            start_loc=past_paths[agent_id][-1]['loc'], 
                            absolute_temporal_range_completion_timestep_constraint_list=\
                                absolute_temporal_range_completion_timestep_constraint_list,
                            inter_goal_temporal_completion_timestep_constraint_list=\
                                inter_goal_temporal_completion_timestep_constraint_list,  
                            precedence_completion_timestep_constraint_list=\
                                precedence_completion_timestep_constraint_list,
                            return_constraint_list=return_constraint_list
                        )
            # Task allocated to agent
            else:
                # All agents at start node if no past_paths are given
                if past_paths == None and past_bomb_id_tup_dict == None:
                    agent_path, agent_path_cost = \
                        self.env.multi_step_a_star(
                            agent_id=agent_id,
                            algo=self.algo,
                            bomb_tup_list=copy.deepcopy(bomb_id_tup_dict[agent_id]),
                            collision_constraint_list=collision_constraint_list,
                            node_ids_list=node_ids_list,
                            past_last_timestep=past_last_timestep,
                            start_loc=self.env.start_nodes[agent_id].id, 
                            absolute_temporal_range_completion_timestep_constraint_list=\
                                absolute_temporal_range_completion_timestep_constraint_list,
                            inter_goal_temporal_completion_timestep_constraint_list=\
                                inter_goal_temporal_completion_timestep_constraint_list,
                            precedence_completion_timestep_constraint_list=\
                                precedence_completion_timestep_constraint_list,
                            return_constraint_list=return_constraint_list  
                        )
                # Moving from last known location from past_paths
                else:
                    agent_path, agent_path_cost = \
                        self.env.multi_step_a_star(
                            agent_id=agent_id,
                            algo=self.algo,
                            bomb_tup_list=copy.deepcopy(bomb_id_tup_dict[agent_id]),
                            collision_constraint_list=collision_constraint_list,
                            node_ids_list=node_ids_list,
                            past_last_timestep=past_last_timestep,
                            start_loc=past_paths[agent_id][-1]['loc'], 
                            absolute_temporal_range_completion_timestep_constraint_list=\
                                absolute_temporal_range_completion_timestep_constraint_list,
                            inter_goal_temporal_completion_timestep_constraint_list=\
                                inter_goal_temporal_completion_timestep_constraint_list,
                            precedence_completion_timestep_constraint_list=\
                                precedence_completion_timestep_constraint_list,
                            return_constraint_list=return_constraint_list    
                        )
        # Multi-label a star
        elif self.low_level_search == 'multi_label_a_star':
            # Account for the fact that no task is allocated the agent
            if len(node_ids_list) == 0:
                # All agents at start node if no past_paths are given
                # Move to same location (i.e start node)
                if past_paths == None and past_bomb_id_tup_dict == None: 
                    agent_path, agent_path_cost = \
                        self.env.multi_label_a_star(
                            agent_id=agent_id,
                            algo=self.algo,
                            bomb_tup_list=copy.deepcopy(bomb_id_tup_dict[agent_id]),
                            collision_constraint_list=collision_constraint_list,
                            node_ids_list=[self.env.start_nodes[agent_id].id],
                            past_last_timestep=past_last_timestep,
                            start_loc=self.env.start_nodes[agent_id].id, 
                            absolute_temporal_range_completion_timestep_constraint_list=\
                                absolute_temporal_range_completion_timestep_constraint_list,
                            inter_goal_temporal_completion_timestep_constraint_list=\
                                inter_goal_temporal_completion_timestep_constraint_list,
                            precedence_completion_timestep_constraint_list=\
                                precedence_completion_timestep_constraint_list,
                            return_constraint_list=return_constraint_list
                        )
                # Moving from last known location from past_paths
                # Move to same location (i.e last known location from past_paths)
                else:
                    agent_path, agent_path_cost = \
                        self.env.multi_label_a_star(
                            agent_id=agent_id,
                            algo=self.algo,
                            bomb_tup_list=copy.deepcopy(bomb_id_tup_dict[agent_id]),
                            collision_constraint_list=collision_constraint_list,
                            node_ids_list=[past_paths[agent_id][-1]['loc']],
                            past_last_timestep=past_last_timestep,
                            start_loc=past_paths[agent_id][-1]['loc'], 
                            absolute_temporal_range_completion_timestep_constraint_list=\
                                absolute_temporal_range_completion_timestep_constraint_list,
                            inter_goal_temporal_completion_timestep_constraint_list=\
                                inter_goal_temporal_completion_timestep_constraint_list,
                            precedence_completion_timestep_constraint_list=\
                                precedence_completion_timestep_constraint_list,
                            return_constraint_list=return_constraint_list    
                        )
            # Task allocated to agent
            else:
                # All agents at start node if no past_paths are given
                if past_paths == None and past_bomb_id_tup_dict == None:
                    agent_path, agent_path_cost = \
                        self.env.multi_label_a_star(
                            agent_id=agent_id,
                            algo=self.algo,
                            bomb_tup_list=copy.deepcopy(bomb_id_tup_dict[agent_id]),
                            collision_constraint_list=collision_constraint_list,
                            node_ids_list=node_ids_list,
                            past_last_timestep=past_last_timestep,
                            start_loc=self.env.start_nodes[agent_id].id, 
                            absolute_temporal_range_completion_timestep_constraint_list=\
                                absolute_temporal_range_completion_timestep_constraint_list,
                            inter_goal_temporal_completion_timestep_constraint_list=\
                                inter_goal_temporal_completion_timestep_constraint_list,
                            precedence_completion_timestep_constraint_list=\
                                precedence_completion_timestep_constraint_list,
                            return_constraint_list=return_constraint_list    
                        )
                # Moving from last known location from past_paths
                else:
                    agent_path, agent_path_cost = \
                        self.env.multi_label_a_star(
                            agent_id=agent_id,
                            algo=self.algo,
                            bomb_tup_list=copy.deepcopy(bomb_id_tup_dict[agent_id]),
                            collision_constraint_list=collision_constraint_list,
                            node_ids_list=node_ids_list,
                            past_last_timestep=past_last_timestep,
                            start_loc=past_paths[agent_id][-1]['loc'], 
                            absolute_temporal_range_completion_timestep_constraint_list=\
                                absolute_temporal_range_completion_timestep_constraint_list,
                            inter_goal_temporal_completion_timestep_constraint_list=\
                                inter_goal_temporal_completion_timestep_constraint_list, 
                            precedence_completion_timestep_constraint_list=\
                                precedence_completion_timestep_constraint_list,
                            return_constraint_list=return_constraint_list   
                        )
        else:
            raise NotImplementedError(f'Low-level search {self.low_level_search} is not implemented.')
        return agent_path, agent_path_cost

    def get_root_nodes(
            self,
            start_time: float,
            solution_timeout: float, 
            sub_team_bomb_tup_list: list[tuple[int, Color]],
            past_last_timestep: int,
            past_score: float,
            past_paths: Optional[dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]] = None,
            past_bomb_id_tup_dict: Optional[dict[str, list[tuple[int, Color]]]] = None,
            sub_bomb_dependency_order_list: Optional[list[list[int]]] = None,
            sub_bomb_fuse_dict: Optional[dict[int, tuple[int, int]]] = None  
        ):
        """
        Function to get root nodes given sub task allocation push to open list for root nodes.

        Parameters
        ----------
        start_time : float
            Start time of solver
        solution_timeout : float
            Timeout in seconds to find solution
        sub_team_bomb_tup_list : list[tuple[int, Color]]
            List of task sequences of (bomb ID, Color) to be allocated to agents for the subtask.
        past_last_timestep : int
            Last timestep of previous solution
        past_score : float
            Previous score
        past_paths : Optional[dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]], default=None
            Previous paths. paths is a dictionary mapping agent ID to list containing sequence of dictionaries that 
            represents a path. Dictionary has the following structure:
            {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
             'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
             if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
             'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
             None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
             of tool being applied to bomb if action is 'apply_bomb_tool' else None}

            Possible Actions
            ----------------
            'go_to': Travel to next location (node ID) in path
            'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence
                               in task allocation
        past_bomb_id_tup_dict : Optional[dict[str, list[tuple[int, Color]]]], default=None
            Previous bomb_id_tup_dict. bomb_id_tup_dict is the dictionary mapping of agent ID to the list containing the 
            sequence of tuples (bomb ID, Color) that the agent have to execute for defusal step using corresponding 
            color tool.
        sub_bomb_dependency_order_list: Optional[list[list[int]]], default=None
            List of lists containing ordered bomb IDs according to their dependencies relevant to subtask specified by
            sub_bomb_seq_dict. Ordering is based on ordering of bomb defusal from smallest to largest index.
        sub_bomb_fuse_dict: Optional[dict[int, tuple[int, int]]], default=None
            Dictionary mapping bomb ID to bomb fuses in tuple format to represent temporal ranges for given subtask.
        """
        # Generate a task sequence for each agent and push to open_root_list
        for allocation in generate_allocations(sub_team_bomb_tup_list, len(self.env.agents)):
            bomb_id_tup_dict = {}
            paths = {}
            path_cost_dict = {}
            paths_cost = 0
            # Iterate over agents and assign task sequence
            for i, agent_id in enumerate(self.env.agents):
                # Obtain task sequence in terms of tuples of (bomb ID, Color) for specific agent
                bomb_id_tup_dict[agent_id] = allocation[i]
            # Prune allocation based on heuristics
            if not self.is_valid_bomb_allocation(bomb_id_tup_dict) \
                or not self.is_optimal_bomb_allocation(bomb_id_tup_dict, self.pruning_heuristic):
                continue
            # Boolean to track if path exist for all agents
            path_exist = True
            # Iterate over agents and assign task sequence
            for agent_id in self.env.agents:
                # Generate path and its cost for given agent given current task assignment and constraints
                # Accounts for past paths and task assignment
                agent_path, agent_path_cost = \
                    self.get_new_path_and_cost(
                        agent_id=agent_id, 
                        bomb_id_tup_dict=bomb_id_tup_dict,
                        collision_constraint_list=[],
                        past_bomb_id_tup_dict=past_bomb_id_tup_dict,
                        past_last_timestep=past_last_timestep,
                        past_paths=past_paths,
                        absolute_temporal_range_completion_timestep_constraint_list=[],
                        inter_goal_temporal_completion_timestep_constraint_list=[],
                        precedence_completion_timestep_constraint_list=[],
                        return_constraint_list=[]
                    )
                # Path doesn't exist for this particular agent
                if agent_path == None:
                    path_exist = False
                    break
                paths[agent_id] = agent_path
                path_cost_dict[agent_id] = agent_path_cost
                paths_cost += agent_path_cost
            # Skip if path doesn't exist for any agent
            if not path_exist:
                continue
            # Generate collisions if desired
            if self.include_collisions:
                collisions = detect_collisions(paths)
            else: 
                collisions = []
            # Evaluate score of paths through rollouts in environment
            absolute_temporal_range_conflicts, inter_goal_temporal_conflicts, precedence_conflicts, env_done, \
            path_done, last_timestep, score = \
                self.evaluate_paths(
                    bomb_id_tup_dict=bomb_id_tup_dict,
                    paths=paths,
                    past_bomb_id_tup_dict=past_bomb_id_tup_dict, 
                    past_paths=past_paths,
                    sub_bomb_dependency_order_list=sub_bomb_dependency_order_list,
                    sub_bomb_fuse_dict=sub_bomb_fuse_dict
                )
            assert env_done == True or path_done == True, 'Either environment or paths must terminate'

            # CBS-TA-PTC
            if self.algo == 'cbs_ta_ptc':
                # Generate root node
                root = {'absolute_temporal_range_conflicts': absolute_temporal_range_conflicts,
                        'absolute_temporal_range_completion_timestep_constraints': [], 
                        'collisions': collisions,
                        'collisions_constraints': [],
                        'cost': paths_cost,
                        'env_done': env_done,
                        'individual_cost': path_cost_dict,
                        'inter_goal_temporal_conflicts': inter_goal_temporal_conflicts,
                        'inter_goal_temporal_completion_timestep_constraints': [],
                        'last_timestep': last_timestep,
                        'paths': paths,
                        'path_done': path_done,
                        'precedence_conflicts': precedence_conflicts,
                        'precedence_completion_timestep_constraints': [], 
                        'root': True, 
                        'score': score,
                        'task_allocation': bomb_id_tup_dict
                       }
            # CBS-TA
            elif self.algo == 'cbs_ta':
                return_conflicts = []
                # Generate return conflicts if score is below desired bound
                if score < self.epsilon * (10 * len(sub_team_bomb_tup_list) + past_score):
                    return_conflicts = get_return_conflicts(last_timestep=last_timestep,
                                                            paths=paths, 
                                                            past_paths=past_paths)
                # Generate root node
                root = {'collisions': collisions,
                        'collisions_constraints': [],
                        'cost': paths_cost,
                        'env_done': env_done,
                        'individual_cost': path_cost_dict,
                        'last_timestep': last_timestep,
                        'paths': paths,
                        'path_done': path_done,
                        'return_conflicts': return_conflicts,
                        'return_constraints': [], 
                        'root': True, 
                        'score': score,
                        'task_allocation': bomb_id_tup_dict
                       }
            # Push root to open_root_list
            self.push_node(node=root, root=True)
            # Check for solution timeout
            if time.time() - start_time > solution_timeout:
                raise Exception(f'Timeout given solution timeout of {solution_timeout} seconds!')

    def is_optimal_bomb_allocation(self, 
                                   bomb_id_tup_dict: dict[str, list[tuple[int, Color]]], 
                                   pruning_heuristic: str
        ) -> bool:
        """
        Function to check task allocation to each agent is "optimal" (SUBJECT TO UPDATES TO GYM_DRAGON) based on the 
        following possible heuristics:

            1) 'even_bomb_allocation' : Bomb sequence tasks must evenly distributed amongst agents
            2) 'none' : No pruning
        
        Parameters
        ----------
        bomb_id_tup_dict : dict[str, list[tuple[int, Color]]]
            Dictionary mapping of agent ID to the list containing the sequence of tuples (bomb ID, Color) that the agent 
            have to execute for defusal step using corresponding color tool.
        pruning_heuristic : str
            Heuristic to prune the solution.

        Returns
        -------
        optimal : bool 
            Whether sub task allocation is optimal.
        """
        # Reset environment
        self.env_reset()
        return self.env.is_optimal_bomb_allocation(bomb_id_tup_dict, pruning_heuristic)

    def is_solvable(self) -> bool:
        """
        Function to check if the entire task allocation is solvable given the tool set of all the agents.

        Returns
        -------
        solvable : bool 
            Whether entire task allocation is solvable given the tool set of all the agents.
        """
        # Reset environment
        self.env_reset()
        return self.env.is_solvable()

    def is_valid_bomb_allocation(self, bomb_id_tup_dict: dict[str, list[tuple[int, Color]]]) -> bool:
        """
        Function to check if the sub task allocation is valid. 
        Refer to is_valid_bomb_allocation() in DragonBaseEnvSearch() for details.

        Parameters
        ----------
        bomb_id_tup_dict : dict[str, list[tuple[int, Color]]]
            Dictionary mapping of agent ID to the list containing the sequence of tuples (bomb ID, Color) that the agent 
            have to execute for defusal step using corresponding color tool.

        Returns
        -------
        valid : bool 
            Whether sub task allocation is valid.
        """
        # Reset environment
        self.env_reset()
        return self.env.is_valid_bomb_allocation(bomb_id_tup_dict)

    def pop_node(self, root: bool) -> dict[str, Union[float, list, dict]]:
        """
        Function to pop node from heap based on the following priorities for respective algorithm:

            CBS-TA-PTC
            ----------
            1) -Score (i.e maximal score)
            2) Number of absolute temporal range conflicts
            3) Number of precedence conflicts
            4) Number of inter-goal temporal conflicts
            5) Number of collisions (Note that DragonBaseEnv has no collisions)
            6) Path cost
            7) Number of generated nodes (i.e. expand earliest generated node)

            CBS-TA
            ------
            1) -Score (i.e maximal score)
            2) Number of return conflicts
            3) Number of collisions (Note that DragonBaseEnv has no collisions)
            4) Path cost
            5) Number of generated nodes (i.e. expand earliest generated node)
        
        Parameters
        ----------
        root : bool
            Whether to pop from open_root_list or open_list.

        Returns
        -------
        node : dict[str, Union[float, list, dict]]
            Node popped from list for respective algorithm.

            CBS-TA-PTC
            ----------
            {'absolute_temporal_range_conflicts': List of absolute temporal range conflicts,
             'absolute_temporal_range_completion_timestep_constraints': List of completion timestep constraints from 
                                                                        absolute temporal range conflicts,
             'collisions': List of collisions,
             'collisions_constraints': List of constraints from collisions (vertex/edge collisions),
             'cost': Total path cost from all agents,
             'env_done': Whether the environment has terminated following paths, 
             'individual_cost': Dictionary mapping agent ID to respective agent's path cost,
             'inter_goal_temporal_conflicts': List of inter-goal temporal conflicts,
             'inter_goal_temporal_completion_timestep_constraints': List of completion timestep constraints from 
                                                                    inter-goal temporal conflicts,
             'last_timestep': Last timestep from path in environment, 
             'paths': Dictionary mapping agent ID to path of agent,
             'path_done': Whether the paths of all agents have terminated,
             'precedence_conflicts': List of precedence conflicts,
             'precedence_completion_timestep_constraints': List of completion timestep constraints from precedence 
                                                           conflicts, 
             'root': If node is root node,  
             'score': Evaluated score of path in environment,
             'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                                agent
            }

            CBS-TA
            ------
            {'collisions': List of collisions,
             'collisions_constraints': List of constraints from collisions (vertex/edge collisions),
             'cost': Total path cost from all agents,
             'env_done': Whether the environment has terminated following paths, 
             'individual_cost': Dictionary mapping agent ID to respective agent's path cost,
             'last_timestep': Last timestep from path in environment, 
             'paths': Dictionary mapping agent ID to path of agent,
             'path_done': Whether the paths of all agents have terminated,
             'return_conflicts': List of precedence conflicts,
             'return_constraints': List of return constraints from return conflicts identical to vertex collisions 
             'root': If node is root node,  
             'score': Evaluated score of path in environment,
             'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                                agent
            }
        """
        # CBS-TA-PTC
        if self.algo == 'cbs_ta_ptc':
            if root:
                _, _, _, _, _, _, _, node = heapq.heappop(self.open_root_list)
            else:
                _, _, _, _, _, _, _, node = heapq.heappop(self.open_list)
        # CBS-TA
        elif self.algo == 'cbs_ta':
            if root:
                _, _, _, _, _, node = heapq.heappop(self.open_root_list)
            else:
                _, _, _, _, _, node = heapq.heappop(self.open_list)
        self.num_of_expanded += 1
        return node

    def print_results(self, success: bool) -> float:
        """
        Function to print relevant metrics.

        Parameters
        ----------
        success : bool
            Whether solution is found

        Returns
        -------
        compute_time : float
            Cpu time required to generate solution
        """
        if success: 
            compute_time = time.time() - self.start_time
            print("CPU time (s):                {:.2f}".format(compute_time))
            print("Sum of costs:                {}".format(self.solution_node['cost']))
            print("Individual costs:            {}".format(self.solution_node['individual_cost']))
            print("Score:                       {}".format(self.solution_node['score']))
            print("Maximum score:               {}".format(self.solution_node['max_score']))
            print("Epsilon:                     {}".format(self.solution_node['epsilon']))
            print("Desired score lower bound:   {}".format(self.epsilon * self.max_score))
            print("Mission length:              {}".format(self.mission_length))
            print("Last time:                   {}".format(self.solution_node['last_time']))
            print("Last timestep:               {}".format(self.solution_node['last_timestep']))
            print("Task allocation:             {}".format(self.solution_node['task_allocation']))
            print("Task allocation heuristic:   {}".format(self.task_allocation_heuristic))
            print("Number of clusters:          {}".format(self.num_of_clusters))
            print("Pruning heuristic:           {}".format(self.pruning_heuristic))
            print("Bomb dependencies:           {}".format(self.bomb_dependency_order_list))
            print("Bomb fuses:                  {}".format(self.bomb_fuse_dict))

        else:
            compute_time = 0

            # Reset environment
            self.env_reset()

            print("\n Tool Allocation \n")
            for agent_id, agent in self.env.agents.items():
                print(f'Agent {agent_id}: {agent.tool_remaining_uses}')

            print("\n Bombs \n")
            for node in self.env.graph.nodes.values():
                for bomb in node.bombs:
                    print(f'Bomb ID: {bomb.id}')
                    print(f'Bomb node ID: {node.id}')
                    print(f'Bomb color sequence: {bomb.sequence}')
                    print("\n")

        return compute_time

    def push_node(self, node: dict[str, Union[float, list, dict]], root: bool):
        """
        Function to push node into heap sorted by the following priorities for respective algorithm:
            
            CBS-TA-PTC
            ----------
            1) -Score (i.e maximal score)
            2) Number of absolute temporal range conflicts
            3) Number of precedence conflicts
            4) Number of inter-goal temporal conflicts
            5) Number of collisions (Note that DragonBaseEnv has no collisions)
            6) Path cost
            7) Number of generated nodes (i.e. expand earliest generated node)

            CBS-TA
            ------
            1) -Score (i.e maximal score)
            2) Number of return conflicts
            3) Number of collisions (Note that DragonBaseEnv has no collisions)
            4) Path cost
            5) Number of generated nodes (i.e. expand earliest generated node)

        Parameters
        ----------
        node : dict[str, Union[float, list, dict]]
            Node to be pushed to the list for respective algorithm.

            CBS-TA-PTC
            ----------
            {'absolute_temporal_range_conflicts': List of absolute temporal range conflicts,
             'absolute_temporal_range_completion_timestep_constraints': List of completion timestep constraints from 
                                                                        absolute temporal range conflicts,
             'collisions': List of collisions,
             'collisions_constraints': List of constraints from collisions (vertex/edge collisions),
             'cost': Total path cost from all agents,
             'env_done': Whether the environment has terminated following paths, 
             'individual_cost': Dictionary mapping agent ID to respective agent's path cost,
             'inter_goal_temporal_conflicts': List of inter-goal temporal conflicts,
             'inter_goal_temporal_completion_timestep_constraints': List of completion timestep constraints from 
                                                                    inter-goal temporal conflicts,
             'last_timestep': Last timestep from path in environment, 
             'paths': Dictionary mapping agent ID to path of agent,
             'path_done': Whether the paths of all agents have terminated,
             'precedence_conflicts': List of precedence conflicts,
             'precedence_completion_timestep_constraints': List of completion timestep constraints from precedence 
                                                           conflicts, 
             'root': If node is root node,  
             'score': Evaluated score of path in environment,
             'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                                agent
            }

            CBS-TA
            ------
            {'collisions': List of collisions,
             'collisions_constraints': List of constraints from collisions (vertex/edge collisions),
             'cost': Total path cost from all agents,
             'env_done': Whether the environment has terminated following paths, 
             'individual_cost': Dictionary mapping agent ID to respective agent's path cost,
             'last_timestep': Last timestep from path in environment, 
             'paths': Dictionary mapping agent ID to path of agent,
             'path_done': Whether the paths of all agents have terminated,
             'return_conflicts': List of precedence conflicts,
             'return_constraints': List of return constraints from return conflicts identical to vertex collisions 
             'root': If node is root node,  
             'score': Evaluated score of path in environment,
             'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                                agent
            }
        root : bool
            Whether to push from open_root_list or open_list
        """
        # CBS-TA-PTC
        if self.algo == 'cbs_ta_ptc':
            # Push root node to open_root_list
            if root:
                heapq.heappush(self.open_root_list, 
                               (-node['score'],
                                len(node['absolute_temporal_range_conflicts']),  
                                len(node['precedence_conflicts']), 
                                len(node['inter_goal_temporal_conflicts']), 
                                len(node['collisions']), 
                                node['cost'],
                                self.num_of_generated, 
                                node))
            # Push other nodes to open_list
            else: 
                heapq.heappush(self.open_list, 
                               (-node['score'],
                                len(node['absolute_temporal_range_conflicts']), 
                                len(node['precedence_conflicts']),  
                                len(node['inter_goal_temporal_conflicts']), 
                                len(node['collisions']),
                                node['cost'], 
                                self.num_of_generated, 
                                node))
        # CBS-TA
        elif self.algo == 'cbs_ta':
            # Push root node to open_root_list
            if root:
                heapq.heappush(self.open_root_list, 
                               (-node['score'],
                                len(node['return_conflicts']),  
                                len(node['collisions']), 
                                node['cost'],
                                self.num_of_generated, 
                                node))
            # Push other nodes to open_list
            else: 
                heapq.heappush(self.open_list, 
                               (-node['score'],
                                len(node['return_conflicts']),
                                len(node['collisions']),
                                node['cost'], 
                                self.num_of_generated, 
                                node))
        self.num_of_generated += 1

    def render_solution(self):
        """
        Function to render solution path.
        """
        try:
            while True:
                _, _, _, _, _, _, _= self.evaluate_paths(bomb_id_tup_dict=self.solution_node['task_allocation'],
                                                         paths=self.solution_node['paths'],
                                                         render=True)
        except KeyboardInterrupt:
            # Generate gif
            get_gif(save_path=self.save_path)
            print("Render terminated")

    def reset(self):
        """
        Function to reset list and search variables
        """
        self.open_list = []
        self.open_root_list = []
        self.num_of_generated = 0
        self.num_of_expanded = 0

    def resolve_conflicts(self, node: dict[str, Union[float, list, dict]]) -> list:
        """
        Function to resolve conflicts given a CT node based on the CBS algorithm based on the following priority:

            CBS-TA-PTC
            ----------
            1) Absolute temporal range conflicts 
            2) Precedence conflicts
            3) Inter-goal temporal conflicts 
            4) Collisions

            CBS-TA
            ------
            1) Return conflicts 
            2) Collisions

        Parameters
        ----------
        node : dict[str, Union[float, list, dict]]
            Node with conflicts to be resolved.

            CBS-TA-PTC
            ----------
            {'absolute_temporal_range_conflicts': List of absolute temporal range conflicts,
             'absolute_temporal_range_completion_timestep_constraints': List of completion timestep constraints from 
                                                                        absolute temporal range conflicts,
             'collisions': List of collisions,
             'collisions_constraints': List of constraints from collisions (vertex/edge collisions),
             'cost': Total path cost from all agents,
             'env_done': Whether the environment has terminated following paths, 
             'individual_cost': Dictionary mapping agent ID to respective agent's path cost,
             'inter_goal_temporal_conflicts': List of inter-goal temporal conflicts,
             'inter_goal_temporal_completion_timestep_constraints': List of completion timestep constraints from 
                                                                    inter-goal temporal conflicts,
             'last_timestep': Last timestep from path in environment, 
             'paths': Dictionary mapping agent ID to path of agent,
             'path_done': Whether the paths of all agents have terminated,
             'precedence_conflicts': List of precedence conflicts,
             'precedence_completion_timestep_constraints': List of completion timestep constraints from precedence 
                                                           conflicts, 
             'root': If node is root node,  
             'score': Evaluated score of path in environment,
             'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                                agent
            }

            CBS-TA
            ------
            {'collisions': List of collisions,
             'collisions_constraints': List of constraints from collisions (vertex/edge collisions),
             'cost': Total path cost from all agents,
             'env_done': Whether the environment has terminated following paths, 
             'individual_cost': Dictionary mapping agent ID to respective agent's path cost,
             'last_timestep': Last timestep from path in environment, 
             'paths': Dictionary mapping agent ID to path of agent,
             'path_done': Whether the paths of all agents have terminated,
             'return_conflicts': List of precedence conflicts,
             'return_constraints': List of return constraints from return conflicts identical to vertex collisions 
             'root': If node is root node,  
             'score': Evaluated score of path in environment,
             'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                                agent
            }

        Returns
        -------
        constraint_list : list
            List of constraints
        """
        # CBS-TA-PTC
        if self.algo == 'cbs_ta_ptc':
            # Obtain absolute temporal range constraints given absolute temporal range conflicts
            if len(node['absolute_temporal_range_conflicts']) != 0:
                # Select first absolute temporal range constraint and resolve it
                absolute_temporal_range_conflict = node['absolute_temporal_range_conflicts'][0]
                absolute_temporal_range_completion_timestep_constraint_list = \
                    resolve_absolute_temporal_range_conflict(absolute_temporal_range_conflict)
                # Absolute temporal range conflict takes priority over precedence, inter-goal temporal conflicts and 
                # collisions
                precedence_completion_timestep_constraint_list = []
                inter_goal_temporal_completion_timestep_constraint_list = []
                collision_constraint_list = []
            # Obtain completion timestep constraints given precedence conflicts
            elif len(node['precedence_conflicts']) != 0:
                # No absolute temporal range conflicts
                absolute_temporal_range_completion_timestep_constraint_list = []
                # Select first precedence conflict and resolve it
                precedence_conflict = node['precedence_conflicts'][0]
                precedence_completion_timestep_constraint_list = resolve_precedence_conflict(precedence_conflict)
                # Precedence conflict takes priority over inter-goal temporal conflict and collisions
                inter_goal_temporal_completion_timestep_constraint_list = []
                collision_constraint_list = []
            # Obtain inter-goal temporal constraints given inter-goal temporal conflicts
            elif len(node['inter_goal_temporal_conflicts']) != 0:
                # No absolute temporal range and precedence conflicts
                absolute_temporal_range_completion_timestep_constraint_list = []
                precedence_completion_timestep_constraint_list= []
                # Select first inter-goal temporal conflict and resolve it
                inter_goal_temporal_conflict = node['inter_goal_temporal_conflicts'][0]
                inter_goal_temporal_completion_timestep_constraint_list = \
                    resolve_inter_goal_temporal_conflict(inter_goal_temporal_conflict)
                # Inter-goal temporal conflict takes priority over collisions
                collision_constraint_list = []
            # Obtain collision constraints given collisions
            elif len(node['collisions']) != 0:
                # No absolute temporal range, precedence and inter-goal temporal conflicts
                absolute_temporal_range_completion_timestep_constraint_list = []
                precedence_completion_timestep_constraint_list= []
                inter_goal_temporal_completion_timestep_constraint_list = []
                # If collisions are considered
                if self.include_collisions:
                    collision = node['collisions'][0]
                    collision_constraint_list = resolve_collision(collision)
                else:
                    collision_constraint_list = []
            # No conflicts but score below desired bound somehow -> pruned
            else:
                absolute_temporal_range_completion_timestep_constraint_list = []
                precedence_completion_timestep_constraint_list= []
                inter_goal_temporal_completion_timestep_constraint_list = []
                collision_constraint_list = []
            # Combine all constraints. Should only contain constraints of one type.
            constraint_list = absolute_temporal_range_completion_timestep_constraint_list \
                              + precedence_completion_timestep_constraint_list \
                              + inter_goal_temporal_completion_timestep_constraint_list + collision_constraint_list
        # CBS-TA
        elif self.algo == 'cbs_ta':
            # Obtain return constraints given return conflicts
            if len(node['return_conflicts']) != 0:
                # Select first return constraint and resolve it
                return_conflict = node['return_conflicts'][0]
                return_constraint_list = resolve_return_conflict(return_conflict)
                # Return conflict takes priority over collisions
                collision_constraint_list = []
            # Obtain collision constraints given collisions
            elif len(node['collisions']) != 0:
                # No return conflicts
                return_constraint_list = []
                # If collisions are considered
                if self.include_collisions:
                    collision = node['collisions'][0]
                    collision_constraint_list = resolve_collision(collision)
                else:
                    collision_constraint_list = []
            # No conflicts but score below desired bound somehow -> pruned
            else:
                return_constraint_list = []
                collision_constraint_list = []
            # Combine all constraints. Should only contain constraints of one type.
            constraint_list = return_constraint_list + collision_constraint_list
        return constraint_list

def solve(solution_timeout: float = 60.0,
          algo: str = 'cbs_ta_ptc',
          num_bombs_sub_task: int = 3,
          mission_length: float = 10*60,
          recon_phase_length: float = 2*60,  
          seconds_per_timestep: float = 2.0,
          valid_regions: set[Region] = set(Region),
          csv_path: Optional[str] = None,
          num_bombs_per_region: int = 15,
          budget_per_region: int = None,
          start_location: Optional[tuple[int, int]] = (24, 149),
          start_regions: set[Region] = set(Region),
          epsilon: float = 1.0,
          same_start_loc: bool = True,
          include_collisions: bool = False,
          include_chained_bombs: bool = True,
          include_fire_bombs: bool = False,
          include_fuse_bombs: bool = True,
          bomb_explode_terminate: bool = True,
          task_allocation_heuristic: str = 'k_means',
          pruning_heuristic: str = 'even_bomb_allocation',
          low_level_search: str = 'multi_label_a_star',
          save_render: bool = False,
          verbose: bool = True,
          render: bool = False,
          **kwargs
    ) -> tuple[dict, bool, list[float], list[float], Optional[Exception]]:
    """
    Function to solve a generated gym_dragon task

    Parameters
    ----------
    solution_timeout : float, default=60.0
        Timeout in seconds to find solution
    algo : str, default='cbs_ta_ptc'
        CBS algorithm ('cbs_ta', "cbs_ta_ptc")
    num_bombs_sub_task : int, default=3
        Number of bombs to resolve per CBS Reward search
    mission_length : float, default=10*60
        Mission length, in seconds
    recon_phase_length : float, default=2*60
        Length of reconnaissance phase, in seconds
    seconds_per_timestep : float, default=2.0
        Number of seconds per timestep in the environment
    valid_regions : set[Region], optional
        Set of regions to include in the environment
    csv_path : str, optional
        Path to CSV file containing bomb distribution
    num_bombs_per_region : int, default=15
        Number of bombs to be placed randomly in each region
    budget_per_region : int, default=None
        Team budget per region, to be spent randomly on purchasing tools
    start_location : tuple[int, int], optional, default=(24, 149)
        Initial location for all agents
    start_regions : set[Region], optional
        Set of valid regions for initial mission locations
    epsilon : float, default=1.0
        Suboptimality factor for score. Default is optimal
    same_start_loc : bool, default=True
        Whether for all agents to start at the same location
        NOTE: Default DragonBaseEnv starts at same location/node
    include_collisions : bool, default=False
        Whether to consider collisions for environment
        NOTE: Default DragonBaseEnv has no collisions
    include_chained_bombs : bool, default=True
        Whether to include chained bombs in the environment
        if self.
    include_fire_bombs : bool, default=False
        Whether to include fire bombs in the environment
    include_fuse_bombs : bool, default=True
        Whether to include fuse bombs in the environment
    bomb_explode_terminate : bool, default=True
        Whether to terminate environment when bomb explodes
    task_allocation_heuristic : str, default='k_means'
        Heuristic to order tasks
    pruning_heuristic : str, default='even_bomb_allocation'
        Heuristic to prune subtask allocations
    low_level_search : str
        Algorithm for low level search. Either 'multi_step_a_star' or 'multi_label_a_star'
    save_render : bool, default=False
        Whether to save rendered images
    verbose : bool, default=True
        Whether to print metrics/results  
    render : bool, default=False
        Whether to render solution  
    
    Keyword Arguments
    -----------------
    save_path : str
        Path to save rendered images
    num_of_clusters : int
        Number of clusters for K-Means
    csv_subtract_fuse : int
        Recon phase length to subtract from fuse if reading bomb distribution from CSV
    overlay_graph : bool
            Whether to overlay the graph representation over the map for rendering

    Returns
    -------
    solution_node : dict
        Solution node in the form of a dictionary of the following format:
        {'cost': Total path cost from all agents,
         'last_time': Last time (in seconds) from path in environment,    
         'last_timestep': Last timestep from path in environment, 
         'paths': Dictionary mapping agent ID to path of agent,
         'score': Evaluated score of path in environment,
         'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                            agent
        }
    success : bool
        Whether solver is successful finding solution given epsilon
    compute_time_list : list[float]
            List containing cpu time required to generate solution based on sub-solutions sequentially
    optimality_ratio_list : list[float]
        List containing ratio of score w.r.t max score of solution based on sub-solutions sequentially
    exception : Optional[Exception]
        Exception from find_sub_solution() if any
    """
    # Define a signal handler function
    def signal_handler(sig, frame):
        print("Received signal {}. Terminating...".format(sig))
        raise SystemExit

    # Register the signal handler for the SIGTERM signal
    signal.signal(signal.SIGTERM, signal_handler)

    # Find solution
    try:
        # CBSSolver
        solver = CBSSolver(
            algo=algo,
            num_bombs_sub_task=num_bombs_sub_task,
            mission_length=mission_length,
            recon_phase_length=recon_phase_length,
            seconds_per_timestep=seconds_per_timestep,
            valid_regions=valid_regions,
            csv_path=csv_path,
            num_bombs_per_region=num_bombs_per_region,
            budget_per_region=budget_per_region,
            start_location=start_location,
            start_regions=start_regions,
            epsilon=epsilon,
            same_start_loc=same_start_loc,
            include_collisions=include_collisions,
            include_chained_bombs=include_chained_bombs,
            include_fire_bombs=include_fire_bombs,
            include_fuse_bombs=include_fuse_bombs,
            bomb_explode_terminate=bomb_explode_terminate,
            task_allocation_heuristic=task_allocation_heuristic,
            pruning_heuristic=pruning_heuristic,
            low_level_search=low_level_search,
            save_render=save_render,
            verbose=verbose,
            save_path=kwargs.get('save_path', './experiments/gif/test'),
            num_of_clusters=kwargs.get('num_of_clusters', num_bombs_per_region // num_bombs_sub_task),
            csv_subtract_fuse=kwargs.get('csv_subtract_fuse', 0)
        )
        solution_node, success, compute_time_list, optimality_ratio_list, exception = \
            solver.find_solution(solution_timeout=solution_timeout)
    except SystemExit:
        if verbose:
            print("Task terminated by signal")
    # Only exception is exception from no possible solution from __init__()
    except Exception as e:
        assert 'For given bombs and tools from agents, task has no solution' in str(e), \
            f'Caught unexpected exception {str(e)}'
        if verbose:
            print(f'Exception raised during __init__(): {str(e)}')
        return None, None, None, None, e
        
    # Print optimality metric
    if verbose:
        print(f"Solution score: {solution_node['score']}")
        print(f"Max score: {solution_node['max_score']}")
        print(f"Optimality ratio: {optimality_ratio_list[-1]}")
        print(f"Compute time: {compute_time_list[-1]}")

    # Render solution if desired
    if render:
        solver.render_solution()

    return solution_node, success, compute_time_list, optimality_ratio_list, exception 