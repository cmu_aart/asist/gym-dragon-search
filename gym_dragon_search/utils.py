import heapq
import imageio
import itertools
import numpy as np
import operator
import os
import random

from gym_dragon.core import Color
from scipy.optimize import linprog
from typing import Callable, Iterator, Union, Optional

def bomb_dependency_order_sort(sorted_bomb_seq_dict: dict[int, tuple[Color]], 
                               bomb_dependency_order_list: list[list[int]]
    ) -> dict[int, tuple[Color]]:
    """
    Function to sort an existing ordered dictionary mapping bomb ID to bomb sequence for bomb defusal task assignment 
    to respect bomb dependency ordering.

    Parameters
    ----------
    sorted_bomb_seq_dict : dict[int, tuple[Color]]
        Existing ordered dictionary mapping bomb ID to bomb sequence for bomb defusal task assignment
    bomb_dependency_order_list : list[list[int]]
        List of lists containing ordered bomb IDs according to their dependencies.
        Ordering is based on ordering of bomb defusal from smallest to largest index.
    
    Returns
    -------
    dependency_sorted_bomb_seq_dict : dict[int, tuple[Color]]
        Sorted dictionary mapping bomb ID to bomb sequence for bomb defusal task assignment that respect bomb 
        dependency ordering while preserving ordering of sorted_bomb_seq_dict as much as possible.
    """
    # Dictionary mapping bomb ID to bomb sequence post sorting 
    dependency_sorted_bomb_seq_dict = {}
    # Obtain a list of ordered bomb IDs
    sorted_bomb_id_list = list(sorted_bomb_seq_dict.keys())
    # Iterate over dependeny ordering lists in bomb_dependency_order_list
    for bomb_order_list in bomb_dependency_order_list:
        # Check each dependency pair starting from dependent bomb that is not dependent on any other bomb
        for i in reversed(range(len(bomb_order_list) - 1)):
            # Obtain corresponding bomb ID of dependency pair and their index in sorted_bomb_id_list
            curr_bomb_id = bomb_order_list[i]
            curr_dependent_bomb_id = bomb_order_list[i + 1]
            curr_bomb_id_ind = sorted_bomb_id_list.index(curr_bomb_id)
            curr_dependent_bomb_id_ind = sorted_bomb_id_list.index(curr_dependent_bomb_id)
            # Execute insert sort to ensure bomb dependency is respected
            if curr_bomb_id_ind > curr_dependent_bomb_id_ind:
                sorted_bomb_id_list.pop(curr_bomb_id_ind)
                sorted_bomb_id_list.insert(curr_dependent_bomb_id_ind, curr_bomb_id)
    # Rebuild sorted dictionary given new ordering
    for bomb_id in sorted_bomb_id_list:
        dependency_sorted_bomb_seq_dict[bomb_id] = sorted_bomb_seq_dict[bomb_id]
    return dependency_sorted_bomb_seq_dict

def build_collision_constraint_dict(agent_id: str, 
                                    collision_constraint_list: list[dict[str, Union[int, list[int], float]]]                                  
    ) -> tuple[dict[float, list[dict[str, Union[int, list[int], float, str]]]], float]:
    """
    Function to build a dictionary of collision constraints for agent with agent ID for each timestep

    Parameters
    ----------
    agent_id : str
        Agent ID of agent whose constraints are generated
    collision_constraint_list : list[dict[str, Union[int, list[int]]]]
        Dictionary containing constraint parameters of the following structure
        {'agent': agent ID, 'conflict_type': 'edge_collision'/'vertex_collision', 
         'loc': location (node ID) of vertex (list of len 1)/edge collision (list of len 2), 
         'timestep': float (at the end for edge collision)}

    Returns
    -------
    collision_constraint_dict : dict[float, list[dict[str, Union[int, list[int], float, str]]]]
        Dictionary mapping timesteps to constraints for the specified agent.
        Constraints are of the following format:
        {'agent': agent ID, 'conflict_type': 'edge_collision'/'vertex_collision', 
         'loc': location (node ID) of vertex (list of len 1)/edge collision (list of len 2), 
         'timestep': float (at the end for edge collision)}
    max_timestep : float
        Maximum timestep of constraint in lookup_table 
    """
    collision_constraint_dict = dict()
    max_timestep = 0
    for constraint in collision_constraint_list:
        if constraint['agent_id'] == agent_id:
            if constraint['timestep'] in collision_constraint_dict:
                collision_constraint_dict[constraint['timestep']].append(constraint)
            else:
                collision_constraint_dict[constraint['timestep']] = [constraint]
            if constraint['timestep'] > max_timestep:
                max_timestep = constraint['timestep']
    return collision_constraint_dict, max_timestep

def build_return_constraint_dict(agent_id: str, 
                                 return_constraint_list: list[dict[str, Union[int, str]]]                                  
    ) -> dict[float, list[dict[str, Union[int, str]]]]:
    """
    Function to build a dictionary of return constraints for agent with agent ID for each timestep

    Parameters
    ----------
    agent_id : str
        Agent ID of agent whose constraints are generated
    return_conflict_list : list[dict[str, Union[int, str]]]
        List containing return conflicts (vertex constraints) based on dictionary of following structure:
        {'agent_id': agent ID, 'conflict_type': str, 'loc': node ID, 'timestep': timestep}

    Returns
    -------
    return_constraint_dict : dict[float, list[dict[str, Union[int, str]]]]
        Dictionary mapping timesteps to return constraints for the specified agent.
        Constraints are of the following format:
        {'agent_id': agent ID, 'conflict_type': str, 'loc': node ID, 'timestep': timestep}
    max_timestep : float
        Maximum timestep of constraint in lookup_table 
    """
    return_constraint_dict = dict()
    max_timestep = 0
    for constraint in return_constraint_list:
        if constraint['agent_id'] == agent_id:
            if constraint['timestep'] in return_constraint_dict:
                return_constraint_dict[constraint['timestep']].append(constraint)
            else:
                return_constraint_dict[constraint['timestep']] = [constraint]
            if constraint['timestep'] > max_timestep:
                max_timestep = constraint['timestep']
    return return_constraint_dict, max_timestep

def build_temporal_constraint_dict(
        agent_id: str,
        temporal_constraint_list: list[dict[str, Union[Callable, Color, float, str]]]
    ) -> dict[tuple[int, Color], dict[str, Union[Callable, Color, float, str]]]:
    """
    Function to build a dictionary of temporal constraints for agent with agent ID

    Parameters
    ----------
    agent_id : str
        Agent ID of agent whose constraints are generated
    temporal_constraint_list : list[dict[str, Union[Callable, Color, float, str]]]
        List of temporal constraints that are dictionaries of the following structure:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
         'tool_color': Color, 'value': float}

    Returns
    -------
    temporal_constraint_dict : dict[tuple[int, Color], dict[str, Union[Callable, Color, float, str]]]
        Dictionary mapping (bomb ID, Color) tuple to temporal constraints for the given agent.
        Constraints are of the following format:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
         'tool_color': Color, 'value': float}
    """
    temporal_constraint_dict = dict()
    for constraint in temporal_constraint_list:
        if constraint['agent_id'] == agent_id:
            if (constraint['bomb_id'], constraint['tool_color']) in temporal_constraint_dict:
                temporal_constraint_dict[(constraint['bomb_id'], constraint['tool_color'])].append(constraint)
            else:
                temporal_constraint_dict[(constraint['bomb_id'], constraint['tool_color'])] = [constraint]
    return temporal_constraint_dict

def compare_nodes(node_1: dict[str, Union[int, float, dict]], node_2: dict[str, Union[int, float, dict]]) -> bool:
    """
    Function to compare two nodes based on g and h values.
    
    Parameters
    ----------
    node_1 : dict[str, Union[int, float, dict]]
        Dictionary containing node 1 parameters of the following structure:
        {'loc': node ID, 'g_val': float, 'h_val': float, 'parent': node_dict, 'timestep': float}
    node_2 : dict[str, Union[int, float, dict]]
        Dictionary containing node 2 parameters of the following structure:
        {'loc': node ID, 'g_val': float, 'h_val': float, 'parent': node_dict, 'timestep': float}
    """
    # Return true is node_1 is better than node_2
    return node_1['g_val'] + node_1['h_val'] < node_2['g_val'] + node_2['h_val']

def compute_distance(loc_tup_1: tuple[float, float], loc_tup_2: tuple[float, float]) -> float:
    """
    Function to calculate the euclidean distance between two cartesian cooridinates

    Parameters
    ----------
    loc_tup_1 : tuple[float, float]
        Tuple with the cartesian coordinates of the first location
    loc_tup_2 : tuple[float, float]
        Tuple with the cartesian coordinates of the second location
    """
    return ((loc_tup_1[0] - loc_tup_2[0])**2 + (loc_tup_1[1] - loc_tup_2[1])**2)**0.5

def detect_collision(path_1: list[dict[str, Union[int, str, None, Color, list[Color]]]], 
                     path_2: list[dict[str, Union[int, str, None, Color, list[Color]]]]
    ) -> Optional[tuple[list[int], int]]:
    """
    Function to return the first collision between two paths. There are two types of collisions:
        
        1) Vertex collision: When two agents occupy the same location (node) at the same timestep
        2) Edge collision: When two agents swap their location at the same timestep 

    Parameters
    ----------
    path_1 : list[dict[str, Union[int, str, None, Color, list[Color]]]]
        List containing sequence of dictionaries with location (node ID) and the action to be implemented at the 
        location (node ID) that represents a path. Global path consists of all the node IDs of nodes to be visited 
        by node_ids_list. Dictionary has the following structure:
        {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
         'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
         if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
         'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
         None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
         of tool being applied to bomb if action is 'apply_bomb_tool' else None}

        Possible Actions
        ----------------
        'go_to': Travel to next location (node ID) in path
        'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence 
                           in task allocation
    path_2 : list[dict[str, Union[int, str, None, Color, list[Color]]]]
        List containing sequence of dictionaries with location (node ID) and the action to be implemented at the 
        location (node ID) that represents a path. Global path consists of all the node IDs of nodes to be visited 
        by node_ids_list. Dictionary has the following structure:
        {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
         'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
         if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
         'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
         None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
         of tool being applied to bomb if action is 'apply_bomb_tool' else None}

        Possible Actions
        ----------------
        'go_to': Travel to next location (node ID) in path
        'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence 
                           in task allocation

    Returns
    -------
    collision : tuple[list[int], int]
        Tuple containing location of collision and timestep
    """
    max_path_len = max(len(path_1), len(path_2))
    prev_loc_1 = None
    prev_loc_2 = None
    for timestep in range(max_path_len):
        loc_1 = get_location(path_1, timestep)
        loc_2 = get_location(path_2, timestep)
        if loc_1 == loc_2:
            return ([loc_1], timestep)
        elif loc_1 == prev_loc_2 and loc_2 == prev_loc_1:
            return ([prev_loc_1, prev_loc_2], timestep)
        prev_loc_1 = loc_1
        prev_loc_2 = loc_2
    return None

def detect_collisions(paths: dict[str, list[dict[str, Union[str, int]]]]
    ) -> list[dict[str, Union[int, list[int], float]]]:
    """
    Function return a list of first collisions between all agent pairs.

    Parameters
    ----------
    paths : dict[str, list[dict[str, Union[str, int]]]]
        Dictionary mapping from agent ID to agent's path, which is a list containing sequence of dictionaries with 
        location (node ID) and the action to be implemented at the location (node ID). Dictionary has the following 
        structure:
        {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
         'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
         if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
         'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
         None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
         of tool being applied to bomb if action is 'apply_bomb_tool' else None}

        Possible Actions
        ----------------
        'go_to': Travel to next location (node ID) in path
        'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence 
                           in task allocation

    Returns
    -------
    collisions_list : list[dict[str, Union[int, list[int]]]]
        List containing collisions based on dictionary of following structure:
        {'agent_id_1': agent ID, 'agent_id_2': agent ID, 'loc': [node ID] (vertex collision) / 
         [node ID, node ID] (edge collision), 'timestep': timestep}
    """
    collisions_list = []
    for i, (agent_id_1, path_1) in enumerate(paths):
        for j, (agent_id_2, path_2) in enumerate(paths):
            if j > i:
                result = detect_collision(path_1, path_2)
                if result is not None:
                    collisions_list.append({'agent_id_1': agent_id_1, 
                                            'agent_id_2': agent_id_2, 
                                            'loc': result[0], 
                                            'timestep': result[1]})
    return collisions_list

def get_absolute_temporal_range_conflicts(
        bomb_id_defusal_info_dict: dict[str, list[dict[str, Union[float, int, str, Color, tuple[Color]]]]],
        seconds_per_timestep: float,
        sub_bomb_fuse_dict: dict[int, tuple[int, int]]
    ) -> list[dict[str, Union[Callable, float, str]]]:
    """
    Function to return absolute temporal range conflicts

    Parameters
    ----------
    bomb_id_defusal_info_dict : dict[str, list[dict[str, Union[float, int, str, Color, tuple[Color]]]]]
        Dictionary mapping bomb ID to list containing information about bomb defusals that are dictionaries of the 
        following structure: 
        {'agent_id': agent ID, 'bomb_countdown_length': int, 'bomb_id': bomb ID, 'bomb_sequence': tuple[Color], 
         'time': time, timestep': timestep, 'tool_color': Color}
    seconds_per_timestep: float
        Seconds per timestep
    sub_bomb_fuse_dict : dict[int, tuple[int, int]]
        Dictionary mapping bomb ID to bomb fuses in tuple format to represent temporal ranges for given subtask

    Returns
    -------
    absolute_temporal_range_conflict_list : list[dict[str, Union[Callable, float, str]]]
        List of absolute temporal range conflicts/constraints that are dictionaries of the following structure:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
         'tool_color': Color, 'value': float}
    """
    # List to store precedence conflicts
    absolute_temporal_range_conflict_list = []
    # If there are relevant bomb fuses
    if sub_bomb_fuse_dict:
        # Iterate over sub_bomb_fuse_dict
        for bomb_id, temporal_range_tup in sub_bomb_fuse_dict.items():
            # Get defusal_info_list pertaining to bomb ID in sub_bomb_fuse_dict
            defusal_info_list = bomb_id_defusal_info_dict[bomb_id]
            # Iterate over defusal_info_list
            for defusal_info_dict in defusal_info_list:
                # Defusal step is less than the specified lower bound of fuse
                if defusal_info_dict['timestep'] < temporal_range_tup[0] // seconds_per_timestep:
                    absolute_temporal_range_conflict_list\
                        .append({'agent_id': defusal_info_dict['agent_id'],
                                 'bomb_id': defusal_info_dict['bomb_id'],
                                 'conflict_type': 'absolute_temporal_range_conflict',
                                 'operator': operator.ge,
                                 'tool_color': defusal_info_dict['tool_color'],
                                 'value': temporal_range_tup[0] // seconds_per_timestep
                                })
                # Defusal step is larger than specified upper bound of fuse
                elif defusal_info_dict['timestep'] > temporal_range_tup[1] // seconds_per_timestep:
                    absolute_temporal_range_conflict_list\
                        .append({'agent_id': defusal_info_dict['agent_id'],
                                 'bomb_id': defusal_info_dict['bomb_id'],
                                 'conflict_type': 'absolute_temporal_range_conflict',
                                 'operator': operator.le,
                                 'tool_color': defusal_info_dict['tool_color'],
                                 'value': temporal_range_tup[1] // seconds_per_timestep
                                })
    return absolute_temporal_range_conflict_list

def generate_allocations(lst: list, n: int) -> Iterator[list[list]]:
    """
    Function to return generator that allocates elements in a list to n separate lists.
    Note that allocation preserves ordering from the original list.

    Parameters
    ----------
    lst : list
        Original list with the elements to be allocated
    n : int
        Number of lists of elements to be allocated

    Returns
    -------
    generator : Iterator[list[list]]
        Generator that generates allocation
    """
    for allocation in itertools.product(range(n), repeat=len(lst)):
        groups = [[] for _ in range(n)]
        for i, group_index in enumerate(allocation):
            groups[group_index].append(lst[i])
        yield groups

def get_allocations_list(lst: list, n: int) -> list[list]:
    """
    Function to get a list containing all possible allocations of elements in a list to n separate lists.
    Note that allocation preserves ordering from the original list.

    Parameters
    ----------
    lst : list
        Original list with the elements to be allocated
    n : int
        Number of lists of elements to be allocated

    Returns
    -------
    results : list[list]
        List containing all possible allocations
    """
    result = []
    for allocation in itertools.product(range(n), repeat=len(lst)):
        allocated_lists = [[] for _ in range(n)]
        for i, x in enumerate(lst):
            allocated_lists[allocation[i]].append(x)
        result.append(allocated_lists)
    return result

def get_bomb_dependency_order_list(bomb_dependency: dict[int, int]) -> list[list[int]]:
    """
    Function to return list of lists containing order of bombs to be defused respecting bomb dependency

    Parameters
    ----------
    bomb_dependency : dict[int, int]
        Dictionary mapping bomb ID to bomb ID of bomb it is dependent on

    Returns
    -------
    bomb_dependency_order_list : list[list[int]]
        List of lists containing ordered bomb IDs according to their dependencies.
        Ordering is based on ordering of bomb defusal from smallest to largest index.
    """
    # List to store lists of ordered bomb IDs according to their dependencies
    # Ordering is based on ordering of bomb defusal from smallest to largest index
    bomb_dependency_order_list = []
    # Iterate while there exist bombs with dependencies
    while not all(value is None for value in bomb_dependency.values()):
        # List to store bomb IDs in order
        bomb_order_list = []
        # Variables to track bomb ID and dependent bomb ID
        curr_bomb_id = None
        curr_dependent_bomb_id = None
        # Obtain bomb ID at the start of the dependency with no other bombs dependent on it
        for bomb_id, dependent_bomb_id in bomb_dependency.items():
            if dependent_bomb_id != None and bomb_id not in bomb_dependency.values():
                curr_bomb_id = bomb_id
                curr_dependent_bomb_id = dependent_bomb_id
                break
        # Go down the dependency chain and append bomb IDs to bomb_order_list
        while curr_bomb_id != None:
            bomb_order_list.append(curr_bomb_id)
            curr_bomb_id = curr_dependent_bomb_id
            curr_dependent_bomb_id = bomb_dependency.get(curr_bomb_id, None)
        # Remove bomb IDs from dependency chain from bomb_dependency
        for bomb_id in bomb_order_list:
            bomb_dependency.pop(bomb_id)
        # Reverse ordering to get bomb defusal ordering
        bomb_order_list.reverse()
        # Append bomb_order_list to bomb_dependency_order_list
        bomb_dependency_order_list.append(bomb_order_list)
    return bomb_dependency_order_list

def get_bomb_defusal_info(paths: dict[str, list[dict[str, Union[str, int]]]],
                          start_timestep: int, 
                          last_timestep: int,
                          seconds_per_timestep: float
    ) -> dict[str, list[dict[str, Union[float, int, str, Color, tuple[Color]]]]]:
    """
    Function to return bomb defusal information from paths of agents

    Parameters
    ----------
    paths : dict[str, list[dict[str, Union[str, int]]]]
        Dictionary mapping of agent ID to the path of the agent, which is a list containing sequence of dictionaries 
        with location (node ID) and the action to be implemented at the location (node ID) that represents a path. 
        Dictionary has the following structure:
        {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
         'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
         if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
         'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
         None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
         of tool being applied to bomb if action is 'apply_bomb_tool' else None}

        Possible Actions
        ----------------
        'go_to': Travel to next location (node ID) in path
        'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence 
                           in task allocation
    start_timestep : int
        Starting timestep
    last_timestep : int
        Last timestep from past paths that is continued by this path
    seconds_per_timestep: float
        Seconds per timestep

    Returns
    -------
    bomb_id_defusal_info_dict : dict[str, list[dict[str, Union[float, int, str, Color, tuple[Color]]]]]
        Dictionary mapping bomb ID to list containing information about bomb defusals that are dictionaries of the 
        following structure: 
        {'agent_id': agent ID, 'bomb_countdown_length': int, 'bomb_id': bomb ID, 'bomb_sequence': tuple[Color], 
         'time': time, timestep': timestep, 'tool_color': Color}
    """
    # Dictionary to store bomb defusal information
    bomb_defusal_info_dict = {}
    # Boolean to track when all paths have terminated
    done = False
    # Track timestep corresponding to environment timestep
    timestep = last_timestep if last_timestep == start_timestep else last_timestep + 1
    # Iterate while paths have not terminated
    while not done:
        # List to track if all agent's path have terminated
        path_done_list = []
        # Iterate over agent paths
        for agent_id, path in paths.items():
            # Obtain path dictionary at specified timestep
            if last_timestep == start_timestep:
                agent_path_dict, agent_path_done = get_path_dict(path, timestep - last_timestep)
            else:
                agent_path_dict, agent_path_done = get_path_dict(path, timestep - 1 - last_timestep)
            # Update path_done_list with path terminal flag
            path_done_list.append(agent_path_done)
            # Check for non-terminal path dictionary
            if not agent_path_done and agent_path_dict['action'] == 'apply_bomb_tool':
                # Generate bomb defusal information
                bomb_defusal_info = {'agent_id': agent_id,
                                     'bomb_countdown_length': agent_path_dict['bomb_countdown_length'],
                                     'bomb_id': agent_path_dict['bomb_id'],
                                     'bomb_sequence': agent_path_dict['bomb_sequence'],
                                     'time': timestep * seconds_per_timestep,
                                     'timestep': timestep,
                                     'tool_color': agent_path_dict['tool_color']}
                # Update bomb_defusal_info_dict
                if agent_path_dict['bomb_id'] in bomb_defusal_info_dict:
                    bomb_defusal_info_dict[agent_path_dict['bomb_id']].append(bomb_defusal_info)
                else:
                    bomb_defusal_info_dict[agent_path_dict['bomb_id']] = [bomb_defusal_info]
        # Increment timestep
        timestep += 1
        # Check if all paths have terminated
        done = all(path_done_list)
    return bomb_defusal_info_dict

def get_gif(save_path : str):
    """
    Function to create a gif from saved images from the rendered path

    Parameters
    ----------
    save_path : str
        Path where rendered images are saved
    """
    # Get a list of all the image filenames in the directory
    filenames = sorted(os.listdir(save_path), key=lambda x: int(x.split('.')[0]))
    # Create an empty list to store the images
    images = []
    # Loop through each filename and add the image to the list
    for filename in filenames:
        filepath = os.path.join(save_path, filename)
        image = imageio.imread(filepath)
        images.append(image)
    # Use imageio.mimsave() to save the images as a slower GIF
    output_file = save_path + "output.gif"
    imageio.mimsave(output_file, images, duration=500)

def get_inter_goal_temporal_conflicts(
        bomb_id_defusal_info_dict: dict[str, list[dict[str, Union[float, int, str, Color, tuple[Color]]]]],
        seconds_per_timestep: float
    ) -> list[dict[str, Union[Color, float, int, str]]]:
    """
    Function to generate inter-goal temporal conflicts.

    Parameters
    ----------
    bomb_id_defusal_info_dict : dict[str, list[dict[str, Union[float, int, str, Color, tuple[Color]]]]]
        Dictionary mapping bomb ID to list containing information about bomb defusals that are dictionaries of the 
        following structure: 
        {'agent_id': agent ID, 'bomb_countdown_length': int, 'bomb_id': bomb ID, 'bomb_sequence': tuple[Color], 
         'time': time, timestep': timestep, 'tool_color': Color}
    seconds_per_timestep: float
        Seconds per timestep

    Returns
    -------
    inter_goal_temporal_conflict_list : list[dict[str, Union[Color, float, int, str]]]
        List of inter-goal temporal conflicts which are dictionaries of the following structure:
        {'agent_id_1': agent ID, 'agent_id_1_bomb_id': bomb ID, agent_id_1_time': time, 'agent_id_1_timestep': timestep, 
         'agent_id_1_tool_color': Color, 'agent_id_2': agent ID, 'agent_id_2_bomb_id': bomb ID,
         'agent_id_2_time': time, 'agent_id_2_timestep': timestep, 'agent_id_2_tool_color': Color, 
         'bomb_countdown_length': int, 'seconds_per_timestep': float}
    """
    # List to store inter-goal temporal conflicts
    inter_goal_temporal_conflict_list = []
    # Iterate over bomb_id_defusal_info_dict
    for bomb_id, defusal_info_list in bomb_id_defusal_info_dict.items():
        # Ignore one-stage bombs
        if len(defusal_info_list) == 1:
            continue
        # Sort defusal_info_dict in defusal_info_list in ascending timestep.
        sorted_defusal_info_list = list(sorted(defusal_info_list, key=lambda x: x['timestep']))
        # Generate corresponding inter-goal temporal conflicts relative to previous agent
        # Based on inequality:
        # 0 <= timestep_agent_id_2 - timestep_agent_id_1 <= bomb_countdown_length (in seconds) // seconds_per_timestep
        # Given how the inter-goal temporal conflict is generated, 
        # timestep_agent_id_2 - timestep_agent_id_1 >= 0 is always true
        for i in range(len(sorted_defusal_info_list) - 1):
            curr_defusal_info_dict = sorted_defusal_info_list[i]
            next_defusal_info_dict = sorted_defusal_info_list[i + 1]
            if next_defusal_info_dict['timestep'] - curr_defusal_info_dict['timestep'] > \
                curr_defusal_info_dict['bomb_countdown_length'] // seconds_per_timestep:
                inter_goal_temporal_conflict_list\
                    .append({'agent_id_1': curr_defusal_info_dict['agent_id'],
                             'agent_id_1_bomb_id': bomb_id,
                             'agent_id_1_time': curr_defusal_info_dict['timestep'] * seconds_per_timestep,
                             'agent_id_1_timestep': curr_defusal_info_dict['timestep'],
                             'agent_id_1_tool_color': curr_defusal_info_dict['tool_color'],
                             'agent_id_2': next_defusal_info_dict['agent_id'],
                             'agent_id_2_bomb_id': bomb_id,
                             'agent_id_2_time': next_defusal_info_dict['timestep'] * seconds_per_timestep,
                             'agent_id_2_timestep': next_defusal_info_dict['timestep'],
                             'agent_id_2_tool_color': next_defusal_info_dict['tool_color'],
                             'bomb_countdown_length': curr_defusal_info_dict['bomb_countdown_length'],
                             'seconds_per_timestep': seconds_per_timestep
                            })
    return inter_goal_temporal_conflict_list

def get_multi_label_a_star_h_value(curr_loc: int,
                                   curr_goal_index: int,
                                   node_ids_list: list[int],
                                   h_values_dict : dict[int, dict[int, float]]
    ) -> float:
    """
    Function to return the h value for multi-label a star search

    Parameters
    ---------- 
    curr_loc : int
        Node ID of current location
    curr_goal_index : int
        Index of Node ID of current goal in node_ids_list
    node_ids_list : list[int]
        List of node IDs that are the sequence of nodes that the agent has to visit
    h_values_dict : dict[int, dict[int, float]]
        Dictionary mapping node IDs (goal locations) to dictionary mapping node ID (other nodes) to h values from goal 
        location. 

    Returns
    -------
    h_value : float 
        H value for multi-label a star search
    """
    # H value
    h_value = 0.0
    # Add h value from current location to current goal location
    h_value += h_values_dict[node_ids_list[curr_goal_index]][curr_loc]
    # Obtain node IDs from current goal onwards
    curr_node_ids_list = node_ids_list[curr_goal_index:]
    # Iterate over goal nodes in curr_node_ids_list, starting from current goal node ID
    for i in range(len(curr_node_ids_list) - 1):
        # Add h value from one goal location to the next goal location
        h_value += h_values_dict[curr_node_ids_list[i + 1]][curr_node_ids_list[i]]
    return h_value

def get_new_node_constraints(
        algo: str,
        node: dict[str, Union[float, list, dict]],
        constraint: dict, 
    ) -> Union[tuple[list, list, list, list], tuple[list, list]]:
    """
    Function to add new constraint to existing constraints in CT node.

    Parameters
    ----------
    algo : str
        CBS algorithm ('cbs_ta', "cbs_ta_ptc")
    node : dict[str, Union[float, list, dict]]
        Node with conflicts to be resolved.

        CBS-TA-PTC
        ----------
        {'absolute_temporal_range_conflicts': List of absolute temporal range conflicts,
         'absolute_temporal_range_completion_timestep_constraints': List of completion timestep constraints from 
                                                                    absolute temporal range conflicts,
         'collisions': List of collisions,
         'collisions_constraints': List of constraints from collisions (vertex/edge collisions),
         'cost': Total path cost from all agents,
         'env_done': Whether the environment has terminated following paths, 
         'individual_cost': Dictionary mapping agent ID to respective agent's path cost,
         'inter_goal_temporal_conflicts': List of inter-goal temporal conflicts,
         'inter_goal_temporal_completion_timestep_constraints': List of completion timestep constraints from 
                                                                inter-goal temporal conflicts,
         'last_timestep': Last timestep from path in environment, 
         'paths': Dictionary mapping agent ID to path of agent,
         'path_done': Whether the paths of all agents have terminated,
         'precedence_conflicts': List of precedence conflicts,
         'precedence_completion_timestep_constraints': List of completion timestep constraints from precedence 
                                                       conflicts, 
         'root': If node is root node,  
         'score': Evaluated score of path in environment,
         'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                            agent
        }

        CBS-TA
        ------
        {'collisions': List of collisions,
         'collisions_constraints': List of constraints from collisions (vertex/edge collisions),
         'cost': Total path cost from all agents,
         'env_done': Whether the environment has terminated following paths, 
         'individual_cost': Dictionary mapping agent ID to respective agent's path cost,
         'last_timestep': Last timestep from path in environment, 
         'paths': Dictionary mapping agent ID to path of agent,
         'path_done': Whether the paths of all agents have terminated,
         'return_conflicts': List of precedence conflicts,
         'return_constraints': List of return constraints from return conflicts identical to vertex collisions 
         'root': If node is root node,  
         'score': Evaluated score of path in environment,
         'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                            agent
        }
    constraint : dict
        Constraint to be added
    """
    # CBS-TA-PTC
    if algo == 'cbs_ta_ptc':
        # Copy existing constraints
        new_node_absolute_temporal_range_completion_timestep_constraints = \
            node['absolute_temporal_range_completion_timestep_constraints'][:] 
        new_node_collision_constraints = node['collisions_constraints'][:]
        new_node_inter_goal_temporal_completion_timestep_constraints = \
            node['inter_goal_temporal_completion_timestep_constraints'][:]
        new_node_precedence_completion_timestep_constraints = \
            node['precedence_completion_timestep_constraints'][:]
        # Check for collision constraint
        if constraint['conflict_type'] == 'vertex_collision' or constraint['conflict_type'] == 'edge_collision':
            # Add collision constraint
            if constraint not in node['collisions_constraints']:
                new_node_collision_constraints += [constraint]
        # Check for absolute temporal range constraint
        elif constraint['conflict_type'] == 'absolute_temporal_range_conflict':
            # Add absolute temporal range constraint
            if constraint not in node['absolute_temporal_range_completion_timestep_constraints']:
                new_node_absolute_temporal_range_completion_timestep_constraints += [constraint]
        # Check for inter-goal temporal constraint
        elif constraint['conflict_type'] == 'inter_goal_temporal_conflict':
            # Update inter-goal temporal conflict
            if constraint not in node['inter_goal_temporal_completion_timestep_constraints']:
                new_node_inter_goal_temporal_completion_timestep_constraints += [constraint]
        # Check for completion timestep constraint
        elif constraint['conflict_type'] == 'precedence_conflict':
            # Update inter-goal temporal conflict
            if constraint not in node['precedence_completion_timestep_constraints']:
                new_node_precedence_completion_timestep_constraints += [constraint]
        return new_node_absolute_temporal_range_completion_timestep_constraints, \
               new_node_collision_constraints, new_node_inter_goal_temporal_completion_timestep_constraints, \
               new_node_precedence_completion_timestep_constraints
    # CBS-TA
    elif algo == 'cbs_ta':
        # Copy existing constraints
        new_node_collision_constraints = node['collisions_constraints'][:]
        new_node_return_constraints = node['return_constraints'][:]
        # Check for collision constraint
        if constraint['conflict_type'] == 'vertex_collision' or constraint['conflict_type'] == 'edge_collision':
            # Add collision constraint
            if constraint not in node['collisions_constraints']:
                new_node_collision_constraints += [constraint]
        elif constraint['conflict_type'] == 'return_conflict':
            # Add collision constraint
            if constraint not in node['return_constraints']:
                new_node_return_constraints += [constraint]
        return new_node_collision_constraints, new_node_return_constraints

def get_path_and_cost(goal_node: dict[str, Union[int, float, dict]]
    ) -> tuple[list[dict[str, Union[int, str, None]]], float]:
    """
    Function to return the path from a goal node

    Parameters
    ----------
    goal_node : dict[str, Union[int, float, dict]]
        Dictionary containing node parameters of the following structure:
        {'loc': node ID, 'g_val': float, 'h_val': float, 'parent': node_dict, 'timestep': int}

    Returns
    -------
    path : list[dict[str, Union[int, str, None]]]
        List containing sequence of dictionary with location (node ID) and the action to be implemented at the 
        location (node ID) that represents a path. Dictionary has the following structure:
        {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
         'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
         if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
         'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
         None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
         of tool being applied to bomb if action is 'apply_bomb_tool' else None}

        Possible Actions
        ----------------
        'go_to': Travel to next location (node ID) in path
        'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence 
                           in task allocation
    cost : float
        Cost of path
    """
    path = []
    curr = goal_node
    while curr is not None:
        path.append({'loc': curr['loc'],
                     'loc_centroid': curr['loc_centroid'], 
                     'action': 'go_to', 
                     'bomb_id': None, 
                     'bomb_countdown_length': None,
                     'bomb_location': None,
                     'bomb_sequence': None,
                     'tool_color': None
                    })
        curr = curr['parent']
    path.reverse()
    return path, goal_node['g_val']

def get_path_dict(path: list[dict[str, Union[int, str, None]]], 
                  relative_timestep: float
    ) -> dict[str, Union[str, int]]:
    """
    Function to return dictionary of a path given timestep relative to last timestep of path to index path.

    Parameters
    ----------
    path : list[dict[str, Union[int, str, None]]]
        List containing sequence of dictionaries with location (node ID) and the action to be implemented at the 
        location (node ID) that represents a path. Global path consists of all the node IDs of nodes to be visited 
        by node_ids_list. Dictionary has the following structure:
        {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
         'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
         if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
         'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
         None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
         of tool being applied to bomb if action is 'apply_bomb_tool' else None}

        Possible Actions
        ----------------
        'go_to': Travel to next location (node ID) in path
        'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence 
                           in task allocation
    relative_timestep : float
        Timestep relative to last timestep of path to index path 

    Returns
    -------
    dictionary : dict[str, Union[str, int]]
        Dictionary with node ID at the given timestep and action to be executed at stated node ID
    path_done : bool
        Whether end of path is reached
    """
    if relative_timestep < 0:
        raise ValueError(f'Relative timestep given is {relative_timestep}. Should not be negative!')
    elif relative_timestep < len(path):
        return path[int(relative_timestep)], False
    # Wait at the goal location
    else:
        return path[-1], True

def get_precedence_conflicts(
        bomb_id_defusal_info_dict: dict[str, list[dict[str, Union[float, int, str, Color, tuple[Color]]]]],
        seconds_per_timestep: float,
        agent_id_list: list[str],
        sub_bomb_dependency_order_list: Optional[list[list[int]]] = None
    ) -> list[dict[str, Union[Color, float, int, str]]]:
    """
    Function to generate precedence conflicts.

    Parameters
    ----------
    bomb_id_defusal_info_dict : dict[str, list[dict[str, Union[float, int, str, Color, tuple[Color]]]]]
        Dictionary mapping bomb ID to list containing information about bomb defusals that are dictionaries of the 
        following structure: 
        {'agent_id': agent ID, 'bomb_countdown_length': int, 'bomb_id': bomb ID, 'bomb_sequence': tuple[Color], 
         'time': time, timestep': timestep, 'tool_color': Color}
    seconds_per_timestep : float
        Seconds per timestep
    agent_id_list : list[str]
        List containing agent IDs the same order they are iterated in env.step()
    sub_bomb_dependency_order_list : Optional[list[list[int]]], default=None
        List of lists containing ordered bomb IDs according to their dependencies relevant to subtask specified by
        sub_bomb_seq_dict. Ordering is based on ordering of bomb defusal from smallest to largest index.   
    
    Returns
    -------
    precedence_conflict_list : list[dict[str, Union[Color, float, int, str]]]
        List of precedence conflicts which are dictionaries of the following structure:
        {'agent_id_1': agent ID, 'agent_id_1_bomb_id': bomb ID, 'agent_id_1_time': time, 
         'agent_id_1_timestep': timestep, 'agent_id_1_tool_color': Color, 'agent_id_2': agent ID, 
         'agent_id_2_bomb_id': bomb ID, 'agent_id_2_time': time, 'agent_id_2_timestep': timestep, 
         'agent_id_2_tool_color': Color} 
    """
    # List to store precedence conflicts
    precedence_conflict_list = []

    # Precedence conflicts can arise from two sources, ranked based on resolution priority:
    #   1) Bomb dependencies based precedence conflicts (i.e all bomb defusals for a bomb must occur before/after
    #      the bomb defusals of another bomb that is dependent on it)
    #   2) Bomb sequence based precedence conflicts (i.e bomb defusal steps must occur in order of the bomb sequence)
    
    # If there are relevant bomb dependencies
    if sub_bomb_dependency_order_list:
        # Iterate over sub_bomb_dependency_order_list
        for bomb_order_list in sub_bomb_dependency_order_list:
            # Iterate over bomb IDs in bomb_order_list 
            for i in range(len(bomb_order_list) - 1):
                # Get defusal_info_list for current bomb in defusal dependency chain
                curr_defusal_info_list = bomb_id_defusal_info_dict[bomb_order_list[i]]
                # Get defusal_info_list for next bomb in defusal dependency chain
                next_defusal_info_list = bomb_id_defusal_info_dict[bomb_order_list[i + 1]]
                # Track minimum timestep and agent when next bomb in defusal dependency chain first turns active 
                next_min_timestep = float('inf')
                next_min_timestep_agent_id = None
                next_min_timestep_tool_color = None
                for next_defusal_info_dict in next_defusal_info_list:
                    if next_defusal_info_dict['timestep'] < next_min_timestep:
                        next_min_timestep = next_defusal_info_dict['timestep']
                        next_min_timestep_agent_id = next_defusal_info_dict['agent_id']
                        next_min_timestep_tool_color = next_defusal_info_dict['tool_color']
                # Iterate over bomb defusal events in curr_defusal_info_list
                for curr_defusal_info_dict in curr_defusal_info_list:
                    # Check for any defusal step for current bomb occurs AFTER the EARLIEST defusal step of bomb further 
                    # down in the dependency chain. Add precedence conflict if so.
                    if curr_defusal_info_dict['timestep'] > next_min_timestep:
                        precedence_conflict_list\
                            .append({'agent_id_1': curr_defusal_info_dict['agent_id'],
                                     'agent_id_1_bomb_id': curr_defusal_info_dict['bomb_id'],  
                                     'agent_id_1_time': curr_defusal_info_dict['timestep'] \
                                                        * seconds_per_timestep,
                                     'agent_id_1_timestep': curr_defusal_info_dict['timestep'],
                                     'agent_id_1_tool_color': curr_defusal_info_dict['tool_color'],
                                     'agent_id_2': next_min_timestep_agent_id,
                                     'agent_id_2_bomb_id': bomb_order_list[i + 1],
                                     'agent_id_2_time': next_min_timestep * seconds_per_timestep,
                                     'agent_id_2_timestep': next_min_timestep,
                                     'agent_id_2_tool_color': next_min_timestep_tool_color
                                    })
                    # Check for any defusal step for current bomb occurs AT the EARLIEST defusal step of bomb further 
                    # down in the dependency chain. 
                    elif curr_defusal_info_dict['timestep'] == next_min_timestep:
                        # Check if agent defusal step for current bomb in env.step() is AFTER that of the EARLIEST 
                        # defusal step of bomb further down in the depenency chain. Add precedence conflict if so.
                        if agent_id_list.index(curr_defusal_info_dict['agent_id']) > \
                            agent_id_list.index(next_min_timestep_agent_id):
                            precedence_conflict_list\
                                .append({'agent_id_1': curr_defusal_info_dict['agent_id'],
                                         'agent_id_1_bomb_id': curr_defusal_info_dict['bomb_id'],  
                                         'agent_id_1_time': curr_defusal_info_dict['timestep'] \
                                                            * seconds_per_timestep,
                                         'agent_id_1_timestep': curr_defusal_info_dict['timestep'],
                                         'agent_id_1_tool_color': curr_defusal_info_dict['tool_color'],
                                         'agent_id_2': next_min_timestep_agent_id,
                                         'agent_id_2_bomb_id': bomb_order_list[i + 1],
                                         'agent_id_2_time': next_min_timestep * seconds_per_timestep,
                                         'agent_id_2_timestep': next_min_timestep,
                                         'agent_id_2_tool_color': next_min_timestep_tool_color
                                        })

    # Iterate over bomb_id_defusal_info_dict to generate bomb sequence based precedence conflicts
    for bomb_id, defusal_info_list in bomb_id_defusal_info_dict.items():
        # Obtain bomb sequence
        bomb_sequence = defusal_info_list[0]['bomb_sequence']
        # Sort defusal_info_list in order of bomb sequence based on tool color
        sorted_defusal_info_list = list(sorted(defusal_info_list,
                                               key=lambda x: list(bomb_sequence).index(x['tool_color'])))
        # Iterate over sorted_defusal_info_list
        for i in range(len(sorted_defusal_info_list)):
            for j in range(i + 1, len(sorted_defusal_info_list)):
                # Check if precedence constraint in terms of order of bomb sequence defusal is followed
                # Add precedence conflict is not followed
                if sorted_defusal_info_list[i]['timestep'] > sorted_defusal_info_list[j]['timestep']: 
                    precedence_conflict_list\
                        .append({'agent_id_1': sorted_defusal_info_list[i]['agent_id'],
                                 'agent_id_1_bomb_id': bomb_id,
                                 'agent_id_1_time': sorted_defusal_info_list[i]['timestep'] * seconds_per_timestep,
                                 'agent_id_1_timestep': sorted_defusal_info_list[i]['timestep'],
                                 'agent_id_1_tool_color': sorted_defusal_info_list[i]['tool_color'],
                                 'agent_id_2': sorted_defusal_info_list[j]['agent_id'],
                                 'agent_id_2_bomb_id': bomb_id,
                                 'agent_id_2_time': sorted_defusal_info_list[j]['timestep'] * seconds_per_timestep,
                                 'agent_id_2_timestep': sorted_defusal_info_list[j]['timestep'],
                                 'agent_id_2_tool_color': sorted_defusal_info_list[j]['tool_color']
                                })
                # Account for defusal at the same timestep
                elif sorted_defusal_info_list[i]['timestep'] == sorted_defusal_info_list[j]['timestep']:
                    # Add precedence conflict if order of bomb sequence defusal is not followed within env.step()
                    if agent_id_list.index(sorted_defusal_info_list[i]['agent_id']) > \
                        agent_id_list.index(sorted_defusal_info_list[j]['agent_id']):
                        precedence_conflict_list\
                            .append({'agent_id_1': sorted_defusal_info_list[i]['agent_id'],
                                     'agent_id_1_bomb_id': bomb_id,
                                     'agent_id_1_time': sorted_defusal_info_list[i]['timestep'] * seconds_per_timestep,
                                     'agent_id_1_timestep': sorted_defusal_info_list[i]['timestep'],
                                     'agent_id_1_tool_color': sorted_defusal_info_list[i]['tool_color'],
                                     'agent_id_2': sorted_defusal_info_list[j]['agent_id'],
                                     'agent_id_2_bomb_id': bomb_id,
                                     'agent_id_2_time': sorted_defusal_info_list[j]['timestep'] * seconds_per_timestep,
                                     'agent_id_2_timestep': sorted_defusal_info_list[j]['timestep'],
                                     'agent_id_2_tool_color': sorted_defusal_info_list[j]['tool_color']
                                    })
    return precedence_conflict_list

def get_return_conflicts(
        last_timestep: int, 
        paths: dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]], 
        past_paths: Optional[dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]] = None
    ) -> list[dict[str, Union[int, str]]]:
    """
    Function to generate return conflicts.

    Parameters
    ----------
    last_timestep : int
        Last timestep of environment
    paths : dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]
        Dictionary mapping agent ID to list containing sequence of dictionaries that represents a path. 
        Dictionary has the following structure:
        {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
         'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
         if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
         'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
         None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
         of tool being applied to bomb if action is 'apply_bomb_tool' else None}

        Possible Actions
        ----------------
        'go_to': Travel to next location (node ID) in path
        'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence
                           in task allocation
    past_paths : Optional[dict[str, list[dict[str, Union[int, str, None, Color, tuple[Color]]]]]], default=None
        Previous paths   
    
    Returns
    -------
    return_conflict_list : list[dict[str, Union[int, str]]]
        List containing return conflicts (vertex constraints) based on dictionary of following structure:
        {'agent_id': agent ID, 'conflict_type': str, 'loc': node ID, 'timestep': timestep} 
    """
    # List to store return conflicts
    return_conflict_list = []
    # Get starting timestep of path
    if past_paths == None:
        start_timestep = 0
    else:
        start_timestep = len(past_paths)
    # Iterate over paths
    for agent_id, path in paths.items():
        return_conflict_list.append({
                'agent_id': agent_id,
                'conflict_type': 'return_conflict',
                'loc': get_path_dict(path, last_timestep - start_timestep)[0]['loc'],
                'timestep': last_timestep,
            })
    return return_conflict_list

def get_sub_bomb_dependency_order_list(bomb_dependency_order_list: list[list[int]], 
                                       sub_bomb_seq_dict: dict[int, tuple[Color]]
    ) -> Optional[list[list[int]]]:
    """
    Function to generate relevant bomb dependency order list given a subtask

    Parameters
    ----------
    bomb_dependency_order_list : list[list[int]]
        List of lists containing ordered bomb IDs according to their dependencies.
        Ordering is based on ordering of bomb defusal from smallest to largest index.
    sub_bomb_seq_dict : dict[int, tuple[Color]]
        Existing ordered dictionary mapping bomb ID to bomb sequence for bomb defusal task assignment

    Returns
    -------
    sub_bomb_dependency_order_list : Optional[list[list[int]]]
        List of lists containing ordered bomb IDs according to their dependencies relevant to subtask specified by
        sub_bomb_seq_dict. Ordering is based on ordering of bomb defusal from smallest to largest index.
    """
    # Get the list of bomb IDs relevant to the subtask
    sub_bomb_id_list = list(sub_bomb_seq_dict.keys())
    # List to store relevant bomb_order_list 
    sub_bomb_dependency_order_list = []
    # Iterate over bombs in bomb_dependency_order_list
    for bomb_order_list in bomb_dependency_order_list:
        # Make a copy of bomb_order_list
        sub_bomb_order_list = bomb_order_list.copy()
        # Iterate over bomb IDs in bomb_order_list
        for bomb_id in bomb_order_list:
            # Check if bomb ID is relvant to current defusal task
            if bomb_id not in sub_bomb_id_list:
                # Remove irrelevant bomb IDs
                sub_bomb_order_list.remove(bomb_id)
        # Only append sub_bomb_order_list with more than 1 bomb ID
        # Precedence constraint is only required when considering more than 1 bomb with dependencies in the same 
        # subtask. Else, it is assumed that the global task assignment have accounted for bomb dependencies 
        if len(sub_bomb_order_list) > 1:
            sub_bomb_dependency_order_list.append(sub_bomb_order_list)
    # No relevant bomb dependencies
    if len(sub_bomb_dependency_order_list) == 0:
        return None
    else:
        return sub_bomb_dependency_order_list

def get_sub_bomb_fuse_dict(bomb_fuse_dict: dict[int, int], 
                           sub_bomb_seq_dict: dict[int, tuple[Color]]
    ) -> dict[int, tuple[int, int]]:
    """
    Function to generate relevant bomb fuse dictionary in tuple format given a subtask

    Parameters
    ----------
    bomb_fuse_dict : dict[int, int]
        Dictionary mapping bomb ID to bomb fuses
    sub_bomb_seq_dict : dict[int, tuple[Color]]
        Existing ordered dictionary mapping bomb ID to bomb sequence for bomb defusal task assignment

    Returns
    -------
    sub_bomb_fuse_dict : dict[int, tuple[int, int]]
        Dictionary mapping bomb ID to bomb fuses in tuple format to represent temporal ranges for given subtask
    """
    # Dictionary to store relevant bomb fuses 
    sub_bomb_fuse_dict = {}
    # Iterate over bomb IDs in sub_bomb_id_list
    for bomb_id in sub_bomb_seq_dict:
        # Make fuse a temporal range if its int/float
        if isinstance(bomb_fuse_dict[bomb_id], (int, float, np.int64)):
            sub_bomb_fuse_dict[bomb_id] = (0, bomb_fuse_dict[bomb_id])
        # No change if its a tuple
        elif isinstance(bomb_fuse_dict[bomb_id], tuple):
            sub_bomb_fuse_dict = bomb_fuse_dict[bomb_id]
        else:
            raise NotImplementedError("Unknown type for fuse type that is not implemented.")
    return sub_bomb_fuse_dict

def is_collision_constrained(
        curr_loc: int, 
        next_loc: int, 
        next_time: float, 
        collision_constraint_dict: dict[float, list[dict[str, Union[int, list[int], float, str]]]]
    ) -> bool:
    """
    Function to check if motion violates collision constraints for a given agent

    Parameters
    ----------
    curr_loc : int
        Node ID of current location at current timestep
    next_loc : int
        Node ID of next location at next timestep (current timestep + 1)
    next_time : float
        Next timestep (current timestep + 1)
    collision_constraint_dict : dict[float, list[dict[str, Union[int, list[int], float, str]]]]
        Dictionary mapping timesteps to constraints for the specified agent.
        Constraints are of the following format:
        {'agent': agent ID, 'conflict_type': 'edge_collision'/'vertex_collision', 
         'loc': location (node ID) of vertex (list of len 1)/edge collision (list of len 2), 
         'timestep': float (at the end for edge collision)}
    
    Returns
    -------
    is_collision_contrained : bool
        Whether movement violates collision constraints
    """
    # Check if constraint at next timestep is in dictionary for given agent
    if next_time in collision_constraint_dict:
        # Iterate over constraints
        for constraint in collision_constraint_dict[next_time]:
            # Vertex collision
            if len(constraint['loc']) == 1 and constraint['loc'][0] == next_loc:
                return True
            # Edge collision
            elif len(constraint['loc']) == 2 and constraint['loc'][0] == curr_loc and \
                constraint['loc'][1] == next_loc:
                return True
    return False

def is_return_constrained(
        curr_loc: int, 
        next_loc: int, 
        next_time: float, 
        return_constraint_dict: dict[float, list[dict[str, Union[int, str]]]]
    ) -> bool:
    """
    Function to check if motion violates collision constraints for a given agent

    Parameters
    ----------
    curr_loc : int
        Node ID of current location at current timestep
    next_loc : int
        Node ID of next location at next timestep (current timestep + 1)
    next_time : float
        Next timestep (current timestep + 1)
    return_constraint_dict : dict[float, list[dict[str, Union[int, str]]]]
        Dictionary mapping timesteps to return constraints for the specified agent.
        Constraints are of the following format:
        {'agent_id': agent ID, 'conflict_type': str, 'loc': node ID, 'timestep': timestep}
    
    Returns
    -------
    is_return_contrained : bool
        Whether movement violates return constraints
    """
    # Check if constraint at next timestep is in dictionary for given agent
    if next_time in return_constraint_dict:
        # Iterate over constraints
        for constraint in return_constraint_dict[next_time]:
            # Vertex collision
            if constraint['loc'] == next_loc:
                return True
    return False

def is_temporal_constrained(
        defuse_bomb_tup: tuple[int, Color],
        defuse_bomb_timestep: float, 
        temporal_constraint_dict: dict[tuple[int, Color], dict[str, Union[Callable, Color, float, str]]]
    ) -> tuple[bool, Optional[bool]]:
    """
    Function to check if path violates temporal constraints for a given agent

    Parameters
    ----------
    defuse_bomb_tup : tuple[int, Color]
        Tuple of (bomb ID, Color) representing atomic defusal task
    defuse_bomb_timestep : float
        Timestep where the bomb is being defused
    temporal_constraint_dict : dict[tuple[int, Color], dict[str, Union[Callable, Color, float, str]]]
        Dictionary mapping (bomb ID, Color) tuple to temporal constraints for the given agent.
        Constraints are of the following format:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
         'tool_color': Color, 'value': float}

    Returns
    -------
    is_temporal_constrained : bool
        Whether movement violates any inter-goal temporal constraint
    temporal_future_valid : Optional[bool] 
        Whether it is possible for future paths to be valid.
    """
    # Iterate over bomb IDs and their corresponding inter-goal temporal constraints
    for bomb_tup, temporal_constraint_list in temporal_constraint_dict.items():
        # Bomb being defused has inter-goal temporal constraints
        if defuse_bomb_tup == bomb_tup:
            # Iterate over inter-goal temporal constraints
            for temporal_constraint in temporal_constraint_list:
                # Agent does not satisfy inter-goal temporal constraint
                if not temporal_constraint['operator'](defuse_bomb_timestep, temporal_constraint['value']):
                    # Inter-goal temporal constraint requires defusual timestep to be: 
                    # 1) Less than, 2) Less than or equal or 3) Equal to value specified
                    # Valid paths in the future don't exist as timestep always increases
                    if temporal_constraint['operator'] == operator.lt or \
                        temporal_constraint['operator'] == operator.le or \
                        temporal_constraint['operator'] == operator.eq:
                        return True, False
                    # Valid paths in the future may exist
                    else:
                        return True, True
            # Agent satisfy all inter-goal temporal constraints for bomb with given bomb ID
            return False, None
    # Bomb with given bomb ID does not have any inter-goal temporal constraints (i.e. 1 stage bombs)
    return False, None

def is_solvable_temporal_constraint(
        precedence_completion_timestep_constraint_dict: dict[str, dict[str, Union[Callable, float, str]]],
        inter_goal_temporal_completion_timestep_constraint_dict: dict[str, dict[str, Union[Callable, float, str]]],
        absolute_temporal_range_completion_timestep_constraint_dict: dict[str, dict[str, Union[Callable, float, str]]],
        mission_length: float,
    ) -> bool:
    """
    Function to check if there exist a solution given temporal constraints (completion timestep and inter-goal temporal)
    for a given agent.

    Parameters
    ----------
    precedence_completion_timestep_constraint_dict : dict[str, dict[str, Union[Callable, float, str]]]
        Dictionary mapping bomb ID to precedence completion timestep constraints for the given agent.
        Constraints are of the following format:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'loc': node ID, 
         'operator': operator Callable, 'value': float}
    inter_goal_temporal_completion_timestep_constraint_dict : dict[str, dict[str, Union[Callable, float, str]]]
        Dictionary mapping bomb ID to inter-goal temporal completion timestep constraints for the given agent.
        Constraints are of the following format:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'loc': node ID, 
         'operator': operator Callable, 'value': float}
    absolute_temporal_range_completion_timestep_constraint_dict : dict[str, dict[str, Union[Callable, float, str]]]
        Dictionary mapping bomb ID to absolute temporal range completion timestep constraints for the given agent.
        Constraints are of the following format:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'loc': node ID, 
         'operator': operator Callable, 'value': float}
    mission_length : float
        Total mission length, in seconds

    Returns
    -------
    is_solvable_temporal_constraint : bool
        Whether there exist a solution given temporal constraints (completion timestep and inter-goal temporal)
        for a given agent.
    """
    # Variables for solver. Linprog only accepts constraints in less than equal / equal format
    # Coefficients of the linear objective function to be minimized
    c = [1]
    # Inequality constraint matrix. Each row of A_ub specifies the coefficients of a linear inequality constraint on x.
    A_ub = []
    # Inequality constraint vector. Each element represents an upper bound on the corresponding value of A_ub @ x.
    b_ub = []
    # Equality constraint matrix. Each row of A_eq specifies the coefficients of a linear equality constraint on x.
    A_eq = []
    # Equality constraint vector. Each element of A_eq @ x must equal the corresponding element of b_eq.
    b_eq = []
    # Bound for x
    x_bounds = (0, int(mission_length))

    # Add coefficients of constraints to respective solver variables
    # Iterate over completion timestep constraints
    for precedence_completion_timestep_constraint_list in precedence_completion_timestep_constraint_dict.values():
        for precedence_completion_timestep_constraint in precedence_completion_timestep_constraint_list:
            # Checks x < value 
            if precedence_completion_timestep_constraint['operator'] == operator.lt:
                # Equivalent to checking x <= value - 1 as timestep is discrete
                A_ub.append([1])
                b_ub.append(precedence_completion_timestep_constraint['value'] - 1)
            # Checks x <= value
            elif precedence_completion_timestep_constraint['operator'] == operator.le:
                # No change
                A_ub.append([1])
                b_ub.append(precedence_completion_timestep_constraint['value'])
            # Checks x == value
            elif precedence_completion_timestep_constraint['operator'] == operator.eq:
                # No change
                A_eq.append([1])
                b_eq.append(precedence_completion_timestep_constraint['value'])
            # Checks if x >= value
            elif precedence_completion_timestep_constraint['operator'] == operator.ge:
                # Equivalent to checking -x <= -value
                A_ub.append([-1])
                b_ub.append(-precedence_completion_timestep_constraint['value'])
            # Checks if x > value
            elif precedence_completion_timestep_constraint['operator'] == operator.gt:
                # Equivalent to checking -x <= -value - 1 as timestep is discrete
                A_ub.append([-1])
                b_ub.append(-precedence_completion_timestep_constraint['value'] - 1)
    # Iterate over inter-goal temporal constraints
    for inter_goal_temporal_completion_timestep_constraint_list in \
        inter_goal_temporal_completion_timestep_constraint_dict.values():
        for inter_goal_temporal_completion_timestep_constraint in \
            inter_goal_temporal_completion_timestep_constraint_list:
            # Checks x < value 
            if inter_goal_temporal_completion_timestep_constraint['operator'] == operator.lt:
                # Equivalent to checking x <= value - 1 as timestep is discrete
                A_ub.append([1])
                b_ub.append(inter_goal_temporal_completion_timestep_constraint['value'] - 1)
            # Checks x <= value
            elif inter_goal_temporal_completion_timestep_constraint['operator'] == operator.le:
                # No change
                A_ub.append([1])
                b_ub.append(inter_goal_temporal_completion_timestep_constraint['value'])
            # Checks x == value
            elif inter_goal_temporal_completion_timestep_constraint['operator'] == operator.eq:
                # No change
                A_eq.append([1])
                b_eq.append(inter_goal_temporal_completion_timestep_constraint['value'])
            # Checks if x >= value
            elif inter_goal_temporal_completion_timestep_constraint['operator'] == operator.ge:
                # Equivalent to checking -x <= -value
                A_ub.append([-1])
                b_ub.append(-inter_goal_temporal_completion_timestep_constraint['value'])
            # Checks if x > value
            elif inter_goal_temporal_completion_timestep_constraint['operator'] == operator.gt:
                # Equivalent to checking -x <= -value - 1 as timestep is discrete
                A_ub.append([-1])
                b_ub.append(-inter_goal_temporal_completion_timestep_constraint['value'] - 1)
    # Iterate over absolute temporal range constraints
    for absolute_temporal_range_completion_timestep_constraint_list in \
        absolute_temporal_range_completion_timestep_constraint_dict.values():
        for absolute_temporal_range_completion_timestep_constraint in \
            absolute_temporal_range_completion_timestep_constraint_list:
            # Checks x < value 
            if absolute_temporal_range_completion_timestep_constraint['operator'] == operator.lt:
                # Equivalent to checking x <= value - 1 as timestep is discrete
                A_ub.append([1])
                b_ub.append(absolute_temporal_range_completion_timestep_constraint['value'] - 1)
            # Checks x <= value
            elif absolute_temporal_range_completion_timestep_constraint['operator'] == operator.le:
                # No change
                A_ub.append([1])
                b_ub.append(absolute_temporal_range_completion_timestep_constraint['value'])
            # Checks x == value
            elif absolute_temporal_range_completion_timestep_constraint['operator'] == operator.eq:
                # No change
                A_eq.append([1])
                b_eq.append(absolute_temporal_range_completion_timestep_constraint['value'])
            # Checks if x >= value
            elif absolute_temporal_range_completion_timestep_constraint['operator'] == operator.ge:
                # Equivalent to checking -x <= -value
                A_ub.append([-1])
                b_ub.append(-absolute_temporal_range_completion_timestep_constraint['value'])
            # Checks if x > value
            elif absolute_temporal_range_completion_timestep_constraint['operator'] == operator.gt:
                # Equivalent to checking -x <= -value - 1 as timestep is discrete
                A_ub.append([-1])
                b_ub.append(-absolute_temporal_range_completion_timestep_constraint['value'] - 1)

    # Minimises c @ x such that
    # A_ub @ x <= b_ub
    # A_eq @ x == b_eq
    # lb <= x <= ub
    if len(A_ub) != 0 and len(b_ub) != 0:
        if len(A_eq) != 0 and len(b_eq) != 0:
            result = linprog(c, A_ub=A_ub, b_ub=b_ub, A_eq=A_eq, b_eq=b_eq, bounds=[x_bounds], integrality=1)
        else:
            result = linprog(c, A_ub=A_ub, b_ub=b_ub, bounds=[x_bounds], integrality=1)
        # Check if there exist a solution
        if result.success:
            return True
        else:
            return False
    else:
        return True

def push_multi_label_a_star_node(open_list: list[dict[str, Union[int, float, dict]]], 
                                 node: dict[str, Union[int, float, dict]]
    ):
    """
    Function to push node to open list for multi-label a star search.
    Breaks ties based on the criteria (lower the better):
        1) g_value + h_value  
        2) h_value
        3) - goal_loc_ind
        3) node_location (i.e. node ID)
        4) random

    Parameters
    ----------
    open_list : list[dict[str, Union[int, float, dict]]]
        Open list
    node : dict[str, Union[int, float, dict]]
        Dictionary with parameters of node pushed to heap with the following structure:
        {'loc': node ID, 'g_val': float, 'h_val': float, 'parent': node_dict, 'timestep': float}
    """
    heapq.heappush(open_list, (node['g_val'] + node['h_val'], 
                               node['h_val'], 
                               - node['goal_loc_ind'], 
                               node['loc'], 
                               random.random(), 
                               node))

def pop_multi_label_a_star_node(open_list: list[dict[str, Union[int, float, dict]]]) -> dict:
    """
    Function to pop node to open list for multi-label a star search.

    Parameters
    ----------
    open_list : list[dict[str, Union[int, float, dict]]]
        Open list
    """
    _, _, _, _, _, curr = heapq.heappop(open_list)
    return curr

def push_multi_step_a_star_node(open_list: list[dict[str, Union[int, float, dict]]], 
                                node: dict[str, Union[int, float, dict]]
    ):
    """
    Function to push node to open list for multi-step a star search.
    Breaks ties based on the criteria (lower the better):
        1) g_value + h_value  
        2) h_value
        3) node_location (i.e. node ID)
        4) random

    Parameters
    ----------
    open_list : list[dict[str, Union[int, float, dict]]]
        Open list
    node : dict[str, Union[int, float, dict]]
        Dictionary with parameters of node pushed to heap with the following structure:
        {'loc': node ID, 'g_val': float, 'h_val': float, 'parent': node_dict, 'timestep': float}
    """
    heapq.heappush(open_list, (node['g_val'] + node['h_val'], node['h_val'], node['loc'], random.random(), node))

def pop_multi_step_a_star_node(open_list: list[dict[str, Union[int, float, dict]]]) -> dict:
    """
    Function to pop node to open list for multi-step a star search.

    Parameters
    ----------
    open_list : list[dict[str, Union[int, float, dict]]]
        Open list
    """
    _, _, _, _, curr = heapq.heappop(open_list)
    return curr

def resolve_absolute_temporal_range_conflict(
        absolute_temporal_range_conflict: dict[str, Union[Callable, float, str]]
    ) -> list[dict[str, Union[Callable, float, str]]]:
    """
    Function to return an absolute temporal range constraint

    Parameters
    ----------
    absolute_temporal_range_conflict : dict[str, Union[Callable, float, str]]
        An absolute temporal range conflict is a dictionary of the following structure:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
         'tool_color': Color, 'value': float}

    Returns
    -------
    absolute_temporal_range_completion_timestep_constraint_list : list[dict[str, Union[Callable, float, str]]]
        List of absolute temporal range completion timestep constraints that are dictionaries of the following 
                   structure:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
         'tool_color': Color, 'value': float}
    """
    # An absolute_temporal_range_conflict is essentially a constraint on its own
    return [absolute_temporal_range_conflict]

def resolve_collision(collision: dict[str, Union[int, list[int], float]]
    ) -> list[dict[str, Union[int, list[int], float, str]]]:
    """
    Function to return a list of (two) constraints to resolve the given collision

    Parameters
    ----------
    collision : dict[str, Union[int, list[int]]]
        A collision is based on dictionary of following structure:
        {'agent_id_1': agent ID, 'agent_id_2': agent ID, 'loc': [node ID] (vertex collision) / 
         [node ID, node ID] (edge collision), 'timestep': float}
    
    Returns
    -------
    collision_constraints_list : list[dict[str, Union[int, list[int]]]]
        List of (two) constraints that are dictionaries with the following structure:
        {'agent': agent ID, 'conflict_type': 'edge_collision'/'vertex_collision', 
         'loc': location (node ID) of vertex (list of len 1)/edge collision (list of len 2), 
         'timestep': float (at the end for edge collision)}

        Vertex collision: The first constraint prevents the first agent to be at the specified location at the
                          specified timestep, and the second constraint prevents the second agent to be at the
                          specified location at the specified timestep.
        Edge collision: The first constraint prevents the first agent to traverse the specified edge at the
                        specified timestep, and the second constraint prevents the second agent to traverse the
                        specified edge at the specified timestep
    """
    collision_constraints_list = []
    if len(collision['loc']) == 1:
        collision_constraints_list.append({'agent_id': collision['agent_id_1'],
                                           'conflict_type': 'vertex_collision', 
                                           'loc': collision['loc'], 
                                           'timestep': collision['timestep']
                                          })
        collision_constraints_list.append({'agent_id': collision['agent_id_2'],
                                           'conflict_type': 'vertex_collision', 
                                           'loc': collision['loc'], 
                                           'timestep': collision['timestep']
                                          })
    elif len(collision['loc']) == 2:
        collision_constraints_list.append({'agent_id': collision['agent_id_1'],
                                           'conflict_type': 'edge_collision', 
                                           'loc': collision['loc'], 
                                           'timestep': collision['timestep']
                                          })
        collision_constraints_list.append({'agent_id': collision['agent_id_2'],
                                           'conflict_type': 'edge_collision', 
                                           'loc': list(reversed(collision['loc'])), 
                                           'timestep': collision['timestep']
                                          })
    return collision_constraints_list

def resolve_inter_goal_temporal_conflict(inter_goal_temporal_conflict: dict[str, Union[Color, float, int, str]]
    ) -> list[dict[str, Union[Callable, Color,float, str]]]:
    """
    Function to return a list of (two) temporal constraints to resolve a inter-goal temporal conflict

    Parameters
    ----------
    inter_goal_temporal_conflict : dict[str, Union[Color, float, int, str]]
        Inter-goal temporal conflict which is a dictionary of the following structure:
        {'agent_id_1': agent ID, 'agent_id_1_bomb_id': bomb ID, agent_id_1_time': time, 'agent_id_1_timestep': timestep, 
         'agent_id_1_tool_color': Color, 'agent_id_2': agent ID, 'agent_id_2_bomb_id': bomb ID,
         'agent_id_2_time': time, 'agent_id_2_timestep': timestep, 'agent_id_2_tool_color': Color, 
         'bomb_countdown_length': int, 'seconds_per_timestep': float}
    
    Returns
    -------
    inter_goal_temporal_completion_timestep_constraint_list : list[dict[str, Union[Callable, Color,float, str]]]
        List of inter-goal temporal completion timestep constraints that are dictionaries of the following structure:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
         'tool_color': Color, 'value': float}
    """
    # List to store inter-goal temporal completion timestep constraints
    inter_goal_temporal_completion_timestep_constraint_list = []
    # Constraining agent 1 by making it wait
    inter_goal_temporal_completion_timestep_constraint_list\
        .append({'agent_id': inter_goal_temporal_conflict['agent_id_1'],
                 'bomb_id': inter_goal_temporal_conflict['agent_id_1_bomb_id'],
                 'conflict_type': 'inter_goal_temporal_conflict',
                 'operator': operator.ge,
                 'tool_color': inter_goal_temporal_conflict['agent_id_1_tool_color'],
                 'value': inter_goal_temporal_conflict['agent_id_2_timestep'] - \
                          inter_goal_temporal_conflict['bomb_countdown_length'] // \
                          inter_goal_temporal_conflict['seconds_per_timestep'] 
                })
    # Constraining agent 2 by making it arrive faster
    inter_goal_temporal_completion_timestep_constraint_list\
        .append({'agent_id': inter_goal_temporal_conflict['agent_id_2'],
                 'bomb_id': inter_goal_temporal_conflict['agent_id_2_bomb_id'],
                 'conflict_type': 'inter_goal_temporal_conflict',
                 'operator': operator.lt,
                 'tool_color': inter_goal_temporal_conflict['agent_id_2_tool_color'],
                 'value': inter_goal_temporal_conflict['agent_id_2_timestep']
                }) 
    return inter_goal_temporal_completion_timestep_constraint_list

def resolve_precedence_conflict(precedence_conflict: dict[str, Union[Color, float, int, str]]
    ) -> list[dict[str, Union[Callable, Color,float, str]]]:
    """
    Function to return a list of (two) completion timestep constraints to resolve precedence conflict

    Parameters
    ----------
    precedence_conflict : list[dict[str, Union[Color, float, int, str]]]
        Precedence conflict which is a dictionary of the following structure:
        {'agent_id_1': agent ID, 'agent_id_1_bomb_id': bomb ID, 'agent_id_1_time': time, 
         'agent_id_1_timestep': timestep, 'agent_id_1_tool_color': Color, 'agent_id_2': agent ID, 
         'agent_id_2_bomb_id': bomb ID, 'agent_id_2_time': time, 'agent_id_2_timestep': timestep, 
         'agent_id_2_tool_color': Color} 

    Returns
    -------
    precedence_completion_timestep_constraint_list : list[dict[str, Union[Callable, float, str]]]
        List of precedence completion timestep constraints that are dictionaries of the following structure:
        {'agent_id': agent ID, 'bomb_id': bomb ID, 'conflict_type': str, 'operator': operator Callable, 
         'tool_color': Color, 'value': float}
    """
    # List to store precedence completion timestep constraints
    precedence_completion_timestep_constraint_list = []
    # Make agent 2 complete task after agent 1
    precedence_completion_timestep_constraint_list.append({'agent_id': precedence_conflict['agent_id_2'],
                                                           'bomb_id': precedence_conflict['agent_id_2_bomb_id'],
                                                           'conflict_type': 'precedence_conflict',
                                                           'operator': operator.gt,
                                                           'tool_color': precedence_conflict['agent_id_2_tool_color'],
                                                           'value': precedence_conflict['agent_id_1_timestep'] 
                                                          })
    # Make agent 1 complete task faster (by at least one timestep)
    precedence_completion_timestep_constraint_list.append({'agent_id': precedence_conflict['agent_id_1'],
                                                           'bomb_id': precedence_conflict['agent_id_1_bomb_id'],
                                                           'conflict_type': 'precedence_conflict',
                                                           'operator': operator.le,
                                                           'tool_color': precedence_conflict['agent_id_1_tool_color'],
                                                           'value': precedence_conflict['agent_id_1_timestep'] - 1
                                                          })   
    return precedence_completion_timestep_constraint_list

def resolve_return_conflict(return_conflict: dict[str, Union[int, str]]) -> list[dict[str, Union[int, str]]]:
    """
    Function to return a return constraint

    Parameters
    ----------
    return_conflict : dict[str, Union[int, str]]
        An return conflict is a dictionary of the following structure:
        {'agent_id': agent ID, 'conflict_type': str, 'loc': node ID, 'timestep': timestep}

    Returns
    -------
    absolute_temporal_range_completion_timestep_constraint_list : list[dict[str, Union[Callable, float, str]]]
        List of return constraints that are dictionaries of the following structure:
        {'agent_id': agent ID, 'conflict_type': str, 'loc': node ID, 'timestep': timestep}
    """
    # A return conflict is essentially a vertex constraint on its own
    return [return_conflict]