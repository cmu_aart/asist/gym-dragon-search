import signal
import os

from .cbs import CBSSolver

from BaseAgent import MissionContext
from gym_dragon.bridge import DragonBridgeEnv
from MinecraftBridge.messages import MissionStageTransition, MissionStage


class CBSBridge(CBSSolver):
    """
    """

    def __init__(self, context: MissionContext, solution_timeout: float = 60.0, *args, **kwargs):
        """
        Parameters
        ----------
        context : MissionContext
            Mission context
        solution_timeout : float, default=60.0
            Timeout in seconds to find solution
        """
        super().__init__(*args, **kwargs)
        self.context = context
        self.root_env = DragonBridgeEnv(
            context,
            mission_length=kwargs.get('mission_length'),
            recon_phase_length=kwargs.get('recon_phase_length'),
        )

        # Timeout to generate plan
        self.solution_timeout = solution_timeout
        # Track pid of generate_plan()
        self.pid = None

        # Receive messages upon mission state changes
        self.context.minecraft_interface.register_callback(
            MissionStageTransition, self.__onMissionStageTransition)
        # Receive messages upon player inventory update
        self.context.minecraft_interface.register_callback(
            PlayerInventoryUpdate, self.__onPlayerInventoryUpdate)

    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """
        self.shop_stage = True if message.mission_stage == MissionStage.ShopStage else False

    def __onPlayerInventoryUpdate(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlayerInventoryUpdate
            Received PlayerInventoryUpdate message
        """
        # Generate plan when players are at the shop and their inventories are updated
        if self.shop_stage:
            # Kill existing plan generation
            if self.pid:
                os.kill(self.pid, signal.SIGTERM)
            self.generate_plan()

    def generate_plan(self):
        """
        Generate a plan and send via the Redis bus during shop stage when player inventories are updated

        solution_node : dict
            Solution node in the form of a dictionary of the following format:
            {'cost': Total path cost from all agents,
             'last_time': Last time (in seconds) from path in environment,    
             'last_timestep': Last timestep from path in environment, 
             'paths': Dictionary mapping agent ID to path of agent,
             'score': Evaluated score of path in environment,
             'task allocation': Dictionary mapping agent ID to a list containing sequence of tasks / bomb IDs for each 
                                agent
            }
        path : list[dict[str, Union[int, str, None]]]
            List containing sequence of dictionary with location (node ID) and the action to be implemented at the 
            location (node ID) that represents a path. Dictionary has the following structure:
            {'loc': node ID, 'loc_centroid': (x, z) centroid of node, 
             'action': action to be executed after reaching stated node ID, 'bomb_id': bomb ID of bomb
             if action is 'apply_bomb_tool' else None, 'bomb_countdown_length': countdown length of bomb if action is 
             'apply_bomb_tool' else None, 'bomb_location': (x, z) location of bomb if action is 'apply_bomb_tool' else 
             None, 'bomb_sequence': bomb sequence of bomb if action is 'apply_bomb_tool' else None, 'tool_color': Color 
             of tool being applied to bomb if action is 'apply_bomb_tool' else None}

            Possible Actions
            ----------------
            'go_to': Travel to next location (node ID) in path
            'apply_bomb_tool': Apply bomb tool at bomb at current location (node ID) of specified color in bomb sequence 
                               in task allocation
        """
        # Get pid
        self.pid = os.getpid()

        # Define a signal handler function
        def signal_handler(sig, frame):
            print("Received signal {}. Terminating...".format(sig))
            raise SystemExit

        # Register the signal handler for the SIGTERM signal
        signal.signal(signal.SIGTERM, signal_handler)

        # Find solution
        try:
            solution_node, _, _, _, _ = self.find_solution(solution_timeout=self.solution_timeout)
        except SystemExit:
            print("Task terminated by signal")

        self.context.redis_interface.send(solution_node, channel='CBS/plan')

        # Reset pid
        self.pid = None

    def env_reset(self, pre_compute_heuristics: bool = False):
        """
        Reset the solver environment.

        Parameters
        ----------
        pre_compute_heuristics : bool, default=False
            Whether to pre-compute heuristics
        """
        # Reset environment
        self.env.reset(env=self.root_env, seed=self.random_seed)

        # Pre-compute heuristics if necessary
        if pre_compute_heuristics:
            self.env.pre_compute_heuristics(
                seed=self.random_seed, additional_node_id_list=[])
