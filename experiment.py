import matplotlib.pyplot as plt
import numpy as np
import os
import seaborn as sns
import shutil
import time

from gym_dragon_search.cbs import solve
from gym_dragon.core import Region
from typing import Optional

def get_results(
        algo: str,
        num_bombs_per_region: int,
        num_bombs_sub_task: int,
        seconds_per_timestep: float,
        task_allocation_heuristic: str,
        pruning_heuristic: str,
        low_level_search: str,
        solution_timeout: float,
        verbose: bool = True,
        **kwargs
    ):
    """
    Function to run an instance of experiment

    Parameters
    ----------
    algo : str, default='cbs_ta_ptc'
        CBS algorithm ('cbs_ta', "cbs_ta_ptc")
    num_bombs_per_region : int
        Number of bombs to be placed randomly in each region
    num_bombs_sub_task : int
        Number of bombs to resolve per CBS-TA-PTC search
    seconds_per_timestep : float
        Number of seconds per timestep in the environment
    task_allocation_heuristic : str
        Heuristic to order tasks
    pruning_heuristic : str
        Heuristic to prune subtask allocations
    low_level_search : str
        Algorithm for low level search. Either 'multi_step_a_star' or 'multi_label_a_star'
    solution_timeout : float
        Timeout in seconds to find solution
    verbose: bool = True
        Whether to print relevant information
    
    Keyword Arguments
    -----------------
    mission_length : float, default=10*60
        Mission length, in seconds
    recon_phase_length : float, default=2*60
        Length of reconnaissance phase, in seconds
    valid_regions : set[Region], optional
        Set of regions to include in the environment
    start_regions : set[Region], optional
        Set of valid regions for initial mission locations
    epsilon : float, default=1.0
        Suboptimality factor for score. Default is optimal
    same_start_loc : bool, default=True
        Whether for all agents to start at the same location
        NOTE: Default DragonBaseEnv starts at same location/node
    include_collisions : bool, default=False
        Whether to consider collisions for environment
        NOTE: Default DragonBaseEnv has no collisions
    include_chained_bombs : bool, default=True
        Whether to include chained bombs in the environment
        if self.
    include_fire_bombs : bool, default=False
        Whether to include fire bombs in the environment
    include_fuse_bombs : bool, default=True
        Whether to include fuse bombs in the environment

    Returns
    -------
    success : bool
        Whether solver is successful finding solution given epsilon
    compute_time : float
        Cpu time required to generate solution
    optimal_ratio : float
        Ratio of solution score to maximum score
    """
    # Print relevant information
    if verbose:
        print('\n Solving instance with the following parameters: \n')
        print(f'Algorithm: {algo}')
        print(f'Number of bombs per region: {num_bombs_per_region}')
        print(f'Number of bombs per subtask: {num_bombs_sub_task}')
        print(f'Seconds per timestep: {seconds_per_timestep}')
        print(f'Task allocation heuristic: {task_allocation_heuristic}')
        print(f'Pruning heuristic: {pruning_heuristic}')
        print(f'Low-level heuristic: {low_level_search}')

    # Try till a solvable instance (given bomb and tool allocation) is generated
    while True:
        solution_node, success, compute_time_list, optimality_ratio_list, exception  = \
            solve(solution_timeout=solution_timeout,
                  algo=algo,
                  num_bombs_sub_task=num_bombs_sub_task,
                  mission_length=kwargs.get('mission_length', 10*60),
                  recon_phase_length=kwargs.get('recon_phase_length', 2*60),   
                  seconds_per_timestep=seconds_per_timestep,
                  valid_regions=kwargs.get('valid_regions', set[Region]),
                  num_bombs_per_region=num_bombs_per_region,
                  budget_per_region=None,
                  start_regions=kwargs.get('start_regions', set[Region]),
                  epsilon=kwargs.get('epsilon', set[Region]),
                  same_start_loc=kwargs.get('same_start_loc', True),
                  include_collisions=kwargs.get('include_collisions', False),
                  include_chained_bombs=kwargs.get('include_chained_bombs', True),
                  include_fire_bombs=kwargs.get('include_fire_bombs', False),
                  include_fuse_bombs=kwargs.get('include_fuse_bombs', True),
                  bomb_explode_terminate=kwargs.get('bomb_explode_terminate', True), 
                  task_allocation_heuristic=task_allocation_heuristic,
                  pruning_heuristic=pruning_heuristic,
                  low_level_search=low_level_search,
                  save_render=False,
                  verbose=False,
                  render=False)
        # For the case with no possible solutions (given bomb and tool allocation), re-run solver
        if 'For given bombs and tools from agents, task has no solution' in str(exception):
            # Print relevant information
            if verbose:
                print('Caught Exception: ', str(exception))
                print('Trying to generate another environment instance to solve.')
            continue
        # Timeout
        elif 'timeout' in str(exception).lower():
            # Print relevant information
            if verbose:
                print('Caught Exception: ', str(exception))
                print('Solver failed to find solution due to timeout.')
            return False, solution_timeout, optimality_ratio_list[-1]
        else:
            # Found solution of desired optimality
            if success:
                # Print relevant information
                if verbose:
                    print('Solver successfully found a solution of desired optimality.')
                return success, compute_time_list[-1], optimality_ratio_list[-1]
            # All other cases --> Solver failed to find solution 
            # NOTE: TASK INSTANCE MAY ALREADY BE UNSOLVABLE (e.g due to fuses)
            else:
                # Print relevant information
                if verbose:
                    print('Solver failed to find solution of desired optimality.')
                return success, compute_time_list[-1], optimality_ratio_list[-1]

def get_visualisations(
        num_of_iterations: int,
        solution_timeout: float,
        num_bombs_per_region_list: list[int],
        num_bombs_sub_task_list: list[int],
        seconds_per_timestep_list: list[float],
        task_allocation_heuristic_list: list[str],
        pruning_heuristic_list: list[str],
        low_level_search_list: list[str],
        algorithm_list: list[str],
        data_save_path: str,
        visualisation_save_path: str,
        experiment_name: Optional[str] = None,
    ):
    """
    Function to generate visualisations from experiments.

    Parameters
    ----------
    num_of_iterations : int
        Number of iterations to run
    solution_timeout : float
        Timeout in seconds to find solution
    num_bombs_per_region_list : list[int]
        List containing num_bombs_per_region
    num_bombs_sub_task_list : list[int]
        List containing num_bombs_sub_task
    seconds_per_timestep_list : list[float]
        List containing seconds_per_timestep
    task_allocation_heuristic_list : list[str]
        List containing task allocation heuristics
    pruning_heuristic_list : list[str]
        List containing pruning heuristics
    low_level_search_list : list[str]
        List containing low-level search methods
    algorithm_list : list[str]
        List containing algorithms
    data_save_path : str
        Path to load data
    visualisation_save_path : str
        Path to save visualisations
    experiment_name : Optional[str]
        Experiment name for save path
    """
    # Get experiment name
    if experiment_name == None:
        experiment_name = f'max_num_bombs_per_region_{num_bombs_per_region_list[-1]}_' + \
                          f'max_num_bombs_sub_task_{num_bombs_sub_task_list[-1]}_' + \
                          f'max_seconds_per_timestep_{seconds_per_timestep_list[-1]}_' + \
                          f"task_heur_{'_'.join(task_allocation_heuristic_list)}_" + \
                          f"pruning_heur_{'_'.join(pruning_heuristic_list)}_" + \
                          f"low_level_{'_'.join(low_level_search_list)}_" + \
                          f"algo_{'_'.join(algorithm_list)}_" + \
                          f'num_of_iterations_{num_of_iterations}_' + \
                          f'solution_timeout_{solution_timeout}'

    # Get save path for visualisations
    save_path = visualisation_save_path + experiment_name + '/'
    if not os.path.exists(save_path):
        # Create directory if it doesn't exist
        os.makedirs(save_path)
    else:
        # Remove and make new directory if it exist
        shutil.rmtree(save_path)
        os.makedirs(save_path)

    # Set seaborn theme
    sns.set_theme(context='paper', style='ticks')

    # Load data arrays
    success_data_arr = np.load(data_save_path + experiment_name + '/' + 'success.npy')
    compute_time_data_arr = np.load(data_save_path + experiment_name + '/' + 'compute_time.npy')
    optimal_ratio_data_arr = np.load(data_save_path + experiment_name + '/' + 'optimal_ratio.npy')
    # Obtain compute time for successful tries
    success_compute_time_data_arr = np.ma.masked_array(compute_time_data_arr, np.logical_not(success_data_arr))

    # Figure 1: Number of bombs per region plots

    # Array to store data arrays for specific algorithm
    algorithm_averaged_success_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_averaged_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_averaged_optimal_ratio_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_averaged_success_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_success_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_optimal_ratio_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_success_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    # Get average and std between algorithms
    for i in range(len(algorithm_list)):
        algorithm_averaged_success_data_arr[i] = np.average(success_data_arr[:, :, :, :, :, :, i, :], 
                                                            axis=(1, 2, 3, 4, 5, 6))
        algorithm_averaged_compute_time_data_arr[i] = np.average(compute_time_data_arr[:, :, :, :, :, :, i, :], 
                                                                 axis=(1, 2, 3, 4, 5, 6))
        algorithm_averaged_optimal_ratio_data_arr[i] = np.average(optimal_ratio_data_arr[:, :, :, :, :, :, i, :], 
                                                                  axis=(1, 2, 3, 4, 5, 6))
        algorithm_averaged_success_compute_time_data_arr[i] = \
            np.average(success_compute_time_data_arr[:, :, :, :, :, :, i, :], axis=(1, 2, 3, 4, 5, 6))
        algorithm_std_success_data_arr[i] = np.std(np.average(success_data_arr[:, :, :, :, :, :, i, :], 
                                                              axis=(1, 2, 3, 4, 5)), axis=-1)
        algorithm_std_compute_time_data_arr[i] = np.std(np.average(compute_time_data_arr[:, :, :, :, :, :, i, :], 
                                                                   axis=(1, 2, 3, 4, 5)), axis=-1)
        algorithm_std_optimal_ratio_data_arr[i] = np.std(np.average(optimal_ratio_data_arr[:, :, :, :, :, :, i, :], 
                                                                    axis=(1, 2, 3, 4, 5)), axis=-1)
        algorithm_std_success_compute_time_data_arr[i] = \
            np.std(np.average(success_compute_time_data_arr[:, :, :, :, :, :, i, :], axis=(1, 2, 3, 4, 5)), axis=-1)
    # Get figure
    fig, ((ax_1, ax_2), (ax_3, ax_4)) = plt.subplots(2, 2, figsize=(8, 8))
    # Add lineplots with std as fill between for respective algorithm
    for i, algorithm in enumerate(algorithm_list):
        sns.lineplot(x=num_bombs_per_region_list, 
                     y=algorithm_averaged_success_data_arr[i], 
                     ax=ax_1, 
                     label='CBS-TA-PTC' if algorithm == 'cbs_ta_ptc' else 'CBS-TA',
                     legend=False)
        ax_1.fill_between(
            x=num_bombs_per_region_list, 
            y1=np.clip(algorithm_averaged_success_data_arr[i] - algorithm_std_success_data_arr[i], 0, None), 
            y2=np.clip(algorithm_averaged_success_data_arr[i] + algorithm_std_success_data_arr[i], None, 1), 
            alpha=0.15
        )
        sns.lineplot(x=num_bombs_per_region_list, 
                     y=algorithm_averaged_optimal_ratio_data_arr[i], 
                     ax=ax_2, 
                     label='_nolegend_',
                     legend=False)
        ax_2.fill_between(
            x=num_bombs_per_region_list, 
            y1=np.clip(algorithm_averaged_optimal_ratio_data_arr[i] - algorithm_std_optimal_ratio_data_arr[i], 0, None), 
            y2=np.clip(algorithm_averaged_optimal_ratio_data_arr[i] + algorithm_std_optimal_ratio_data_arr[i], None, 1), 
            alpha=0.15
        )
        sns.lineplot(x=num_bombs_per_region_list, 
                     y=algorithm_averaged_compute_time_data_arr[i], 
                     ax=ax_3, 
                     label='_nolegend_',
                     legend=False)
        ax_3.fill_between(
            x=num_bombs_per_region_list, 
            y1=np.clip(algorithm_averaged_compute_time_data_arr[i] - algorithm_std_compute_time_data_arr[i], 0, None), 
            y2=algorithm_averaged_compute_time_data_arr[i] + algorithm_std_compute_time_data_arr[i], 
            alpha=0.15
        )
        sns.lineplot(x=num_bombs_per_region_list, 
                     y=algorithm_averaged_success_compute_time_data_arr[i], 
                     ax=ax_4, 
                     label='_nolegend_',
                     legend=False)
        ax_4.fill_between(
            x=num_bombs_per_region_list, 
            y1=np.clip(algorithm_averaged_success_compute_time_data_arr[i] \
                       - algorithm_std_success_compute_time_data_arr[i], 0, None), 
            y2=algorithm_averaged_success_compute_time_data_arr[i] + algorithm_std_success_compute_time_data_arr[i], 
            alpha=0.15
        )
    # Set axis labels
    ax_1.set_xlabel('Number of bombs per region', fontsize=12)
    ax_1.set_ylabel('Success rate', fontsize=12)
    ax_2.set_xlabel('Number of bombs per region', fontsize=12)
    ax_2.set_ylabel('Optimality', fontsize=12)
    ax_3.set_xlabel('Number of bombs per region', fontsize=12)
    ax_3.set_ylabel('Runtime (seconds)', fontsize=12)
    ax_4.set_xlabel('Number of bombs per region', fontsize=12)
    ax_4.set_ylabel('Runtime for optimal solutions (seconds)', fontsize=12)
    # Generate legend
    fig.legend(loc='lower center', bbox_to_anchor=(0.5, -0.05), ncols=len(algorithm_list), fontsize=12)
    # Save plot
    fig.tight_layout()
    fig.savefig(fname=save_path + 'plots_num_bombs_per_region', bbox_inches='tight')

    # Figure 2: Number of bombs per subtask plots

    # Array to store data arrays for specific algorithm
    algorithm_averaged_success_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_averaged_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_averaged_optimal_ratio_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_averaged_success_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_success_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_optimal_ratio_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_success_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    # Get average and std between algorithms
    for i in range(len(algorithm_list)):
        algorithm_averaged_success_data_arr[i] = np.average(success_data_arr[:, :, :, :, :, :, i, :], 
                                                            axis=(0, 2, 3, 4, 5, 6))
        algorithm_averaged_compute_time_data_arr[i] = np.average(compute_time_data_arr[:, :, :, :, :, :, i, :], 
                                                                 axis=(0, 2, 3, 4, 5, 6))
        algorithm_averaged_optimal_ratio_data_arr[i] = np.average(optimal_ratio_data_arr[:, :, :, :, :, :, i, :], 
                                                                  axis=(0, 2, 3, 4, 5, 6))
        algorithm_averaged_success_compute_time_data_arr[i] = \
            np.average(success_compute_time_data_arr[:, :, :, :, :, :, i, :], axis=(0, 2, 3, 4, 5, 6))
        algorithm_std_success_data_arr[i] = np.std(np.average(success_data_arr[:, :, :, :, :, :, i, :], 
                                                              axis=(0, 2, 3, 4, 5)), axis=-1)
        algorithm_std_compute_time_data_arr[i] = np.std(np.average(compute_time_data_arr[:, :, :, :, :, :, i, :], 
                                                                   axis=(0, 2, 3, 4, 5)), axis=-1)
        algorithm_std_optimal_ratio_data_arr[i] = np.std(np.average(optimal_ratio_data_arr[:, :, :, :, :, :, i, :], 
                                                                    axis=(0, 2, 3, 4, 5)), axis=-1)
        algorithm_std_success_compute_time_data_arr[i] = \
            np.std(np.average(success_compute_time_data_arr[:, :, :, :, :, :, i, :], axis=(0, 2, 3, 4, 5)), axis=-1)
    # Get figure
    fig, ((ax_1, ax_2), (ax_3, ax_4)) = plt.subplots(2, 2, figsize=(8, 8))
    # Add lineplots with std as fill between for respective algorithm
    for i, algorithm in enumerate(algorithm_list):
        sns.lineplot(x=num_bombs_sub_task_list, 
                     y=algorithm_averaged_success_data_arr[i], 
                     ax=ax_1, 
                     label='CBS-TA-PTC' if algorithm == 'cbs_ta_ptc' else 'CBS-TA',
                     legend=False)
        ax_1.fill_between(
            x=num_bombs_sub_task_list, 
            y1=np.clip(algorithm_averaged_success_data_arr[i] - algorithm_std_success_data_arr[i], 0, None), 
            y2=np.clip(algorithm_averaged_success_data_arr[i] + algorithm_std_success_data_arr[i], None, 1), 
            alpha=0.15
        )
        sns.lineplot(x=num_bombs_sub_task_list, 
                     y=algorithm_averaged_optimal_ratio_data_arr[i], 
                     ax=ax_2, 
                     label='_nolegend_',
                     legend=False)
        ax_2.fill_between(
            x=num_bombs_sub_task_list, 
            y1=np.clip(algorithm_averaged_optimal_ratio_data_arr[i] - algorithm_std_optimal_ratio_data_arr[i], 0, None), 
            y2=np.clip(algorithm_averaged_optimal_ratio_data_arr[i] + algorithm_std_optimal_ratio_data_arr[i], None, 1), 
            alpha=0.15
        )
        sns.lineplot(x=num_bombs_sub_task_list, 
                     y=algorithm_averaged_compute_time_data_arr[i], 
                     ax=ax_3, 
                     label='_nolegend_',
                     legend=False)
        ax_3.fill_between(
            x=num_bombs_sub_task_list, 
            y1=np.clip(algorithm_averaged_compute_time_data_arr[i] - algorithm_std_compute_time_data_arr[i], 0, None), 
            y2=algorithm_averaged_compute_time_data_arr[i] + algorithm_std_compute_time_data_arr[i], 
            alpha=0.15
        )
        sns.lineplot(x=num_bombs_sub_task_list, 
                     y=algorithm_averaged_success_compute_time_data_arr[i], 
                     ax=ax_4, 
                     label='_nolegend_',
                     legend=False)
        ax_4.fill_between(
            x=num_bombs_sub_task_list, 
            y1=np.clip(algorithm_averaged_success_compute_time_data_arr[i] \
                       - algorithm_std_success_compute_time_data_arr[i], 0, None), 
            y2=algorithm_averaged_success_compute_time_data_arr[i] + algorithm_std_success_compute_time_data_arr[i], 
            alpha=0.15
        )
    # Set axis labels
    ax_1.set_xlabel('Number of bombs per subtask', fontsize=12)
    ax_1.set_ylabel('Success rate', fontsize=12)
    ax_2.set_xlabel('Number of bombs per subtask', fontsize=12)
    ax_2.set_ylabel('Optimality', fontsize=12)
    ax_3.set_xlabel('Number of bombs per subtask', fontsize=12)
    ax_3.set_ylabel('Runtime (seconds)', fontsize=12)
    ax_4.set_xlabel('Number of bombs per subtask', fontsize=12)
    ax_4.set_ylabel('Runtime for optimal solutions (seconds)', fontsize=12)
    # Generate legend
    fig.legend(loc='lower center', bbox_to_anchor=(0.5, -0.05), ncols=len(algorithm_list), fontsize=12)
    # Save plot
    fig.tight_layout()
    fig.savefig(fname=save_path + 'plots_num_bombs_per_subtask', bbox_inches='tight')

    # Figure 3: Seconds per timestep plots

    # Array to store data arrays for specific algorithm
    algorithm_averaged_success_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_averaged_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_averaged_optimal_ratio_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_averaged_success_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_success_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_optimal_ratio_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    algorithm_std_success_compute_time_data_arr = np.ndarray(shape=(len(algorithm_list),), dtype=object)
    # Get average and std between algorithms
    for i in range(len(algorithm_list)):
        algorithm_averaged_success_data_arr[i] = np.average(success_data_arr[:, :, :, :, :, :, i, :], 
                                                            axis=(0, 1, 3, 4, 5, 6))
        algorithm_averaged_compute_time_data_arr[i] = np.average(compute_time_data_arr[:, :, :, :, :, :, i, :], 
                                                                 axis=(0, 1, 3, 4, 5, 6))
        algorithm_averaged_optimal_ratio_data_arr[i] = np.average(optimal_ratio_data_arr[:, :, :, :, :, :, i, :], 
                                                                  axis=(0, 1, 3, 4, 5, 6))
        algorithm_averaged_success_compute_time_data_arr[i] = \
            np.average(success_compute_time_data_arr[:, :, :, :, :, :, i, :], axis=(0, 1, 3, 4, 5, 6))
        algorithm_std_success_data_arr[i] = np.std(np.average(success_data_arr[:, :, :, :, :, :, i, :], 
                                                              axis=(0, 1, 3, 4, 5)), axis=-1)
        algorithm_std_compute_time_data_arr[i] = np.std(np.average(compute_time_data_arr[:, :, :, :, :, :, i, :], 
                                                                   axis=(0, 1, 3, 4, 5)), axis=-1)
        algorithm_std_optimal_ratio_data_arr[i] = np.std(np.average(optimal_ratio_data_arr[:, :, :, :, :, :, i, :], 
                                                                    axis=(0, 1, 3, 4, 5)), axis=-1)
        algorithm_std_success_compute_time_data_arr[i] = \
            np.std(np.average(success_compute_time_data_arr[:, :, :, :, :, :, i, :], axis=(0, 1, 3, 4, 5)), axis=-1)
    # Get figure
    fig, ((ax_1, ax_2), (ax_3, ax_4)) = plt.subplots(2, 2, figsize=(8, 8))
    # Add lineplots with std as fill between for respective algorithm
    for i, algorithm in enumerate(algorithm_list):
        sns.lineplot(x=seconds_per_timestep_list, 
                     y=algorithm_averaged_success_data_arr[i], 
                     ax=ax_1, 
                     label='CBS-TA-PTC' if algorithm == 'cbs_ta_ptc' else 'CBS-TA',
                     legend=False)
        ax_1.fill_between(
            x=seconds_per_timestep_list, 
            y1=np.clip(algorithm_averaged_success_data_arr[i] - algorithm_std_success_data_arr[i], 0, None), 
            y2=np.clip(algorithm_averaged_success_data_arr[i] + algorithm_std_success_data_arr[i], None, 1), 
            alpha=0.15
        )
        sns.lineplot(x=seconds_per_timestep_list, 
                     y=algorithm_averaged_optimal_ratio_data_arr[i], 
                     ax=ax_2, 
                     label='_nolegend_',
                     legend=False)
        ax_2.fill_between(
            x=seconds_per_timestep_list, 
            y1=np.clip(algorithm_averaged_optimal_ratio_data_arr[i] - algorithm_std_optimal_ratio_data_arr[i], 0, None), 
            y2=np.clip(algorithm_averaged_optimal_ratio_data_arr[i] + algorithm_std_optimal_ratio_data_arr[i], None, 1), 
            alpha=0.15
        )
        sns.lineplot(x=seconds_per_timestep_list, 
                     y=algorithm_averaged_compute_time_data_arr[i], 
                     ax=ax_3, 
                     label='_nolegend_',
                     legend=False)
        ax_3.fill_between(
            x=seconds_per_timestep_list, 
            y1=np.clip(algorithm_averaged_compute_time_data_arr[i] - algorithm_std_compute_time_data_arr[i], 0, None), 
            y2=algorithm_averaged_compute_time_data_arr[i] + algorithm_std_compute_time_data_arr[i], 
            alpha=0.15
        )
        sns.lineplot(x=seconds_per_timestep_list, 
                     y=algorithm_averaged_success_compute_time_data_arr[i], 
                     ax=ax_4, 
                     label='_nolegend_',
                     legend=False)
        ax_4.fill_between(
            x=seconds_per_timestep_list, 
            y1=np.clip(algorithm_averaged_success_compute_time_data_arr[i] \
                       - algorithm_std_success_compute_time_data_arr[i], 0, None), 
            y2=algorithm_averaged_success_compute_time_data_arr[i] + algorithm_std_success_compute_time_data_arr[i], 
            alpha=0.15
        )
    # Set axis labels
    ax_1.set_xlabel('Seconds per timestep', fontsize=12)
    ax_1.set_ylabel('Success rate', fontsize=12)
    ax_2.set_xlabel('Seconds per timestep', fontsize=12)
    ax_2.set_ylabel('Optimality', fontsize=12)
    ax_3.set_xlabel('Seconds per timestep', fontsize=12)
    ax_3.set_ylabel('Runtime (seconds)', fontsize=12)
    ax_4.set_xlabel('Seconds per timestep', fontsize=12)
    ax_4.set_ylabel('Runtime for optimal solutions (seconds)', fontsize=12)
    # Generate legend
    fig.legend(loc='lower center', bbox_to_anchor=(0.5, -0.05), ncols=len(algorithm_list), fontsize=12)
    # Save plot
    fig.tight_layout()
    fig.savefig(fname=save_path + 'plots_seconds_per_timestep', bbox_inches='tight')

def run_experiments(
        num_of_iterations: int,
        solution_timeout: float,
        num_bombs_per_region_list: list[int],
        num_bombs_sub_task_list: list[int],
        seconds_per_timestep_list: list[float],
        task_allocation_heuristic_list: list[str],
        pruning_heuristic_list: list[str],
        low_level_search_list: list[str],
        algorithm_list: list[str],
        data_save_path: str,
        experiment_name: Optional[str] = None,
        verbose: bool = True,
        **kwargs
    ):
    """
    Function to run tests to obtain results based on the following parameters:

        1) Number of bombs per region
        2) Number of bombs per subtasks  
        3) Seconds per timestep 
        4) Task allocation heuristic ('fuse_length', 'k_means')
        5) Pruning heuristic ('even_bomb_allocation', 'none')
        6) Low level search ('multi_label_a_star', 'multi_step_a_star')
        7) Algorithm ('cbs_ta_ptc', 'cbs_ta')

    Parameters
    ----------
    num_of_iterations : int
        Number of iterations to run
    solution_timeout : float
        Timeout in seconds to find solution
    num_bombs_per_region_list : list[int]
        List containing num_bombs_per_region
    num_bombs_sub_task_list : list[int]
        List containing num_bombs_sub_task
    seconds_per_timestep_list : list[float]
        List containing seconds_per_timestep
    task_allocation_heuristic_list : list[str]
        List containing task allocation heuristics
    pruning_heuristic_list : list[str]
        List containing pruning heuristics
    low_level_search_list : list[str]
        List containing low-level search methods
    algorithm_list : list[str]
        List containing algorithms
    data_save_path : str
        Path to save data
    experiment_name : Optional[str]
        Experiment name for save path
    verbose : bool, default=True
        Whether to print relevant information

    Keyword Arguments
    -----------------
    mission_length : float, default=15*60
        Mission length, in seconds
    valid_regions : set[Region], optional
        Set of regions to include in the environment
    start_regions : set[Region], optional
        Set of valid regions for initial mission locations
    epsilon : float, default=1.0
        Suboptimality factor for score. Default is optimal
    same_start_loc : bool, default=True
        Whether for all agents to start at the same location
        NOTE: Default DragonBaseEnv starts at same location/node
    include_collisions : bool, default=False
        Whether to consider collisions for environment
        NOTE: Default DragonBaseEnv has no collisions
    include_chained_bombs : bool, default=True
        Whether to include chained bombs in the environment
    include_fire_bombs : bool, default=False
        Whether to include fire bombs in the environment
    include_fuse_bombs : bool, default=True
        Whether to include fuse bombs in the environment
    bomb_explode_terminate : bool, default=True
        Whether to terminate environment when bomb explodes
    """
    # Get experiment name
    if experiment_name == None:
        experiment_name = f'max_num_bombs_per_region_{num_bombs_per_region_list[-1]}_' + \
                          f'max_num_bombs_sub_task_{num_bombs_sub_task_list[-1]}_' + \
                          f'max_seconds_per_timestep_{seconds_per_timestep_list[-1]}_' + \
                          f"task_heur_{'_'.join(task_allocation_heuristic_list)}_" + \
                          f"pruning_heur_{'_'.join(pruning_heuristic_list)}_" + \
                          f"low_level_{'_'.join(low_level_search_list)}_" + \
                          f"algo_{'_'.join(algorithm_list)}_" + \
                          f'num_of_iterations_{num_of_iterations}_' + \
                          f'solution_timeout_{solution_timeout}'
    # Get save path for data
    save_path = data_save_path + experiment_name + '/'
    if not os.path.exists(save_path):
        # Create directory if it doesn't exist
        os.makedirs(save_path)
    else:
        # Remove and make new directory if it exist
        shutil.rmtree(save_path)
        os.makedirs(save_path)

    # Array to store all data from experiments
    success_data_arr = np.zeros((len(num_bombs_per_region_list), 
                                 len(num_bombs_sub_task_list), 
                                 len(seconds_per_timestep_list), 
                                 len(task_allocation_heuristic_list), 
                                 len(pruning_heuristic_list), 
                                 len(low_level_search_list),
                                 len(algorithm_list),
                                 num_of_iterations),
                                dtype=bool)
    compute_time_data_arr = np.zeros((len(num_bombs_per_region_list), 
                                      len(num_bombs_sub_task_list), 
                                      len(seconds_per_timestep_list), 
                                      len(task_allocation_heuristic_list), 
                                      len(pruning_heuristic_list), 
                                      len(low_level_search_list),
                                      len(algorithm_list),
                                      num_of_iterations))
    optimal_ratio_data_arr = np.zeros((len(num_bombs_per_region_list), 
                                       len(num_bombs_sub_task_list), 
                                       len(seconds_per_timestep_list), 
                                       len(task_allocation_heuristic_list), 
                                       len(pruning_heuristic_list), 
                                       len(low_level_search_list),
                                       len(algorithm_list),
                                       num_of_iterations))

    # Print relevant information
    if verbose:
        print('\n Start Experiments \n')
        print('Iterating over the following parameters: ')
        print(f'Number of bombs per region: {num_bombs_per_region_list}')
        print(f'Number of bombs per subtask: {num_bombs_sub_task_list}')
        print(f'Seconds per timestep: {seconds_per_timestep_list}')
        print(f'Task allocation heuristic: {task_allocation_heuristic_list}')
        print(f'Pruning heuristic: {pruning_heuristic_list}')
        print(f'Low-level heuristic: {low_level_search_list}')
        print(f'Algorithm: {algorithm_list}')

    # Iterate over num_bombs_per_region
    for i, num_bombs_per_region in enumerate(num_bombs_per_region_list):
        # Iterate over num_bombs_sub_task
        for j, num_bombs_sub_task in enumerate(num_bombs_sub_task_list):
            # Iterate over seconds_per_timestep per 
            for k, seconds_per_timestep in enumerate(seconds_per_timestep_list): 
                # Iterate over task_allocation_heuristic
                for l, task_allocation_heuristic in enumerate(task_allocation_heuristic_list):
                    # Iterate over pruning_heuristic 
                    for m, pruning_heuristic in enumerate(pruning_heuristic_list):
                        # Iterate over low_level_search
                        for n, low_level_search in enumerate(low_level_search_list):
                            # Iterate over algorithms
                            for o, algorithm in enumerate(algorithm_list):
                                # Iterate over number of iterations:
                                for iteration in range(num_of_iterations):
                                    # Get results from solver
                                    success, compute_time, optimal_ratio = \
                                        get_results(
                                            algo=algorithm,
                                            num_bombs_per_region=num_bombs_per_region,
                                            num_bombs_sub_task=num_bombs_sub_task,
                                            seconds_per_timestep=seconds_per_timestep,
                                            task_allocation_heuristic=task_allocation_heuristic,
                                            pruning_heuristic=pruning_heuristic,
                                            low_level_search=low_level_search,
                                            solution_timeout=solution_timeout,
                                            verbose=verbose,
                                            **kwargs
                                        )        
                                    # Update success and compute_time array
                                    success_data_arr[i, j, k, l, m, n, o, iteration] = success
                                    compute_time_data_arr[i, j, k, l, m, n, o, iteration] = compute_time
                                    optimal_ratio_data_arr[i, j, k, l, m, n, o, iteration] = optimal_ratio

    # Save data arrays to file
    np.save(save_path + 'success', success_data_arr)
    np.save(save_path + 'compute_time', compute_time_data_arr)
    np.save(save_path + 'optimal_ratio', optimal_ratio_data_arr)

def main(run_exp: bool, get_vis: bool):
    """
    Main function

    Parameters
    ----------
    run_exp : bool
        Whether to run experiments to generate data array
    get_vis : bool
        Whether to generate visualisations from data array
    """

    # Parameters for run_experiments() and get_visualisations()
    NUM_OF_ITERATIONS                   = 10
    SOLUTION_TIMEOUT                    = 300.0
    NUM_BOMBS_PER_REGION_LIST           = [i for i in range(1, 16)]
    NUM_BOMBS_SUB_TASK_LIST             = [i for i in range(1, 4)]
    SECONDS_PER_TIMESTEP_LIST           = [i for i in range(1, 4)]
    TASK_ALLOCATION_HEURISTIC_LIST      = ['fuse_length'] # ['fuse_length', 'k_means']
    PRUNING_HEURISTIC_LIST              = ['none'] # ['even_bomb_allocation', 'none']
    LOW_LEVEL_SEARCH_LIST               = ['multi_label_a_star'] # ['multi_label_a_star', 'multi_step_a_star']
    ALGORITHM_LIST                      = ['cbs_ta_ptc', 'cbs_ta']
    DATA_SAVE_PATH                      = './experiments/data/'
    VISUALISATION_SAVE_PATH             = './experiments/visualisation/'
    EXPERIMENT_NAME                     = f'max_num_bombs_per_region_{NUM_BOMBS_PER_REGION_LIST[-1]}_' + \
                                          f'max_num_bombs_sub_task_{NUM_BOMBS_SUB_TASK_LIST[-1]}_' + \
                                          f'max_seconds_per_timestep_{SECONDS_PER_TIMESTEP_LIST[-1]}_' + \
                                          f"task_heur_{'_'.join(TASK_ALLOCATION_HEURISTIC_LIST)}_" + \
                                          f"pruning_heur_{'_'.join(PRUNING_HEURISTIC_LIST)}_" + \
                                          f"low_level_{'_'.join(LOW_LEVEL_SEARCH_LIST)}_" + \
                                          f"algo_{'_'.join(ALGORITHM_LIST)}_" + \
                                          f'num_of_iterations_{NUM_OF_ITERATIONS}_' + \
                                          f'solution_timeout_{SOLUTION_TIMEOUT}'
    VERBOSE                             = True
    # Keyword arguments for run_experiments()
    MISSION_LENGTH                      = 15*60
    RECON_PHASE_LENGTH                  = 0*60
    VALID_REGIONS                       = set(Region) # [Region.forest, Region.desert, Region.village]
    START_REGIONS                       = set(Region)
    EPSILON                             = 1.0
    SAME_START_LOC                      = True
    INCLUDE_COLLISIONS                  = False
    INCLUDE_CHAINED_BOMBS               = True
    INCLUDE_FIRE_BOMBS                  = False
    INCLUDE_FUSE_BOMBS                  = True
    BOMB_EXPLODE_TERMINATE              = True
    
    # Run experiments
    if run_exp:
        run_experiments(num_of_iterations=NUM_OF_ITERATIONS,
                        solution_timeout=SOLUTION_TIMEOUT,
                        num_bombs_per_region_list=NUM_BOMBS_PER_REGION_LIST,
                        num_bombs_sub_task_list=NUM_BOMBS_SUB_TASK_LIST,
                        seconds_per_timestep_list=SECONDS_PER_TIMESTEP_LIST,
                        task_allocation_heuristic_list=TASK_ALLOCATION_HEURISTIC_LIST,
                        pruning_heuristic_list=PRUNING_HEURISTIC_LIST,
                        low_level_search_list=LOW_LEVEL_SEARCH_LIST,
                        algorithm_list=ALGORITHM_LIST,
                        data_save_path=DATA_SAVE_PATH,
                        experiment_name=EXPERIMENT_NAME,
                        verbose=VERBOSE,
                        mission_length=MISSION_LENGTH,
                        valid_regions=VALID_REGIONS,
                        start_regions=START_REGIONS,
                        epsilon=EPSILON,
                        same_start_loc=SAME_START_LOC,
                        include_collisions=INCLUDE_COLLISIONS,
                        include_chained_bombs=INCLUDE_CHAINED_BOMBS,
                        include_fire_bombs=INCLUDE_FIRE_BOMBS,
                        include_fuse_bombs=INCLUDE_FUSE_BOMBS,
                        bomb_explode_terminate=BOMB_EXPLODE_TERMINATE)
    # Get visualisations
    if get_vis:
        get_visualisations(num_of_iterations=NUM_OF_ITERATIONS,
                           solution_timeout=SOLUTION_TIMEOUT,
                           num_bombs_per_region_list=NUM_BOMBS_PER_REGION_LIST,
                           num_bombs_sub_task_list=NUM_BOMBS_SUB_TASK_LIST,
                           seconds_per_timestep_list=SECONDS_PER_TIMESTEP_LIST,
                           task_allocation_heuristic_list=TASK_ALLOCATION_HEURISTIC_LIST,
                           pruning_heuristic_list=PRUNING_HEURISTIC_LIST,
                           low_level_search_list=LOW_LEVEL_SEARCH_LIST,
                           algorithm_list=ALGORITHM_LIST,
                           data_save_path=DATA_SAVE_PATH,
                           visualisation_save_path=VISUALISATION_SAVE_PATH,
                           experiment_name=EXPERIMENT_NAME)

if __name__ == "__main__":
    main(False, True) 