import matplotlib.pyplot as plt
import os
import random
import seaborn as sns
import shutil

from gym_dragon_search.cbs import solve
from gym_dragon.core import Color, Region

def main():
    # Parameters for CBSRewardSolver
    SOLUTION_TIMEOUT            = 900.0
    ALGORITHM                   = 'cbs_ta_ptc'
    MISSION_LENGTH              = 8*60
    RECON_PHASE_LENGTH          = 0*60
    SECONDS_PER_TIMESTEP        = 2.0 
    VALID_REGIONS               = set(Region) # set(Region) [Region.forest, Region.desert, Region.village]
    NUM_BOMBS_PER_REGION        = 15
    NUM_BOMBS_SUB_TASK          = 3
    BUDGET_PER_REGION           = None
    START_LOCATION              = (24, 149) # None (24, 149) random.choice([(28, 56), (15, 55), (40, 56), (33, 64), (22, 63)])
    START_REGIONS               = set(Region) # set(Region)
    EPSILON                     = 1.0
    SAME_START_LOC              = True
    INCLUDE_COLLISIONS          = False
    INCLUDE_CHAINED_BOMBS       = True
    INCLUDE_FIRE_BOMBS          = False
    INCLUDE_FUSE_BOMBS          = True
    BOMB_EXPLODE_TERMINATE      = True
    TASK_ALLOCATION_HEURISTIC   = 'fuse_length' # 'k_means' 'fuse_length'
    PRUNING_HEURISTIC           = 'none' # 'even_bomb_allocation' 'none' 
    LOW_LEVEL_SEARCH            = 'multi_label_a_star' # 'multi_step_a_star' 'multi_label_a_star'
    NUM_OF_CLUSTERS             = NUM_BOMBS_PER_REGION // NUM_BOMBS_SUB_TASK
    CSV_PATH                    = './resources/OrionMaps/MapBlocks_Orion_1.1.csv' # './resources/OrionMaps/MapBlocks_Orion_1.1.csv' './resources/mturk_bomb.csv' None
    GIF_SAVE_PATH               = f'./experiments/gif/' + \
                                  f'MapBlocks_Orion_1.1/' if CSV_PATH else \
                                  f'./experiments/gif/' + \
                                  f'num_bombs_per_region_{NUM_BOMBS_PER_REGION}_' + \
                                  f'num_bombs_sub_task_{NUM_BOMBS_SUB_TASK}_' + \
                                  f'incl_collisions{INCLUDE_COLLISIONS}_' + \
                                  f'incl_chained_bombs_{INCLUDE_CHAINED_BOMBS}_' + \
                                  f'incl_fire_bombs_{INCLUDE_FIRE_BOMBS}_' + \
                                  f'incl_fuse_bombs_{INCLUDE_FUSE_BOMBS}_' + \
                                  f'task_alloc_heur_' + f'{TASK_ALLOCATION_HEURISTIC}_' + \
                                  f'pruning_heur_' + f'{PRUNING_HEURISTIC}_' + \
                                  f'low_level_search_' + f'{LOW_LEVEL_SEARCH}_' + \
                                  f'region_all/'
    SAVE_RENDER                 = True
    VERBOSE                     = True
    RENDER                      = True
    CSV_SUBTRACT_FUSE           = 2
    OVERLAY_GRAPH               = True
    OPTIMALITY_SAVE_PATH        = f'./experiments/visualisation/test/' + \
                                  f'MapBlocks_Orion_1.1/' if CSV_PATH else \
                                  f'./experiments/visualisation/test/' + \
                                  f'num_bombs_per_region_{NUM_BOMBS_PER_REGION}_' + \
                                  f'num_bombs_sub_task_{NUM_BOMBS_SUB_TASK}_' + \
                                  f'incl_collisions{INCLUDE_COLLISIONS}_' + \
                                  f'incl_chained_bombs_{INCLUDE_CHAINED_BOMBS}_' + \
                                  f'incl_fire_bombs_{INCLUDE_FIRE_BOMBS}_' + \
                                  f'incl_fuse_bombs_{INCLUDE_FUSE_BOMBS}_' + \
                                  f'task_alloc_heur_' + f'{TASK_ALLOCATION_HEURISTIC}_' + \
                                  f'pruning_heur_' + f'{PRUNING_HEURISTIC}_' + \
                                  f'low_level_search_' + f'{LOW_LEVEL_SEARCH}_' + \
                                  f'region_all/'
    # Solve
    solution_node, success, compute_time_list, optimality_ratio_list, exception  = \
      solve(solution_timeout=SOLUTION_TIMEOUT,
            algo=ALGORITHM,
            num_bombs_sub_task=NUM_BOMBS_SUB_TASK,
            mission_length=MISSION_LENGTH,
            recon_phase_length=RECON_PHASE_LENGTH,  
            seconds_per_timestep=SECONDS_PER_TIMESTEP,
            valid_regions=VALID_REGIONS,
            csv_path=CSV_PATH,
            num_bombs_per_region=NUM_BOMBS_PER_REGION,
            budget_per_region=BUDGET_PER_REGION,
            start_location=START_LOCATION,
            start_regions=START_REGIONS,
            epsilon=EPSILON,
            same_start_loc=SAME_START_LOC,
            include_collisions=INCLUDE_COLLISIONS,
            include_chained_bombs=INCLUDE_CHAINED_BOMBS,
            include_fire_bombs=INCLUDE_FIRE_BOMBS,
            include_fuse_bombs=INCLUDE_FUSE_BOMBS,
            bomb_explode_terminate=BOMB_EXPLODE_TERMINATE,
            task_allocation_heuristic=TASK_ALLOCATION_HEURISTIC,
            pruning_heuristic=PRUNING_HEURISTIC,
            low_level_search=LOW_LEVEL_SEARCH,
            save_render=SAVE_RENDER,
            verbose=VERBOSE,
            render=RENDER,
            save_path=GIF_SAVE_PATH,
            num_of_clusters=NUM_OF_CLUSTERS,
            csv_subtract_fuse=CSV_SUBTRACT_FUSE,
            overlay_graph=OVERLAY_GRAPH)

    # Make save directory for plot
    if not os.path.exists(OPTIMALITY_SAVE_PATH):
        # Create directory if it doesn't exist
        os.makedirs(OPTIMALITY_SAVE_PATH)
    else:
        # Remove and make new directory if it exist
        shutil.rmtree(OPTIMALITY_SAVE_PATH)
        os.makedirs(OPTIMALITY_SAVE_PATH)

    # Plot optimality ratio vs compute time

    # Set default seaborn theme
    sns.set_theme()

    # Get figure
    fig, ax_1 = plt.subplots(1, 1, figsize=(8, 4))
    # Add averaged success rate and runtime lineplots
    sns.lineplot(x=compute_time_list, 
                 y=optimality_ratio_list, 
                 ax=ax_1, 
                 legend=False,
                 drawstyle='steps-post')
    # Set axis labels
    ax_1.set_xlabel('Runtime (s)')
    ax_1.set_ylabel('Optimality Ratio')
    # Show plot
    fig.tight_layout()
    plt.savefig(OPTIMALITY_SAVE_PATH + 'optimality_plot', bbox_inches='tight')

if __name__ == "__main__":
    import cProfile
    cProfile.run('main()', sort='cumtime')